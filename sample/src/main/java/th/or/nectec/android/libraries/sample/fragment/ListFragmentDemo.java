package th.or.nectec.android.libraries.sample.fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import th.or.nectec.android.libraries.sample.R;
import th.or.nectec.android.libraries.sample.adapter.MultipleCursorAdapter;
import th.or.nectec.android.libraries.sample.adapter.TestAdapter;
import th.or.nectec.android.library.provider.NTTable;


/**
 * Created by N. Choatravee on 11/16/2014.
 */
public class ListFragmentDemo extends Fragment implements LoaderCallbacks<Cursor> {

    Bundle savedState;

    EditText mSearchView;
    ListView mDemoListView;

    MultipleCursorAdapter mDemoAdapter;

    String mKeyWord = "";

    public static final int LOAD_ADDRESS = 0;
    public static final int LOAD_PRENAME = 1;

    public ListFragmentDemo() {
        super();
    }

    public static ListFragmentDemo newInstance() {
        ListFragmentDemo fragment = new ListFragmentDemo();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_address_prename, container, false);
        initInstances(rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Restore State Here
        if (!restoreStateFromArguments()) {
            // First Time, Initialize something here
        }

        loadData();
    }

    public void loadData(){
        getLoaderManager().restartLoader(LOAD_ADDRESS, null, this);
        getLoaderManager().restartLoader(LOAD_PRENAME, null, this);
    }

    private void initInstances(View rootView) {
        // init instance with rootView.findViewById here
        mSearchView = (EditText) rootView.findViewById(R.id.search_view);

        mDemoListView = (ListView) rootView.findViewById(R.id.demo_listview);
        mDemoAdapter = new TestAdapter();
        mDemoAdapter.allocateCursors("c1", "c2", "c3");
        mDemoListView.setAdapter(mDemoAdapter);


        mDemoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int itemMode = mDemoAdapter.getItemViewType(position);
                Cursor cursor = (Cursor) mDemoAdapter.getItem(position);

                switch(itemMode){

                    case 0:
                        Toast.makeText(getActivity(), cursor.getString(0), Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(getActivity(), cursor.getString(0), Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getActivity(), cursor.getString(0), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isAdded()){
                    mKeyWord = s.toString();
                    loadData();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save State Here
        saveStateToArguments();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Save State Here
        saveStateToArguments();
    }

    ////////////////////
    // Don't Touch !!
    ////////////////////

    private void saveStateToArguments() {
        if (getView() != null)
            savedState = saveState();
        if (savedState != null) {
            Bundle b = getArguments();
            b.putBundle("savedState", savedState);
        }
    }

    ////////////////////
    // Don't Touch !!
    ////////////////////

    private boolean restoreStateFromArguments() {
        Bundle b = getArguments();
        savedState = b.getBundle("savedState");
        if (savedState != null) {
            restoreState();
            return true;
        }
        return false;
    }

    /////////////////////////////////
    // Restore Instance State Here
    /////////////////////////////////

    private void restoreState() {
        if (savedState != null) {
            // For Example
            //tv1.setText(savedState.getString("text"));
        }
    }

    //////////////////////////////
    // Save Instance State Here
    //////////////////////////////

    private Bundle saveState() {
        Bundle state = new Bundle();
        // For Example
        //state.putString("text", tv1.getText().toString());
        return state;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader loader;
        switch(id){
            case LOAD_ADDRESS:
                loader = new CursorLoader(getActivity(),
                        NTTable.RefAddress.CONTENT_URI,
                        new String[]{NTTable.RefAddress.ADDRESS_CODE, NTTable.RefAddress.SUBDISTRICT, NTTable.RefAddress.DISTRICT, NTTable.RefAddress.PROVINCE},
                        "subdistrict LIKE '%"+ mKeyWord +"%' OR district LIKE '%"+ mKeyWord +"%' OR province LIKE '%"+ mKeyWord +"%'", null, NTTable.RefAddress.ADDRESS_CODE+" ASC");
                return loader;
            case LOAD_PRENAME:

                loader = new CursorLoader(getActivity(),
                        NTTable.RefPrename.CONTENT_URI,
                        new String[]{NTTable.RefPrename.PRENAME, NTTable.RefPrename.PRENAME_FULL},
                        "prename LIKE '%"+ mKeyWord +"%' OR prename_full LIKE '%"+ mKeyWord +"%'", null, null);
                return loader;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch(loader.getId()){
            case LOAD_ADDRESS:
                Log.d("row count", data.getCount() + " ADDRESS");
                mDemoAdapter.swapCursor(data, "c2");
                mDemoAdapter.setKeyword(mKeyWord);
                break;
            case LOAD_PRENAME:
                Log.d("row count", data.getCount() + " PRENAME");
                mDemoAdapter.swapCursor(data, "c1");
                mDemoAdapter.swapCursor(data, "c3");
                mDemoAdapter.setKeyword(mKeyWord);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
