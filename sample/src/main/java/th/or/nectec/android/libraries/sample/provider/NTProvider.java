/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.libraries.sample.provider;

import android.content.UriMatcher;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class NTProvider extends th.or.nectec.android.library.provider.NTProvider {
    {
        AUTHORITY = "th.or.nectec.android.libraries.sample.provider.NTProvider";

        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AUTHORITY, "ref/address", ADDRESS);
        mUriMatcher.addURI(AUTHORITY, "ref/prename", PRENAME);

    }
}
