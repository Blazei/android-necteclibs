package th.or.nectec.android.libraries.sample.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import th.or.nectec.android.libraries.sample.R;
import th.or.nectec.android.library.widget.TextViewHighLighter;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class LocaleListItem extends RelativeLayout {

    TextView mHeaderTextView, mSubdistrictTextView, mDistrictTextView, mProvinceTextView;

    private String imageUrl = "";

    private TextView tvName;

    public LocaleListItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public LocaleListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    public LocaleListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.address_dialog_header_item, this);
    }

    private void initInstances() {
        mHeaderTextView = (TextView) findViewById(R.id.header);
        mSubdistrictTextView = (TextView) findViewById(R.id.subdistrictText);
        mDistrictTextView = (TextView) findViewById(R.id.districtText);
        mProvinceTextView = (TextView) findViewById(R.id.provinceText);
    }

    private void initWithAttrs(AttributeSet attrs) {

    }

    public void showHeader(boolean isShow, String text){
        if(isShow){
            mHeaderTextView.setVisibility(View.VISIBLE);
            mHeaderTextView.setText(text);
        }else{
            mHeaderTextView.setVisibility(View.GONE);
        }
    }

    public void setValue(String subdistrict, String district, String province){
        setValue(subdistrict, district, province, "");
    }

    public void setValue(String subdistrict, String district, String province, String keyword){
        int color = Color.parseColor("#0277BD");
        TextViewHighLighter.highLight(mSubdistrictTextView, subdistrict, keyword, color);
        TextViewHighLighter.highLight(mDistrictTextView, district, keyword, color);
        TextViewHighLighter.highLight(mProvinceTextView, province, keyword, color);
    }
}
