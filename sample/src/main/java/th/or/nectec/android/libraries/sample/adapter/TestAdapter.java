package th.or.nectec.android.libraries.sample.adapter;

import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;

import th.or.nectec.android.libraries.sample.views.LocaleListItem;
import th.or.nectec.android.libraries.sample.views.PrenameListItem;
import th.or.nectec.android.library.provider.NTTable;
import th.or.nectec.android.library.util.CursorHelper;
import th.or.nectec.android.library.util.Log;

/**
 * Created by N. Choatravee on 4/2/2558.
 */
public class TestAdapter extends MultipleCursorAdapter {

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(String tag, Cursor cursor, View convertView, ViewGroup parent) {

        if(tag.equals("c1")){
            return getPrenameListItem(convertView, parent, cursor, cursor.getPosition());
        }else if(tag.equals("c2")){
            return getLocaleView(convertView, parent, cursor, cursor.getPosition());
        }else{
            return getPrenameListItem(convertView, parent, cursor, cursor.getPosition());
        }
    }

    public View getLocaleView(View convertView, ViewGroup parent, Cursor cursor, int currentPosition) {
        LocaleListItem localeItem;
        if (convertView == null) {
            localeItem = new LocaleListItem(parent.getContext());
        } else {
            localeItem = (LocaleListItem) convertView;
        }


        String oldProvince;
        if (cursor.moveToPosition(currentPosition - 1)) {
            oldProvince = CursorHelper.getString(cursor, NTTable.RefAddress.PROVINCE);
        } else {
            oldProvince = "";
        }

        cursor.moveToPosition(currentPosition);
        String subdistrict = CursorHelper.getString(cursor, NTTable.RefAddress.SUBDISTRICT);
        String district = CursorHelper.getString(cursor, NTTable.RefAddress.DISTRICT);
        String province = CursorHelper.getString(cursor, NTTable.RefAddress.PROVINCE);


        if (currentPosition == 0 || (!oldProvince.equals(province))) {
            localeItem.showHeader(true, province);
        } else {
            localeItem.showHeader(false, null);
        }

        localeItem.setValue(subdistrict, district, province, mKeyword);

        return localeItem;
    }

    public PrenameListItem getPrenameListItem(View convertView, ViewGroup parent, Cursor cursor, int currentPosition) {
        PrenameListItem prenameItem;

        if (convertView == null) {
            prenameItem = new PrenameListItem(parent.getContext());
        } else {
            prenameItem = (PrenameListItem) convertView;
        }

        cursor.moveToPosition(currentPosition);
        String prename = CursorHelper.getString(cursor, NTTable.RefPrename.PRENAME);
        String fullPrename = CursorHelper.getString(cursor, NTTable.RefPrename.PRENAME_FULL);
        prenameItem.setValue(prename, fullPrename, mKeyword);

        if (currentPosition == 0) {
            prenameItem.showHeader(true);
        } else {
            prenameItem.showHeader(false);
        }


        return prenameItem;
    }
}
