package th.or.nectec.android.libraries.sample.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import th.or.nectec.android.libraries.sample.R;
import th.or.nectec.android.library.widget.TextViewHighLighter;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PrenameListItem extends RelativeLayout {
    TextView mHeaderTextView, mPrenameTextView, mDescriptionTextView;

    public PrenameListItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public PrenameListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    public PrenameListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.prename_item, this);
    }

    private void initInstances() {
        mHeaderTextView = (TextView) findViewById(R.id.header);
        mPrenameTextView = (TextView) findViewById(R.id.content);
        mDescriptionTextView = (TextView) findViewById(R.id.subcontent);
    }

    private void initWithAttrs(AttributeSet attrs) {

    }

    public void showHeader(boolean isShow){
        if(isShow){
            mHeaderTextView.setVisibility(View.VISIBLE);
        }else{
            mHeaderTextView.setVisibility(View.GONE);
        }
    }

    public void setValue(String prename, String fullPrename, String keyword){
        int color = Color.parseColor("#0277BD");
        TextViewHighLighter.highLight(mPrenameTextView, prename, keyword, color);
        TextViewHighLighter.highLight(mDescriptionTextView, fullPrename, keyword, color);
    }
}
