package th.or.nectec.android.libraries.sample.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import th.or.nectec.android.libraries.sample.R;
import th.or.nectec.android.libraries.sample.fragment.ListFragmentDemo;
import th.or.nectec.android.library.form.NTForm;
import th.or.nectec.android.library.form.validator.DateFieldCompareValidator;
import th.or.nectec.android.library.testLib;
import th.or.nectec.android.library.util.TwiceBackPressed;
import th.or.nectec.android.library.widget.ThaiDatePicker;


public class MainActivity extends ActionBarActivity {

    TwiceBackPressed mTBP;
    NTForm form;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testLib.getProjectName();
        mTBP = new TwiceBackPressed(MainActivity.this);

        form = new NTForm(this);
        form.addDateField(R.id.date_demo, "abc", false);
        form.addDateField(R.id.date_demo_2, "def", true).setSkipValidateOnEmpty(true).addValidator(
                new DateFieldCompareValidator(
                        DateFieldCompareValidator.BEFORE,
                        (ThaiDatePicker) form.getFieldByColumn("abc").getView(), "วันที่สิ้นสุดการเช่าไม่ควรเกิดก่อนวันที่เริ่มเช่า"));

        Button test = (Button) findViewById(R.id.test_validate);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, form.validate()+"", Toast.LENGTH_SHORT).show();
            }
        });

        if(savedInstanceState==null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, ListFragmentDemo.newInstance(), "test")
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {

        if(mTBP.onTwiceBackPressed()){
            super.onBackPressed();
        }
    }
}
