package th.or.nectec.android.libraries.sample.adapter;

import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by N. Choatravee on 29/1/2558.
 */

public abstract class MultipleCursorAdapter extends BaseAdapter {


    ArrayList<Cursor> mCursorSet = new ArrayList<>();
    ArrayList<String> mTag = new ArrayList<>();

    String mKeyword = "";


    int lastPosition = 0;

    public void allocateCursor(String tag) {
        mCursorSet.add(null);
        mTag.add(tag);

        notifyDataSetChanged();
    }

    public void allocateCursors(String... tags){
        for(String tag : tags){
            allocateCursor(tag);
        }
    }

    public void swapCursor(Cursor cursor, String tag) {

        int tagIndex = mTag.indexOf(tag);
        mCursorSet.set(tagIndex, cursor);

        notifyDataSetChanged();
    }

    public void setKeyword(String keyword) {
        mKeyword = keyword;
    }

    @Override
    public int getCount() {

        int cursorSetSize = mCursorSet.size();
        int totalSize = 0;
        for (int i = 0; i < cursorSetSize; i++) {
            totalSize += getCursorCount(i);
        }

        Log.d("total size", totalSize + "");
        return totalSize;

    }


    @Override
    public Object getItem(int position) {
        int cursorIndex = getItemViewType(position);
        Cursor cursor = getActualCursor(position, cursorIndex);
        return cursor;
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        //บอกว่า list นี้มี list item กี่แบบ
        int viewTypeCount = mCursorSet.size();
        return viewTypeCount > 0 ? viewTypeCount : 1;
    }

    @Override
    public int getItemViewType(int position) {
        //จำแนก type ของ list item
        int cursorSetSize = mCursorSet.size();
        int totalLength = 0;
        for (int i = 0; i < cursorSetSize; i++) {
            totalLength += getCursorCount(i);
            if (position < totalLength) {
                return i;
            } else {
                continue;
            }
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int cursorIndex = getItemViewType(position);
        Cursor cursor = getActualCursor(position, cursorIndex);
        return getView(mTag.get(cursorIndex), cursor, convertView, parent);
    }

    public Cursor getActualCursor(int position, int cursorIndex) {
        Cursor cursor = mCursorSet.get(cursorIndex);
        int actualPosition = 0;

        int totalLength = 0;
        int lastLength = 0;
        for (int i = 0; i <= cursorIndex; i++) {
            totalLength += getCursorCount(i);
            if (position < totalLength) {
                actualPosition = position - lastLength;
            } else {
                lastLength = totalLength;
            }
        }

        cursor.moveToPosition(actualPosition);
        return cursor;
    }

    public abstract View getView(String tag, Cursor cursor, View convertView, ViewGroup parent);

    public Cursor getCursor(String tag) {
        return mCursorSet.get(mTag.indexOf(tag));
    }

    private int getCursorCount(int cursorSetIndex){
        return mCursorSet.get(cursorSetIndex)!=null ? mCursorSet.get(cursorSetIndex).getCount() : 0;
    }
}
