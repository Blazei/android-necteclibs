/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.network;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * 
 */
public class HttpDownloadTask extends
		AsyncTask<HttpDownloadTask.DownloadParam, Integer, ArrayList<File>> {

	private ProgressBar mProgress;
	private OnFinishListener mListener;
	private OnProgressUpdateListener mProgressListener;

	public HttpDownloadTask(ProgressBar progress, OnFinishListener listener) {

		this.mProgress = progress;
		this.mListener = listener;
	}
	
	public void setOnProgressUpdateListener(OnProgressUpdateListener listener){
		mProgressListener = listener;
	}

	public static class DownloadParam {
		private ArrayList<String> urlList;
		private ArrayList<File> destList;
		
		public DownloadParam(){
			urlList = new ArrayList<String>();
			destList = new ArrayList<File>();
		}

		public void setParam(String url, File destPath) {
			if (TextUtils.isEmpty(url) || destPath != null)
				return;

			urlList.add(url);
			destList.add(destPath);
		}

		/**
		 * 
		 * @param folder directory to contain all file that download from urlList
		 * @param urlList  ArrayList of URL to download file from internet
		 */
		public void setParam(File folder, ArrayList<String> urlList) {
			if (!folder.isDirectory()) {
				throw new IllegalStateException(
						"must add File object thai is Directory!");
			}
			String directory = folder.getAbsolutePath();
			for (String url : urlList) {
				String filename = url.substring(url.lastIndexOf("/") + 1);
				Log.d("Download", "dir=" + directory + " filename=" + filename);

				this.urlList.add(url);
				this.destList.add(new File(directory, filename));
			}
		}

	}
	
	int fileCount = 0;

	@Override
	protected ArrayList<File> doInBackground(DownloadParam... params) {
		ArrayList<File> successFileList = new ArrayList<File>();
		for (DownloadParam downloadParam : params) {
			fileCount = 0;
			int size = downloadParam.urlList.size();
			for (int i = 0; i < size; i++) {
				fileCount++;
				File success = download(downloadParam.urlList.get(i),
						downloadParam.destList.get(i));
				if (success != null) {
					Log.d("donwload", "success download to "+success.getAbsolutePath());
					successFileList.add(success);
				}else
					Log.d("donwload", "fail to  download "+ downloadParam.urlList.get(i));
				//publishProgress(size,i);
			}
		}
		return successFileList;
	}

	/**
	 * 
	 * @param urlString  url to download file
	 * @param file destination file to write after download file complete
	 * @return Destination File if download success, null if otherwise
	 */
	private File download(String urlString, File file) {
		try {
			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();
			connection.connect();
			// this will be useful so that you can show a typical 0-100%
			// progress bar
			int fileLength = connection.getContentLength();

			if (!file.exists()) {
				file.createNewFile();
			}
			// download the file
			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(file);

			byte data[] = new byte[1024];
			long total = 0;
			int count;
			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				publishProgress(fileCount, 100, (int) (total * 100 / fileLength));
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();

			return file;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		if (mProgress != null) {
			mProgress.setMax(values[1]);
			mProgress.setProgress(values[2]);
		}
		
		if(mProgressListener != null){
			mProgressListener.onProgressUpdate(values[0], values[1], values[2]);
		}
	}

	@Override
	protected void onPostExecute(ArrayList<File> result) {
		super.onPostExecute(result);
		if (mListener != null)
			mListener.onTaskFinish(result);
	}

	public interface OnFinishListener {
		/**
		 * @param result ArrayList of successful download file
		 */
		public void onTaskFinish(ArrayList<File> result);
	}
	
	public interface OnProgressUpdateListener{
		/**
		 * @param fileNumber current download file number
		 * @param max max valuse of process
		 * @param process current finished process
		 */
		public void onProgressUpdate(int fileNumber, int max, int process);
	}

}
