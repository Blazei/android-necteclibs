/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class CheckBoxesGroup extends LinearLayout {

	private String[] previewArray = new String[] { "1:CheckBoxesGroup Item 1",
			"2:CheckBoxesGroup Item 2", "3:CheckBoxesGroup Item 3", };

	public static final int SINGLE_CHOICE = 1;
	public static final int MULTI_CHOICE = 2;

	private Context mContext;
	

	private int mMode = MULTI_CHOICE;
	private final OnCheckedChangeListener mSingleChoiceListerner = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			if (mMode == SINGLE_CHOICE) {
				if (isChecked) {
					setChecked(false);
					buttonView.setChecked(true);
				}
			}
			
			if(mChkChangeListener != null){
				mChkChangeListener.onCheckedChange((String)buttonView.getTag(), isChecked);
			}
		}
	};

	/**
	 * @param context
	 * @param attrs
	 */
	public CheckBoxesGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;

		this.setOrientation(VERTICAL);
		if (isInEditMode()) {
			setArray(previewArray);
		}
	}

	/**
	 * set mode of checkbox Group between SINGLE_CHOICE or MULTI_CHOICE default
	 * is MULTI_CHOICE
	 * 
	 * @param mode
	 * 
	 * @since 1.0
	 */
	public void setMode(int mode) {
		switch (mode) {
		case SINGLE_CHOICE:
		case MULTI_CHOICE:
			mMode = mode;
			break;
		default:
			throw new InvalidParameterException("Mode not support");
		}
	}

	String[] mArray;
	HashMap<String, CheckBox> mItem;

	/**
	 * 
	 * @param stringArrayId
	 *            id of Format Array
	 * 
	 * @since 1.0
	 */
	public void setArray(int stringArrayId) {
		this.setArray(getResources().getStringArray(stringArrayId));
	}
	

	/**
	 * String format is "key:lable"
	 * 
	 * <pre>
	 * example
	 *  	"1:CheckBoxesGroup Item 1",
	 * 		"2:CheckBoxesGroup Item 2",
	 * 		"3:CheckBoxesGroup Item 3",
	 * </pre>
	 * 
	 * @param array
	 *            String array format for set as CheckboxedGroup item
	 * 
	 * @since 1.0
	 */
	public void setArray(String[] array) {
		mArray = array;
		mItem = new HashMap<String, CheckBox>();
		
		this.removeAllViews();
		for (String item : mArray) {
			if (!item.matches(".*:.{1,}"))
				throw new InvalidParameterException("Array Item invalid format");

			String key = item.substring(0, item.indexOf(":"));
			String label = item.substring(item.indexOf(":") + 1);

			CheckBox cb = new CheckBox(mContext);
			cb.setText(label);
			cb.setTag(key);

			if (mMode == SINGLE_CHOICE)
				cb.setOnCheckedChangeListener(mSingleChoiceListerner);

			this.addView(cb);

			mItem.put(key, cb);
		}
		
	}
	
	
	public String[] getKey(){
		ArrayList<String> keys = new ArrayList<String>();
		for (Entry<String, CheckBox> entry : mItem.entrySet()) {
			keys.add(entry.getKey());
		}
		return  keys.toArray(new String[keys.size()]);
	}


	/**
	 * for set enable or disable all item in group
	 * 
	 * @param enabled
	 * 
	 * @since 1.0
	 */
	@Override
	public void setEnabled(boolean enabled) {
		for (Entry<String, CheckBox> entry : mItem.entrySet()) {
			entry.getValue().setEnabled(enabled);
		}
		super.setEnabled(enabled);
	}

	/**
	 * for set enable or disable in group by Key
	 * 
	 * @param enabled
	 * @param keys
	 * 
	 * @since 1.0
	 */
	public void setEnabled(boolean enabled, String... keys) {
		for (String key : keys) {
			mItem.get(key).setEnabled(enabled);
		}
	}

	/**
	 * 
	 * @return true if at least one item was check, false if nothing check
	 * 
	 * @since 1.0
	 */
	public boolean isChecked() {
		for (Entry<String, CheckBox> entry : mItem.entrySet()) {
			if (entry.getValue().isChecked())
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @return String array of key of checked item
	 * 
	 * @since 1.0
	 */
	public ArrayList<String> getCheckedItem() {
		ArrayList<String> chklst = new ArrayList<String>();
		for (Entry<String, CheckBox> entry : mItem.entrySet()) {
			if (entry.getValue().isChecked())
				chklst.add(entry.getKey());
		}
		return chklst;
	}

	/**
	 * set check stage for all CheckBox item
	 * 
	 * @param checked
	 * 
	 * @since 1.0
	 */
	public void setChecked(boolean checked) {
		for (Entry<String, CheckBox> entry : mItem.entrySet()) {
			entry.getValue().setChecked(checked);
		}
	}

	/**
	 * set check stage by key
	 * 
	 * @param checked
	 * @param keys
	 *            key array to check or uncheck
	 * 
	 * @since 1.0
	 */
	public void setChecked(boolean checked, String... keys) {
		for (String key : keys) {
			mItem.get(key).setChecked(checked);
		}
	}

	OnCheckedItemChangeListener mChkChangeListener;

	public void setOnCheckedItemChangeListener(OnCheckedItemChangeListener listener) {
		mChkChangeListener = listener;
	}

	public static interface OnCheckedItemChangeListener {
		public void onCheckedChange(String key, boolean isChecked);
	}
	
	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		SavedState ss = new SavedState(superState);
		
		ss.arry = mArray;
		
		ArrayList<String> chkItem = getCheckedItem();
		ss.checked = chkItem.toArray(new String[chkItem.size()]);
		
		return ss;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		
		setArray(ss.arry);
		setChecked(true, ss.checked);
		
	}
	
	
	static class SavedState extends BaseSavedState {
		protected String[] arry,checked;
		private int arrySize, chkSize;
		
		
		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			arrySize = in.readInt();
			arry = new String[arrySize];
			in.readStringArray(arry);
			
			chkSize = in.readInt();
			checked = new String[chkSize];
			in.readStringArray(checked);
			
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeInt(arry.length);
			out.writeStringArray(arry);
			out.writeInt(checked.length);
			out.writeStringArray(checked);
			
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}

}
