/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.form.NTField;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author N. Choatravee
 *
 */


public class NTCheckBoxField extends NTField {
	
	String mLabel;
	
	/**
	 * @param view
	 * @param Column
	 */
	public NTCheckBoxField(TextView view, String Column) {
		super(view, Column);
	}
	
	/**
	 * 
	 * @param column
	 * @param label
	 */
	public NTCheckBoxField(String column, String label, String[] choice){
		super(column, label);
		mLabel = choice[0];
	}

	/**
	 * 
	 * @return true as string when this field checked or false as string when this field not checked 
	 * 		   
	 */
	@Override
	public String getValue() {
		return String.valueOf(((CheckBox) mView).isChecked());
	}

	/**
	 * 
	 * @param value true or false as string
	 * 		   
	 */
	@Override
	public void setValue(String value) {
		if(TextUtils.isEmpty(value)){
			throw new IllegalArgumentException("Argument must not null or empty");
		}
		
		value = value.toLowerCase();
		if(!value.equals("true") && !value.equals("false")){
			throw new IllegalArgumentException("String argument must be true or false");
		}
		
		Boolean checked = Boolean.valueOf(value);
		((CheckBox) mView).setChecked(checked);
	}

	@Override
	public View onCreateView(Context context) {
		 CheckBox chkBox = new CheckBox(context, null);
		 chkBox.setText(mLabel.substring(mLabel.indexOf(":")+1));
		 return chkBox;
	}
}
