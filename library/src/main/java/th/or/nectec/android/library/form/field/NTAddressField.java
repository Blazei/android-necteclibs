/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import java.util.ArrayList;

import th.or.nectec.android.library.form.IMultiColumnField;
import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.provider.columns.AddressColumns;
import th.or.nectec.android.library.util.StringUtils;
import th.or.nectec.android.library.widget.address.AddressView;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;

/**
 * 
 * Custom NTField for use with AddressView
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTAddressField extends NTField implements IMultiColumnField, AddressColumns {

	AddressView view;

	/**
	 * 
	 * @param view
	 *            AddressView to bind this field instance
	 * @param addressCode
	 *            Columns name of address code can be NULL
	 * @param subdistrict
	 *            Columns name of subdistrict can't be NULL
	 * @param district
	 *            Columns name of district can't be NULL
	 * @param province
	 *            Columns name of province can't be NULL
	 */
	public NTAddressField(AddressView view, String addressCode,
			String subdistrict, String district, String province) {
		super(view, addressCode);

		this.view = view;

		codeColumn = addressCode;
		sColumn = subdistrict;
		dColumn = district;
		pColumn = province;
	}
	
	public NTAddressField(AddressView view){
		super(view, ADDRESS_CODE);
		
		codeColumn = ADDRESS_CODE;
		sColumn = SUBDISTRICT;
		dColumn = DISTRICT;
		pColumn = PROVINCE;
	}
	
	public NTAddressField(String col, String lable){
		super(col, lable);
		codeColumn = col + ADDRESS_CODE;
		sColumn = col + SUBDISTRICT;
		dColumn = col + DISTRICT;
		pColumn = col + PROVINCE;
	}
	
	public NTAddressField(String lable){
		super(ADDRESS_CODE, lable);
		codeColumn = ADDRESS_CODE;
		sColumn = SUBDISTRICT;
		dColumn = DISTRICT;
		pColumn = PROVINCE;
	}
	
	public NTAddressField(String addressCode,
			String subdistrict, String district, String province, String lable){
		super(addressCode, lable);
		
		codeColumn = addressCode;
		sColumn = subdistrict;
		dColumn = district;
		pColumn = province;
	}

	private String codeColumn, sColumn, dColumn, pColumn;

	@Deprecated
	@Override
	public String getValue() {
		return null;
	}

	@Deprecated
	@Override
	public void setValue(String value) {
		// do nothing
	}

	@Override
	public void setValueByCursor(Cursor c) {
		String code = null;
		if (!TextUtils.isEmpty(codeColumn)) {
			code = c.getString(c.getColumnIndex(codeColumn));
		}
		String p = c.getString(c.getColumnIndex(pColumn));
		String d = c.getString(c.getColumnIndex(dColumn));
		String s = c.getString(c.getColumnIndex(sColumn));

		view.setLocale(code, s, d, p);

	}

	/**
	 * get Content value from AddressView
	 * 
	 * NOTE value may have no key AddressCode if you don't set it at constructor
	 */
	@Override
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();

		if (!TextUtils.isEmpty(codeColumn)) {
			cv.put(codeColumn, view.getAddressCode());
		}
		cv.put(pColumn, view.getProvince());
		cv.put(dColumn, view.getDistrict());
		cv.put(sColumn, view.getSubdistrict());
		return cv;
	}

	@Override
	public boolean isValid() {
		if (StringUtils.isEmpty(view.getProvince())) {
			mInvalidMessage = "ยังไม่ได้เลือกที่อยู่";
			return false;
		}
		return super.isValid();
	}

	@Override
	public String[] getColumns() {
		ArrayList<String> columns = new ArrayList<String>();
		columns.add(pColumn);
		columns.add(dColumn);
		columns.add(sColumn);

		if (!TextUtils.isEmpty(codeColumn))
			columns.add(codeColumn);

		return  columns.toArray(new String[columns.size()]);
	}

	@Override
	public View onCreateView(Context context) {
		AddressView av = new AddressView(context, null);
		//parent.addView(av);
		
		return av;
	}

}
