/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.generate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class Value {

	private String value;
	private String label;
	private String defaultProperty;

	public Value() {
	}

	public Value(JSONObject json) {

		this.value = json.optString("value");
		this.label = json.optString("label");
		//use label as value when value undefined.
		if(TextUtils.isEmpty(value)){
			value = label;
		}
		this.defaultProperty = json.optString("default");

	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDefaultProperty() {
		return this.defaultProperty;
	}

	public void setDefaultValue(String defaultProperty) {
		this.defaultProperty = defaultProperty;
	}

	public static Value[] parseValueArray(String jsonArrayString) {

		try {
			JSONArray arry = new JSONArray(jsonArrayString);

			Value[] values = new Value[arry.length()];
			for (int i = 0; i < arry.length(); i++) {
				values[i] = new Value(arry.optJSONObject(i));
			}
			return values;

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String toString() {
		return this.value + ":" + this.label;
	}

	public static String[] toString(Value... v) {
		int length = v.length;
		String[] str = new String[length];
		for (int i = 0; i < length; i++) {
			str[i] = v[i].toString();
		}
		return str;
	}
}
