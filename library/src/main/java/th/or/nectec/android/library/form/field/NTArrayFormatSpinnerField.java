/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.ArrayFormatSpinner;
import android.content.Context;
import android.view.View;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTArrayFormatSpinnerField extends NTField {

	/**
	 * @param view
	 * @param Column
	 */
	public NTArrayFormatSpinnerField(ArrayFormatSpinner view, String Column) {
		super(view, Column);

	}

	@Override
	public String getValue() {
		return  ((ArrayFormatSpinner) mView).getSelectionId();
	}

	@Override
	public void setValue(String value) {
		((ArrayFormatSpinner) mView).setSelection(value);
	}

	@Override
	public View onCreateView(Context context) {
		ArrayFormatSpinner afs = new ArrayFormatSpinner(context, null);
		//parent.addView(afs);
		
		return afs;
	}



}
