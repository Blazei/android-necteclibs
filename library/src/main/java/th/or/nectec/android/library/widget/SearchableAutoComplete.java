/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AutoCompleteTextView;

import th.or.nectec.android.library.R;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class SearchableAutoComplete extends AutoCompleteTextView implements OnTouchListener {


	private Drawable xD;
	OnSearchListener mListener = new OnSearchListener() {
		
		@Override
		public void onSearch() {
			
			
		}
	};
	
	public void setOnSearchListener(OnSearchListener listener){
		mListener = listener;
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public SearchableAutoComplete(Context context, AttributeSet attrs) {
		super(context, attrs);

		xD = getCompoundDrawables()[0];
		if (xD == null) {
			xD = getResources().getDrawable(getDefaultSearchIconId());
		}
		xD.setBounds(0, 0, xD.getMinimumWidth(), xD.getMinimumHeight());
		
		setCompoundDrawables(xD, getCompoundDrawables()[1], 
				getCompoundDrawables()[2], getCompoundDrawables()[3]);
		
		setCompoundDrawablePadding(12);
		if(!isInEditMode()){
			//init();
			super.setOnTouchListener(this);
		}
	}
	
	
	private int getDefaultSearchIconId() {
		//int id = getResources().getIdentifier("ic_clear", "drawable", "android");
		//if (id == 0) {
		int id = R.drawable.ic_search_light;
		//}
		return id;
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		boolean tappedX = event.getX() < ( getPaddingLeft() + xD
				.getIntrinsicWidth());
		if (tappedX) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				if (mListener != null) {
					mListener.onSearch();
				}
				showDropDown();
			}
			return true;
		}else
			return false;
	}
	
	public static interface OnSearchListener{
		public void onSearch();
	}
	
	



}
