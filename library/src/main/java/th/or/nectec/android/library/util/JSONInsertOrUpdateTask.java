/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;


/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * 
 */
public class JSONInsertOrUpdateTask extends
		AsyncTask<JSONInsertOrUpdateTask.Params, Integer, String> {

	public static final String TAG = "JSONInsertTask";
	JSONObject mSQL;
	ProgressBar mProgress;
	OnFinishListener mListener;
	Context mContext;
	SQLiteOpenHelper mDbHelper;
	
	public static final int CURRENT_VERSION = 2;
	int mVersion = 1;

	public JSONInsertOrUpdateTask(Context context, SQLiteOpenHelper dbHelper, JSONObject sql,
			ProgressBar progress, OnFinishListener listener) {
		mContext = context;
		mSQL = sql;
		mDbHelper = dbHelper;
		mProgress = progress;
		mListener = listener;
	}
	
	public void update(){
		mVersion = CURRENT_VERSION;
	}

	public static class Params {
		public Uri uri;
		public String tablename;
		public String[] intColumn;
		public String[] strColumn;
		public String whereCause;
		public String[] argsColumn;
		private String addCol;
		private String addValue;

		public Params() {

		}
		
		public void setAdditionalValue(String col, String value){
			if(!TextUtils.isEmpty(col) && !TextUtils.isEmpty(value)){
				addCol = col;
				addValue = value;
			}
		}
	}

	@Override
	protected String doInBackground(Params... params) {
		int i = 0;
		int lenght = params.length;
		try{
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			String table = "";
			for (Params p : params) {
				table += p.tablename +" ";
				createTable(db, mSQL, p.uri, p.tablename, p.intColumn, p.strColumn,
						p.whereCause, p.argsColumn, p.addCol, p.addValue);
				i++;
				
			}
			db.close();

			return table.trim();
			
		}catch(SQLiteException sqlex){
			sqlex.printStackTrace();
			return null;
		}catch(NullPointerException nex){
			nex.printStackTrace();
			return null;
		}
		
	}

	public void createTable(SQLiteDatabase db,JSONObject sql,Uri uri, String tablename,
			String[] intColumn, String[] strColumn, String whereClause,
			String[] argsColumn, String addCol, String addValue) {
		try {
			JSONArray table;
			
			switch(mVersion){
			case CURRENT_VERSION:
				
				table = sql.optJSONObject(tablename).optJSONArray("data");
				break;
			default:
				table = sql.optJSONArray(tablename);
				break;
			}
			
			if(table == null){
				return;
			}
			JSONObject record;
			int max = table.length();
			Log.d(TAG,"record="+ max);
			publishProgress(0, max);

			db.beginTransaction();
			for (int i = 0; i < max; i++) {
				
				record = table.getJSONObject(i);
				//Log.d("MAX",""+record.toString());
				ContentValues cv = new ContentValues();
				if (intColumn != null) {
					for (String col : intColumn) {
						int v = record.optInt(col, -99);
						if (v != -99)
							cv.put(col, v);
					}
				}
				if (strColumn != null) {
					for (String col : strColumn) {
						String v = record.optString(col);
						if (!TextUtils.isEmpty(v) && !v.equalsIgnoreCase("null")){
							cv.put(col, v);
						}else if (v.equalsIgnoreCase("null")){
							cv.put(col, "");
							//Log.d(tablename, "not found col="+col);
						}else{
							//Log.d(tablename, "not found col="+col);
						}
					}
				}
				
				if(!TextUtils.isEmpty(addCol)){
					cv.put(addCol, addValue);
				}


				String[] whereArgs = new String[argsColumn.length];
				for (int w = 0; w < argsColumn.length; w++) {
					whereArgs[w] = record.optString(argsColumn[w]);

				}
				
				//Log.d(tablename, whereClause + " argument cv="+cv.toString());
				//int affected = mContext.getContentResolver().update(uri, cv, whereClause, whereArgs);
				int affected = db.update(tablename, cv, whereClause, whereArgs);
				if (affected < 1) {
					db.insert(tablename, null, cv);
				}
				
				publishProgress(i * 100 / max, max);
			}
			db.setTransactionSuccessful();
			db.endTransaction();
			
			mContext.getContentResolver().notifyChange(uri, null);
			
			
			Log.d(TAG, "Json Inserted or updated table " + tablename);
		} catch (JSONException ex) {
			Log.e(TAG, "error while createTable " + tablename);
			ex.printStackTrace();
		} catch (NullPointerException n) {
			Log.e(TAG, "null error while createTable " + tablename);
			n.printStackTrace();
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		if (mProgress != null) {
			mProgress.setMax(values[1]);
			mProgress.setProgress(values[0]);
		}
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		mSQL = null;
		mProgress = null;
		if (mListener != null) {
			mListener.onTaskFinish(result);
		}
		mListener = null;
		mContext = null;
	}

	public interface OnFinishListener {
		public void onTaskFinish(String result);
	}
}
