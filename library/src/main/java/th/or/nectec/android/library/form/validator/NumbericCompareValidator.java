/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.validator;

import android.text.TextUtils;


/**
 * 
 * Compare numberic value
 *
 * @version 1.0
 * 
 * @author N. Choatravee
 *
 */
public class NumbericCompareValidator extends AbstractValidator {

	public static final int EQUAL = 0;
	public static final int LESS_THAN = 1;
	public static final int LESS_THAN_EQUAL = 2;
	public static final int GREATER_THAN = 3;
	public static final int GREATER_THAN_EQUAL = 4;
	public static final int NOT_EQUAL = 5;
	
	int mCompareMode;
	Double mValue1, mCompareValue;
	String mErrMessage;
	
	/**
	 * @param mode 
	 * Compare mode (Example: NumbericCompareValidator.EQUAL)
	 * 
	 * @param compareValue
	 */
	public NumbericCompareValidator(int mode, Double compareValue) {
		this(mode, compareValue, null);
	}
	
	/**
	 * @param mode 
	 * Compare mode (Example: NumbericCompareValidator.EQUAL)
	 * 
	 * @param compareValue
	 * @param errorMessage
	 */
	public NumbericCompareValidator(int mode, Double compareValue, String errorMessage) {
		super(null);
		mCompareMode = mode;
		mCompareValue = compareValue;
		mErrMessage = errorMessage;
	}
	
	/**
	 * @param mode 
	 * Compare mode (Example: NumbericCompareValidator.EQUAL)
	 * 
	 * @param compareValue
	 * @param errorMessage
	 */
	public NumbericCompareValidator(int mode, int compareValue, String errorMessage) {
		this(mode, (double)compareValue, errorMessage);
	}

	@Override
	public boolean isValid(String value) throws ValidatorException {
		
		double val = Double.valueOf(value);
		switch (mCompareMode) {
		case EQUAL:	
			return val == mCompareValue;
		case LESS_THAN:
			return val < mCompareValue;
		case LESS_THAN_EQUAL:
			return val <= mCompareValue;
		case GREATER_THAN:
			return val > mCompareValue;
		case GREATER_THAN_EQUAL:
			return val >= mCompareValue;
		case NOT_EQUAL:
			return val != mCompareValue;
		default:
			return true;
		}
	}

	@Override
	public String getMessage() {
		if(!TextUtils.isEmpty(mErrMessage)){
			return mErrMessage;
		}else{
			switch (mCompareMode) {
			case EQUAL:	
				return "ค่าที่เปรียบเทียบต้องเท่ากับ "+mCompareValue;
			case LESS_THAN:
				return "ค่าที่เปรียบเทียบต้องน้อยกว่า "+mCompareValue;
			case LESS_THAN_EQUAL:
				return "ค่าที่เปรียบเทียบต้องน้อยกว่าหรือเท่ากับ "+mCompareValue;
			case GREATER_THAN:
				return "ค่าที่เปรียบเทียบต้องมากกว่า "+mCompareValue;
			case GREATER_THAN_EQUAL:
				return "ค่าที่เปรียบเทียบต้องมากกว่าหรือเท่ากับ "+mCompareValue;
			case NOT_EQUAL:
				return "ค่าที่เปรียบเทียบต้องไม่เท่ากับ "+mCompareValue;
			default:
				return "ไม่ทราบ";
			}
		}

	}

}
