package th.or.nectec.android.library.widget.address;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import th.or.nectec.android.library.R;

public class AddressDialog extends DialogFragment {


    AutoCompleteTextView mSearchAddrView;
    EditText mSubdistrictEdit, mDistrictEdit, mProvinceEdit;
    String mAddressCode, mSubdistrict, mDistrict, mProvince;

    AddressAdapter adapter;
    Uri mAddressUri;

    public static AddressDialog newInstance(Uri uri) {
        AddressDialog f = new AddressDialog();

        Bundle args = new Bundle();
        args.putString("uri", uri.toString());

        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();

        mAddressUri = Uri.parse(args.getString("uri"));

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View view = inflater.inflate(R.layout.address_dialog, null);

        mSearchAddrView = (AutoCompleteTextView) view.findViewById(R.id.search_address);
        mSubdistrictEdit = (EditText) view.findViewById(R.id.subdistrictText);
        mDistrictEdit = (EditText) view.findViewById(R.id.districtText);
        mProvinceEdit = (EditText) view.findViewById(R.id.provinceText);

        mSubdistrictEdit.setText(mSubdistrict);
        mDistrictEdit.setText(mDistrict);
        mProvinceEdit.setText(mProvince);

        adapter = new AddressAdapter(getActivity(), null);
        mSearchAddrView.setAdapter(adapter);

        if (savedInstanceState != null) {
            mAddressCode = savedInstanceState.getString("code");
            mSubdistrict = savedInstanceState.getString("subdistrict");
            mDistrict = savedInstanceState.getString("district");
            mProvince = savedInstanceState.getString("province");
        }

        mSearchAddrView.setOnTouchListener(new OnTouchListener() {

            Drawable ClearIcon = mSearchAddrView.getCompoundDrawables()[2];

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (ClearIcon != null) {
                    boolean tappedX = event.getX() > (mSearchAddrView.getWidth() - mSearchAddrView.getPaddingRight() - ClearIcon
                            .getIntrinsicWidth());
                    if (tappedX) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            mSearchAddrView.setText("");
                            mSubdistrictEdit.setText("");
                            mDistrictEdit.setText("");
                            mProvinceEdit.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        mSearchAddrView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                // TODO Auto-generated method stub
                Cursor locale = (Cursor) listView.getAdapter().getItem(position);

                if (locale != null) {
                    mAddressCode = locale.getString(0);
                    mSubdistrict = locale.getString(1);
                    mDistrict = locale.getString(2);
                    mProvince = locale.getString(3);
                }

                mSearchAddrView.setText("");
                mSubdistrictEdit.setText(mSubdistrict);
                mDistrictEdit.setText(mDistrict);
                mProvinceEdit.setText(mProvince);
            }
        });

        return new AlertDialog.Builder(getActivity())

                .setTitle(getResources().getString(R.string.address_dialog_title))
                .setView(view)
                .setCancelable(true)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if (mSubdistrictEdit.getText().toString().isEmpty()) {
                            Toast.makeText(getActivity(), "โปรดระบุข้อมูลก่อนที่จะบันทึกข้อมูล", Toast.LENGTH_SHORT).show();
                        } else if (mListener != null) {
                            mListener.onAddressSelect(mAddressCode, mSubdistrict, mDistrict, mProvince);
                        }
                        dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                }).create();
    }

    private onAddressListener mListener;

    public void setAddress(String s, String d, String p) {
        mSubdistrict = s;
        mDistrict = d;
        mProvince = p;
    }

    public void setOnAddressSelectListener(onAddressListener listener) {
        this.mListener = listener;
    }

    public static interface onAddressListener {
        public void onAddressSelect(String code, String s, String d, String p);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("code", mAddressCode);
        outState.putString("subdistrict", mSubdistrict);
        outState.putString("district", mDistrict);
        outState.putString("province", mProvince);
    }

}
