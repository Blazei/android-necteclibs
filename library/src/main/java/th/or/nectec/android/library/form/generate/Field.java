/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.generate;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import th.or.nectec.android.library.form.NTFormGeneratorColumn;
import th.or.nectec.android.library.util.CursorHelper;
import android.database.Cursor;
import android.text.TextUtils;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class Field {

	String column;
	String[] columns;

	long id;
	Value[] values;
	String defualtValue;
	String type;
	String label;
	Boolean mandatory;
	HashMap<String, String> option;

	/**
	 * @return the field
	 */
	public String getColumnName() {
		return column;
	}

	public String[] getColumnsName() {
		return columns;
	}

	/**
	 * @return the values
	 */
	public Value[] getValues() {
		return values;
	}

	/**
	 * @return the defualtValue
	 */
	public String getDefualtValue() {
		return defualtValue;
	}
	
	public long getId(){
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	public HashMap<String, String> getOption() {
		return option;
	}

	/**
	 * @return the mandatory
	 */
	public Boolean isMandatory() {
		return mandatory;
	}
	
	public Boolean isMultiColumn(){
		if(columns != null && columns.length > 1)
			return true;
		else
			return false;
	}

	public static Field createByCursor(Cursor c) {
		Field f = new Field();

		f.id = CursorHelper.getLong(c, NTFormGeneratorColumn.ID);
		
		int colIndex = c.getColumnIndex(NTFormGeneratorColumn.COLUMN);
		if(colIndex >= 0)
			f.column = CursorHelper.getString(c, NTFormGeneratorColumn.COLUMN);

		int colsIndex = c.getColumnIndex(NTFormGeneratorColumn.COLUMNS);
		if(colsIndex >= 0){
			String columns = CursorHelper.getString(c,
					NTFormGeneratorColumn.COLUMNS);
			if (!TextUtils.isEmpty(columns)) {
				try {
					JSONArray columnsJSON = new JSONArray(columns);
					f.columns = new String[columnsJSON.length()];
					for (int i = 0; i < columnsJSON.length(); i++) {
						f.columns[i] = columnsJSON.optString(i);
					}
				} catch (JSONException jex) {
					jex.toString();
				}
			}
		}
		
		if(f.column == null && f.columns == null){
			f.column = String.valueOf(f.id);
		}
		
		f.type = CursorHelper.getString(c, NTFormGeneratorColumn.TYPE);
		f.label = CursorHelper.getString(c, NTFormGeneratorColumn.LABEL);
		f.mandatory = CursorHelper
				.getString(c, NTFormGeneratorColumn.MANDATORY).equals("yes") ? true
				: false;

		String value = c.getString(c
				.getColumnIndex(NTFormGeneratorColumn.VALUE));
		if(!TextUtils.isEmpty(value))
			f.values = Value.parseValueArray(value);

//		String optionStr = CursorHelper.getString(c,
//				NTFormGeneratorColumn.OPTION);
//		if (!TextUtils.isEmpty(optionStr))
//			f.option = parseHashMap(optionStr);

		return f;
	}

	private static HashMap<String, String> parseHashMap(JSONObject json) {
		HashMap<String, String> hash = new HashMap<String, String>();
		Iterator<String> keys = json.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			try {
				hash.put(key, json.getString(key));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return hash;
	}

	private static HashMap<String, String> parseHashMap(String json) {
		try {
			return parseHashMap(new JSONObject(json));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

}
