/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.widget.SearchableAutoComplete;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class NTAutoCompleteTextField extends NTTextField {

	/**
	 * @param column
	 * @param label
	 */
	public NTAutoCompleteTextField(String column, String label) {
		super(column, label);
		
	}
	
	public NTAutoCompleteTextField(AutoCompleteTextView view, String Column) {
		super(view, Column);
	}
	
	@Override
	public View onCreateView(Context context) {
		 SearchableAutoComplete txt = new SearchableAutoComplete(context, null);
		 
		 if(mAdapter != null)
			 txt.setAdapter(mAdapter);
		 //parent.addView(tv);
		 return txt;
	}
	
	ArrayAdapter<String> mAdapter;
	public void setAdapter(ArrayAdapter<String> adapter){
		mAdapter = adapter;
	}
	

}
