/** ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.util.Base64;

/**
 * 
 * Helper Class with static method for manage about Hash encryption suck as MD5
 * or SHA-256
 * 
 * @version 1.0
 * @author Piruin Panichphol
 * 
 * @since Family Folder Collector 1.75
 * 
 */
public class MessageDigester {

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static byte[] getSha256(String input) {
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-256");
			md.update(input.getBytes());
			return md.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getSha256String(String input) {
		byte[] bSha256 = getSha256(input);
		if (bSha256 != null) {

			return Base64.encodeToString(bSha256, Base64.DEFAULT).trim();
		} else {
			return null;
		}
	}

	public static byte[] getMD5(String input) {
		try {
			byte[] bytesOfMessage = input.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(bytesOfMessage);
		} catch (NoSuchAlgorithmException e) {
			return null;
		} catch (UnsupportedEncodingException e) {
			return null;
		}

	}

	public static String getMd5String(String text) {
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			byte[] md5hash = new byte[32];
			md.update(text.getBytes(), 0, text.length());
			md5hash = md.digest();
			return convertToHex(md5hash).trim();
		} catch (NoSuchAlgorithmException e) {
			return null;

		}
	}

	public static String getMd5Hex(String text) {
		byte[] md5hash = MessageDigester.getMD5(text);
		return bytesToHex(md5hash);
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String getMd5String(File file){
		
		byte[] bytes = new byte[4096];
		int read = 0;
		try {
			InputStream is = new FileInputStream(file);
			
			MessageDigest digest = MessageDigest.getInstance("MD5");
			while ((read = is.read(bytes)) != -1) {
				digest.update(bytes, 0, read);
			}

			byte[] messageDigest = digest.digest();
			
			is.close();
			
			return convertToHex(messageDigest).trim();
			
		} catch (NoSuchAlgorithmException nsa) {
			return null;
		} catch (IOException io){
			return null;
		}
	}

	private static char[] hexDigits = "0123456789abcdef".toCharArray();

}
