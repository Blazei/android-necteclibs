/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.provider;

import th.or.nectec.android.library.database.sqlite.SQLiteUtils;
import th.or.nectec.android.library.provider.NTTable.RefAddress;
import th.or.nectec.android.library.provider.NTTable.RefPrename;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
class NTDatabaseOpenHelper extends SQLiteOpenHelper {

	public static final int VERSION = 1;
	public static final String NAME = "nectec-lib";

	Context mContext;

	/**
	 * @param context
	 * @param name
	 * @param factory
	 * @param version
	 */
	public NTDatabaseOpenHelper(Context context) {
		super(context, NAME, null, VERSION);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(RefAddress.CREATE_SQL);
		db.execSQL(RefPrename.CREATE_SQL);

		SQLiteUtils.writeDataFromCSV(mContext, db, "RefAddress.csv",
				RefAddress.TABLENAME, new String[] { RefAddress.ADDRESS_CODE,
						RefAddress.PROVINCE, RefAddress.DISTRICT,
						RefAddress.SUBDISTRICT, RefAddress.UPDATE_TIME });

		SQLiteUtils.writeDataFromCSV(mContext, db, "RefPrename.csv",
				RefPrename.TABLENAME, new String[] {RefPrename.CODE, RefPrename.PRENAME,
						RefPrename.PRENAME_FULL,RefPrename.GENDER, RefPrename.STATUS });
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
