/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget.prename;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import th.or.nectec.android.library.provider.NTTable.RefPrename;
import th.or.nectec.android.library.widget.CursorStringIdAdapter;
import th.or.nectec.android.library.widget.SearchListDialog;
import th.or.nectec.android.library.R;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author N. Choatravee
 * @since 1.0
 * 
 */
public class PrenameListDialog extends SearchListDialog.BaseAdapter {
	
	onPrenameListener mListener;
	
	private static final String[] PROJECTION = new String[] { 
		RefPrename.CODE+" as _id",
		RefPrename.PRENAME+" as name",
		RefPrename.PRENAME_FULL,
	};

	private static final String[] FROM = new String[] {"name", RefPrename.PRENAME_FULL};
	private static final int[] TO = new int[] { R.id.content, R.id.subcontent };

	@Override
	public CursorStringIdAdapter getBaseAdapter() {
		CursorStringIdAdapter mAdapter = new CursorStringIdAdapter(
				getActivity(), R.layout.prename_item, null,
				FROM, TO);
		return mAdapter;
	}
	

	@Override
	public Loader<Cursor> onLoadCursor(String filter) {
		Uri uri = getContentUri();
		
		String selection;
		
		if(!TextUtils.isEmpty(filter)){
			selection = "prename LIKE '%"+filter+"%' OR prename_full LIKE '%"+filter+"%' AND status='enable'";
		}else {
			selection = null;
		}
				
		CursorLoader cl = new CursorLoader(getActivity(), uri, PROJECTION,
				selection, null, null);

		return cl;
	}

	@Override
	public Uri getContentUri() {
		return RefPrename.CONTENT_URI;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		// TODO Auto-generated method stub
		super.onItemClick(adapter, view, position, id);
		
		TextView prename = (TextView) view.findViewById(R.id.content);
		String code = prename.getText().toString();
		Log.d("position",code);
		mListener.onPrenameSelect(code);
		dismiss();
	}
	
	public void setOnPrenameSelectListener(onPrenameListener listener) {
		this.mListener = listener;
	}

	public static interface onPrenameListener {
		public void onPrenameSelect(String prename);
	}
	
}
