/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.validator;

import android.text.TextUtils;


/**
 * 
 * Check number that in interval.
 * Note : Open Interval is exclude number boundary that you defined.
 *		  Close Interval is include boundary that you defined.
 *
 * @version 1.0
 * 
 * @author N. Choatravee
 *
 */
public class NumbericIntervalValidator extends AbstractValidator {

	public static final int OPEN_INTERVAL = 0;
	public static final int CLOSE_INTERVAL = 1;
	
	int mCompareMode;
	Double mStartRange, mEndRange;
	
	/**
	 * @param mode 
	 * Interval mode (Example: NumbericIntervalValidator.OPEN_INTERVAL)
	 * 
	 * @param startRange
	 * @param endRange
	 */
	public NumbericIntervalValidator(int mode, Double startRange, Double endRange) {
		super(null);
		mCompareMode = mode;
		mStartRange = startRange;
		mEndRange = endRange;
	}
	
	/**
	 * @param mode 
	 * Interval mode (Example: NumbericIntervalValidator.OPEN_INTERVAL)
	 * 
	 * @param startRange
	 * @param endRange
	 * 
	 * @param errorMessage
	 */
	public NumbericIntervalValidator(int mode, Double startRange, Double endRange, String errorMessage) {
		super(null);
		mCompareMode = mode;
		mStartRange = startRange;
		mEndRange = endRange;
		mErrorMsg = errorMessage;
	}
	
	/**
	 * @param mode 
	 * Interval mode (Example: NumbericIntervalValidator.OPEN_INTERVAL)
	 * 
	 * @param startRange
	 * @param endRange
	 * 
	 * @param errorMessage
	 */
	public NumbericIntervalValidator(int mode, int startRange, int endRange, String errorMessage) {
		super(null);
		mCompareMode = mode;
		mStartRange = (double) startRange;
		mEndRange = (double) endRange;
		mErrorMsg = errorMessage;
	}

	@Override
	public boolean isValid(String value) throws ValidatorException {
		
		double val = Double.valueOf(value);
		switch (mCompareMode) {
		case OPEN_INTERVAL:	
			return (mStartRange < val) && (val < mEndRange);
		case CLOSE_INTERVAL:
			return (mStartRange <= val) && (val <= mEndRange);
		default:
			return true;
		}
	}

	@Override
	public String getMessage() {
		if(!TextUtils.isEmpty(mErrorMsg)){
			return mErrorMsg;
		}else{
			switch (mCompareMode) {
			case CLOSE_INTERVAL:	
				return "ค่านี้ต้องอยู่ระหว่าง "+mStartRange+" ถึง "+mEndRange;
			case OPEN_INTERVAL:
				return "ค่านี้ต้องมีค่าตั้งแต่ "+mStartRange+" ถึง "+mEndRange;
			default:
				return "ไม่ทราบ";
			}
		}
	}
	
	
	
	public static NumbericIntervalValidator getMinimunValidator(int mode, double minValue){
		return new NumbericIntervalValidator(mode, minValue, Double.MAX_VALUE);
	}

}
