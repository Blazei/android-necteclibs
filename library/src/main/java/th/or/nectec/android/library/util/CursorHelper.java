/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * GAP-Thailand Project
 *
 * Copyright (C) 2013 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */
package th.or.nectec.android.library.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

/**
 * @author blaze
 * 
 */
public class CursorHelper {
	public static final String tag = "cursor-helper";

	public static String getString(Cursor c, String columnName) {
		return c.getString(c.getColumnIndex(columnName));
	}

	public static int getInt(Cursor c, String columnName) {
		return c.getInt(c.getColumnIndex(columnName));
	}
	
	public static double getDouble(Cursor c, String columnName) {
		return c.getDouble(c.getColumnIndex(columnName));
	}

	public static long getLong(Cursor c, String columnName) {
		return c.getLong(c.getColumnIndex(columnName));
	}
	
	public static void setText(Activity activity,Cursor c, String columnName, int textViewId){
		String str = CursorHelper.getString(c, columnName);
		if(!TextUtils.isEmpty(str)){
			TextView txt = (TextView)activity.findViewById(textViewId);
			txt.setText(str);
		}
	}
	
	/**
	 * Helper method to parse c data to JsonArray format
	 * 
	 * @param c cursor with data to parse
	 * @param column string array of column name to parse into jsonArray
	 * @return JsonArray from input cursor or null if cursor not moveToFirst
	 */
	public static JSONArray cursorToJsonArray(Cursor c, String[] column) {
		int colLength = column.length;

		if (c.moveToFirst()) {
			JSONArray jsonArray = new JSONArray();
			do {
				JSONObject record = new JSONObject();
				for (int i = 0; i < colLength; i++) {
					try {
						record.put(column[i], c.getString(i));
					} catch (JSONException jex) {
						jex.printStackTrace();
						continue;
					}
				}
				Log.d("cursorToJsonArray", record.toString());
				jsonArray.put(record);
			} while (c.moveToNext());

			return jsonArray;
		}
		return null;
	}
	

	public static void logData(Cursor c){
		if(!c.moveToFirst()){
			Log.d(tag, "not found any data");
			return;
		}
		Log.d(tag, c.toString());
		int columnCount = c.getColumnCount();
		do{
			String record = "";
			for(int i = 0; i < columnCount; i++){
				String data = c.getString(i);
				if(TextUtils.isEmpty(data))
					data="IS NULL";
				String tmp = c.getColumnName(i)+"="+data;
				if(i !=  columnCount -1)
					tmp += ", ";
				
				record += tmp;
			}
			Log.d(tag, record);
		}while(c.moveToNext());
		
	}
}
