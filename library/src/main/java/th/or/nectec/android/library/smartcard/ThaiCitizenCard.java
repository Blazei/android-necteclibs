package th.or.nectec.android.library.smartcard;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import android.text.TextUtils;

import com.idvision.androididcardlib.iCardUsbReader;

/**
 * Class for use with iCardUsbReader to read data from Thailand's citizen Smart-card
 * 
 * @author blaze
 *
 */
public class ThaiCitizenCard {
	private iCardUsbReader reader = null;
	private int lastError;
	private short lastSW;
	private String atrString;
	private String appletVersion;

	public ThaiCitizenCard() {
		this.reader = null;
		this.lastError = 0;
		this.lastSW = 0;
		this.atrString = null;
		this.appletVersion = null;
	}

	public int getLastError() {
		return this.lastError;
	}

	public String getCardAtrString() {
		return this.atrString;
	}

	public boolean init(iCardUsbReader rdr) {
		this.lastError = -2001;
		this.reader = rdr;

		if (this.reader == null)
			return false;
		if (!this.reader.getReaderStatus())
			return false;
		if (!this.reader.getCardStatus())
			return false;

		byte[] atr = new byte[32];
		int len = this.reader.cardConnect(atr);
		if (len == 0) {
			this.lastError = -2002;
			return false;
		}

		this.atrString = "";
		for (int i = 0; i < len; i++) {
			this.atrString += String.format("%02X ",
					new Object[] { Byte.valueOf(atr[i]) });
		}

		short sw = selectApplet();
		if ((sw & 0xFF00) != 24832) {
			this.lastError = -2003;
			return false;
		}

		byte[] buffer = new byte[16];
		if (!readBlock((byte) 0, 0, 4, buffer)) {
			this.lastError = -2004;
			return false;
		}
		try {
			this.appletVersion = new String(buffer, 0, 4, "TIS620");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		this.lastError = 0;
		return true;
	}

	public void close() {
		if (this.reader != null)
			this.reader.cardDisconnect();
	}

	public HolderProfile getHolderProfile() {

		byte[] profile = new byte[400];
		byte[] address = new byte[160];
		boolean loaded = false;
		String cardType = null;

		if (this.appletVersion.equals("0002")) {
			Arrays.fill(address, (byte) 32);
			loaded = readBlock((byte) 1, 0, 377, profile);
			loaded = readBlock((byte) 0, 4, 150, address);
			cardType = "22";
		}

		if (this.appletVersion.equals("0003")) {
			loaded = readBlock((byte) 0, 0, 377, profile);
			loaded = readBlock((byte) 0, 5497, 160, address);
			cardType = "23";
		}

		if (!loaded)
			return null;

		return new HolderProfile(profile, address);
//		String csvText = "";
//		String text = "";
//		// csvText = cardType + "|";
//
//		csvText = csvText + bytes2String(profile, 4, 13) + "|";// ID 1
//
//		text = bytes2String(profile, 17, 100); // Prefix, firstname, midname,
//												// lastname (in THAI) 5
//		text = text.replace('#', '|');
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 117, 100); // Prefix, firstname, midname,
//												// lastname (in ENG) 9
//		text = text.replace('#', '|');
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 217, 8); // Date of Birth 10
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 225, 1); // SEX 11
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 246, 100); // สถานที่ออกบัตร 12
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 359, 8); // วันออกบัตร 13
//		csvText = csvText + text + "|";
//
//		text = bytes2String(profile, 367, 8); // วันหมดอายุ 14
//		csvText = csvText + text + "|";
//
//		text = bytes2String(address, 0, 160); // ที่อยู่ 15 - 22
//		text = text.replace('#', '|');
//		csvText = csvText + text;
//
//		return csvText;
	}

	public byte[] getHolderPhoto() {
		byte[] buffer = new byte[5120];
		if ((this.appletVersion.equals("0002"))
				&& (readBlock((byte) 1, 381, 5116, buffer))) {
			return buffer;
		}

		if ((this.appletVersion.equals("0003"))
				&& (readBlock((byte) 0, 379, 5118, buffer))) {
			return buffer;
		}

		return null;
	}


	private short getResponse(byte[] buffer, int startIndex, int length) {
		byte[] apduResponse = { 0, -64, 0, 0, 64 };
		byte[] res = new byte[512];
		apduResponse[4] = ((byte) (length & 0xFF));
		int reslen = this.reader.exchangeAPDU(apduResponse, 5, res);
		if (reslen <= 0)
			return -1;

		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.put(res[(reslen - 2)]);
		bb.put(res[(reslen - 1)]);
		short lastSW = bb.getShort(0);
		if (lastSW == -28672) {
			System.arraycopy(res, 0, buffer, startIndex, reslen - 2);
		}
		return lastSW;
	}

	private short selectApplet() {
		byte[] res = new byte[32];
		int reslen = 0;
		byte[] apduSelect = { 0, -92, 4, 0, 8, -96, 0, 0, 0, 84, 72, 0, 1 };
		reslen = this.reader.exchangeAPDU(apduSelect, 13, res);
		if (reslen <= 0)
			return -1;

		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.put(res[0]);
		bb.put(res[1]);
		return bb.getShort(0);
	}

	private short readBinary(byte block, int offset, int length) {
		byte[] res = new byte[512];
		byte[] apduRead = { -128, -80, 0, 0, 2, 0, 64 };

		apduRead[2] = ((byte) (offset >>> 8 & 0xFF));
		apduRead[3] = ((byte) (offset & 0xFF));
		apduRead[6] = ((byte) (length & 0xFF));

		int reslen = this.reader.exchangeAPDU(apduRead, 7, res);
		if (reslen > 0) {
			ByteBuffer bb = ByteBuffer.allocate(2);
			bb.order(ByteOrder.BIG_ENDIAN);
			bb.put(res[0]);
			bb.put(res[1]);
			this.lastSW = bb.getShort(0);
			return this.lastSW;

		}

		return -1;
	}

	private boolean readBlock(byte block, int offset, int length, byte[] buffer) {
		int currentOffset = offset;
		int startIndex = 0;

		while (length > 0) {
			int size = length;
			if (size > 250)
				size = 250;
			short sw = readBinary(block, currentOffset, size);
			if ((sw & 0xFF00) != 24832) {
				return false;
			}
			sw = getResponse(buffer, startIndex, size);
			if (sw != -28672) {
				return false;
			}
			currentOffset += size;
			startIndex += size;
			length -= size;
		}

		return true;
	}
}