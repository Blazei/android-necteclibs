package th.or.nectec.android.library.provider.columns;

public interface SyncColumns {

	/**
	 * Sync status flag 0 (Default) = not yet sync and 1 = already sync with server
	 */
	public static final String _SYNC_STATUS = "sync_status";
}
