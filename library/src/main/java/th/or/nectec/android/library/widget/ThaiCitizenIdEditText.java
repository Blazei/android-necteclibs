/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.Log;

import th.or.nectec.android.library.util.ThaiCitizenID;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class ThaiCitizenIdEditText extends ClearableEditText {

	/**
	 * @param context
	 * @param attrs
	 */
	public ThaiCitizenIdEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFilters(new InputFilter[] { new InputFilter.LengthFilter(17), });
		setInputType(InputType.TYPE_CLASS_NUMBER);
		setKeyListener(DigitsKeyListener.getInstance(false, true));
		addTextChangedListener(idWatcher);

		if(isInEditMode()){
			setText("xxxxxxxxxxxxx");
		}
	}

	TextWatcher idWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			String rawid = s.toString().trim();
			String id = rawid.replaceAll("-", "");

			String show = "";
			for (int i = 0; i < id.length(); i++) {
				switch (i) {
				case 1:
				case 5:
				case 10:
				case 12:
					show += "-";
				default:
					show += id.charAt(i);
					break;
				}
			}

			if (!show.equals(rawid)) {
				setText(show);

				int position = length();
				Selection.setSelection(getEditableText(), position);

				if (id.length() == 13) {
					validate(id);
				} else {
					setError(null);
				}

			}
		}
	};

	private boolean validate(String id) {
		if(isInEditMode())
			return true;
		
		boolean valid = ThaiCitizenID.Validate(id);
		if (!valid) {
			setError("รหัสบัตรประชาชนไม่ถูกต้อง");
		}
		Log.d("TEXT", "id=" + getText());
		return valid;
	}

	public boolean isValidID() {
		return validate(getCitizenId());
	}


	/**
	 * 
	 * @return get citizen id without slash
	 *
	 * @since 1.0
	 */
	public String getCitizenId() {
		return getText().toString().trim().replaceAll("-", "");
	}

}
