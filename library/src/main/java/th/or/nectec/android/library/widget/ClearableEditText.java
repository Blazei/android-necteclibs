package th.or.nectec.android.library.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;

import th.or.nectec.android.library.R;

public class ClearableEditText extends EditText implements OnTouchListener,
        OnFocusChangeListener, TextWatcher {


    boolean mClearIconVisible = false;
    private Drawable xD, xDI;
    private OnClearListener listener;
    private OnTouchListener l;
    private OnFocusChangeListener f;

    public ClearableEditText(Context context) {
        super(context);
        init();
    }

    public ClearableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        xD = getCompoundDrawables()[2];
        if (xD == null) {
            xD = getResources().getDrawable(getDefaultClearIconId());
        }
        xD.setBounds(0, 0, xD.getMinimumWidth(), xD.getMinimumHeight());

        xDI = getResources().getDrawable(R.drawable.ic_clear_invisible);
        xDI.setBounds(0, 0, xDI.getMinimumWidth(), xDI.getMinimumHeight());

        if (!isInEditMode()) {
            init();
        } else {
            setClearIconVisible(true);
        }
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init();
        }
    }

    public void setOnClearListener(OnClearListener listener) {
        this.listener = listener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        this.l = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.f = f;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - xD
                    .getIntrinsicWidth());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    setText("");
                    if (listener != null) {
                        listener.onClear();
                    }
                }
                return true;
            }
        }
        if (l != null) {
            return l.onTouch(v, event);
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            boolean isVisible = !TextUtils.isEmpty(getText().toString()) ? true : false;
            setClearIconVisible(isVisible);
        } else {
            setClearIconVisible(false);
        }
        if (f != null) {
            f.onFocusChange(v, hasFocus);
        }
    }

    private void init() {

        setClearIconVisible(false);

        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(this);

    }

    private int getDefaultClearIconId() {
        //int id = getResources().getIdentifier("ic_clear", "drawable", "android");
        //if (id == 0) {
        int id = R.drawable.ic_action_clear;
        //}
        return id;
    }

    protected void setClearIconVisible(boolean visible) {
        if (mClearIconVisible == visible) {
            return;
        }
        mClearIconVisible = visible;

        Drawable x = visible ? xD : xDI;
        setCompoundDrawables(getCompoundDrawables()[0],
                getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (isFocused()) {
            boolean visible = false;
            if (text != null && text.length() > 0)
                visible = true;
            setClearIconVisible(visible);
        }
    }

    public interface OnClearListener {
        public void onClear();
    }
}