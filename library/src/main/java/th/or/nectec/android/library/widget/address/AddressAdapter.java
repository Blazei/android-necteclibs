/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget.address;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.Locale;

import th.or.nectec.android.library.provider.NTTable;
import th.or.nectec.android.library.provider.columns.AddressColumns;
import th.or.nectec.android.library.widget.TextViewHighLighter;
import th.or.nectec.android.library.R;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author N. Choatravee
 *
 */

 class AddressAdapter extends CursorAdapter{

	Cursor mCursor;
	String mFilterText;
    private ContentResolver mContent;

	public AddressAdapter(Context context, Cursor c) {
		super(context, c);
		mCursor = c;
        mContent = context.getContentResolver();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mCursor.moveToPosition(position) ? mCursor : null;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		ViewHolder holder = (ViewHolder) view.getTag();
		int mColor = Color.parseColor("#FF9900"); //light orange

		if(cursor.getString(3).equals("กรุงเทพมหานคร")){
			TextViewHighLighter.highLight(holder.subdistrict, "แขวง"+cursor.getString(1), mFilterText, mColor);
			TextViewHighLighter.highLight(holder.district, "เขต"+cursor.getString(2), mFilterText, mColor);
			TextViewHighLighter.highLight(holder.province, cursor.getString(3), mFilterText, mColor);
		}else{
			TextViewHighLighter.highLight(holder.subdistrict, "ตำบล"+cursor.getString(1), mFilterText, mColor);
			TextViewHighLighter.highLight(holder.district, "อำเภอ"+cursor.getString(2), mFilterText, mColor);
			TextViewHighLighter.highLight(holder.province, "จังหวัด"+cursor.getString(3), mFilterText, mColor);
		}

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub

		LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = vi.inflate(R.layout.address_dialog_item, null);

		ViewHolder holder = new ViewHolder();
		holder.subdistrict = (TextView) v.findViewById(R.id.subdistrictText);
		holder.district = (TextView) v.findViewById(R.id.districtText);
		holder.province = (TextView) v.findViewById(R.id.provinceText);
		v.setTag(holder);

		return v;
	}

	private class ViewHolder {
		TextView subdistrict,district,province;
	}

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {

        if (getFilterQueryProvider() != null) {
            return getFilterQueryProvider().runQuery(constraint);
        }

        String selection = null;
        if (constraint != null) {

            mFilterText = constraint.toString();
            selection = "subdistrict LIKE '"+mFilterText+"%' OR district LIKE '"+mFilterText+"%' OR province LIKE '"+mFilterText+"%'";
        }

        String[] refProjection = new String[]{
                AddressColumns.ADDRESS_CODE+" as _id",
                AddressColumns.SUBDISTRICT,
                AddressColumns.DISTRICT,
                AddressColumns.PROVINCE};

        mCursor = mContent.query(NTTable.RefAddress.CONTENT_URI, refProjection,
                selection, null,
                null);
        return mCursor;
    }

}


