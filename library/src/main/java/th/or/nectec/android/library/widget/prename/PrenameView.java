/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget.prename;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import th.or.nectec.android.library.provider.NTTable.RefAddress;
import th.or.nectec.android.library.provider.NTTable.RefPrename;
import th.or.nectec.android.library.widget.prename.PrenameListDialog.onPrenameListener;

/**
 * 
 * Address View is Button in EditText skin. open AddressDialog for easy and secure way
 * insert address data from user whenever user click on this view 
 * <pre>
 * HOW TO USE
 * 	1.copy asset/RefAddress.csv in NectecLibrary to asset folder of application
 * 	2.copy provider tag of NTProvider in NectecLibrary AndroidManifest.xml
 * 		 to AndroidManifest.xml of application project
 * 	3.you can call onPrenameChangeListener if you want to get data change intermediately (example: check condition of gender).
 * 	4.have fun!
 * </pre>
 * 
 * @version 1.0
 * 
 * @author N. Choatravee
 * 
 */
public class PrenameView extends Button implements onPrenameListener {
	
	onPrenameChangeListener mListener;

	public PrenameView(Context context, AttributeSet attrs) {
		super(context, attrs, android.R.attr.editTextStyle);
//
//		if (!ViewUtils.isSetBackground(context, attrs, 0)) {
//
//			TypedArray att = context.getTheme().obtainStyledAttributes(
//					new int[] { android.R.attr.editTextBackground });
//			try {
//				Drawable d = att.getDrawable(0);
//				this.setBackgroundDrawable(d);
//				this.setFocusable(true);
//
//			} finally {
//				att.recycle();
//			}
//		}
		
		if (context instanceof Activity) {
			Activity act = (Activity) context;
			initialize(act, RefAddress.CONTENT_URI);
		}
	}

	private Activity mActivity;
	private Uri mUri;
	private PrenameListDialog mDialog;

	private void initialize(Activity activity, Uri uri) {
		mActivity = activity;
		mUri = uri;

		mDialog = new PrenameListDialog();
		mDialog.setOnPrenameSelectListener(this);

	}

	String tag = "prename_dialog";


	@Override
	public boolean performClick() {
		boolean handle = false;
		this.requestFocus();
		if (mActivity != null && mUri != null) {
			FragmentManager fm = mActivity.getFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			Fragment prev = fm.findFragmentByTag(tag);

			if (prev != null)
				ft.remove(prev);

			// ft.addToBackStack(null);

			if (mDialog != null) {
				mDialog.setOnPrenameSelectListener(this);
				mDialog.show(fm, tag);
				handle = true;
			}
			ft.commit();
		}
		return handle;
	}

	/**
	 * @return the prename
	 */
	public String getPrename() {
		return prename;
	}

	/**
	 * @return the full prename
	 */
	public String getFullPrename() {
		return full_prename;
	}
	
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * 
	 * @param prename
	 *            value of prename to set
	 * 
	 * @since 1.0
	 */
	public void setPrename(String prename) {
		
		if(!TextUtils.isEmpty(prename)){
			Cursor findPrenameInfo = getContext().getContentResolver().query(
					RefPrename.CONTENT_URI, new String[]{RefPrename.PRENAME, RefPrename.PRENAME_FULL, RefPrename.GENDER}, 
					RefPrename.PRENAME+"=? OR "+RefPrename.PRENAME_FULL+"=?",
					new String[]{prename}, null);
			if(findPrenameInfo.moveToNext()){
				this.prename = findPrenameInfo.getString(0);
				this.full_prename = findPrenameInfo.getString(1);
				this.gender = findPrenameInfo.getString(2);
			}
		}else{
			return;
		}

		this.setText(prename);
		mListener.onPrenameChange(this.prename, gender);
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		SavedState ss = new SavedState(superState);
		ss.gender = this.gender;
		ss.full_prename = this.full_prename;
		ss.prename = this.prename;

		return ss;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());

		onPrenameSelect(ss.prename);
	}

	String prename, full_prename, gender;

	static class SavedState extends BaseSavedState {
		String prename, full_prename, gender;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			this.gender = in.readString();
			this.full_prename = in.readString();
			this.prename = in.readString();
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeString(this.gender);
			out.writeString(this.full_prename);
			out.writeString(this.prename);
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};

	}

	@Override
	public void onPrenameSelect(String prename) {
		// TODO Auto-generated method stub
		setPrename(prename);
	}
	
	public void setOnPrenameChangeListener(onPrenameChangeListener listener) {
		this.mListener = listener;
	}

	public static interface onPrenameChangeListener {
		public void onPrenameChange(String prename, String gender);
	}
}
