/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * 
 */
public class JSONInsertTask extends
		AsyncTask<JSONInsertTask.Params, Integer, String> {

	public static final String TAG = "JSONInsertTask";
	Context mContext;
	JSONObject mSQL;
	ProgressBar mProgress;
	OnFinishListener mListener;
	SQLiteOpenHelper mDbHelper;

	public JSONInsertTask(Context context,SQLiteOpenHelper dbHelper, JSONObject sql,
			ProgressBar progress, OnFinishListener listener) {
		mContext = context;
		mSQL = sql;
		mDbHelper = dbHelper;
		mProgress = progress;
		mListener = listener;
	}

	public static class Params {
		public Uri uri;
		public String tablename;
		public String[] intColumn;
		public String[] strColumn;

		public Params() {

		}
	}

	@Override
	protected String doInBackground(Params... params) {
		int i = 0;
		int lenght = params.length;
		SQLiteDatabase db = mDbHelper
				.getWritableDatabase();
		for (Params p : params) {
			createTable(db, mSQL, p.uri, p.tablename, p.intColumn, p.strColumn);
			i++;
			publishProgress(i * 100 / lenght);
		}
		db.close();
		return null;
	}

	public void createTable(SQLiteDatabase db, JSONObject sql, Uri uri,
			String tablename, String[] intColumn, String[] strColumn) {
		try {

			JSONArray table = sql.optJSONArray(tablename);
			if (table == null) {
				return;
			}

			JSONObject record;
			int max = table.length();

			db.beginTransaction();
			for (int i = 0; i < max; i++) {
				record = table.getJSONObject(i);
				ContentValues cv = new ContentValues();
				if (intColumn != null) {
					for (String col : intColumn) {
						int v = record.optInt(col, -99);
						if (v != -99)
							cv.put(col, v);
					}
				}
				if (strColumn != null) {
					for (String col : strColumn) {
						String v = record.optString(col);
						if (!TextUtils.isEmpty(v))
							cv.put(col, v);
					}
				}

				db.insert(tablename, null, cv);
			}

			db.setTransactionSuccessful();
			db.endTransaction();

			mContext.getContentResolver().notifyChange(uri, null);

			Log.d(TAG, "created table " + tablename);
		} catch (JSONException ex) {
			Log.e(TAG, "error while createTable " + tablename);
			ex.printStackTrace();
		} catch (NullPointerException n) {
			Log.e(TAG, "error while createTable " + tablename);
			n.printStackTrace();
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		if (mProgress != null) {
			mProgress.setProgress(values[0]);
		}
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		if (mListener != null) {
			mListener.onTaskFinish(result);
		}
	}

	public interface OnFinishListener {
		public void onTaskFinish(String result);
	}
}
