package th.or.nectec.android.library.smartcard;

import android.text.TextUtils;

import java.io.UnsupportedEncodingException;

/**
 * Created by N. Choatravee on 16/1/2558.
 */
public class HolderProfile {

    byte[] profileByte;
    byte[] addressByte;

    public HolderProfile(byte[] profile, byte[] address) {
        this.profileByte = profile;
        this.addressByte = address;
    }

    private Address address;
    private Name thaiName, engName;
    private String version, id, birthday, sex, issueDate, expireDate, issueLocation;

    public Address getAddress() {
        String add = bytes2String(this.addressByte, 0, 160);
        address = new Address(add.split("\\#"));
        return address;
    }

    public Name getThaiName() {
        String text = bytes2String(profileByte, 17, 100);
        thaiName = new Name(text.split("\\#"));
        return thaiName;
    }

    public Name getEngName() {
        String text = bytes2String(profileByte, 117, 100);
        engName = new Name(text.split("\\#"));
        return engName;
    }

    public String getCardVersion(){
        version = bytes2String(profileByte, 0, 4);
        return version;
    }

    public String getId() {
        id = bytes2String(profileByte, 4, 13);
        return id;
    }

    public String getBirthday() {
        birthday = bytes2String(profileByte, 217, 8);
        return birthday;
    }

    public String getSex() {
        sex = bytes2String(profileByte, 225, 1);
        return sex;
    }

    public String getIssueDate() {
        issueDate = bytes2String(profileByte, 359, 8);
        return issueDate;
    }

    public String getExpireDate() {
        expireDate = bytes2String(profileByte, 367, 8);
        return expireDate;
    }

    public String getIssueLocation() {
        issueLocation = bytes2String(profileByte, 246, 100);
        return issueLocation;
    }

    private String bytes2String(byte[] buffer, int startIndex, int length) {
        byte[] bytesInput = new byte[length];
        System.arraycopy(buffer, startIndex, bytesInput, 0, length);
        String textOut = null;
        try {
            textOut = new String(bytesInput, "TIS620");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (textOut != null) {
            textOut = textOut.trim();
        }
        return textOut;
    }

    public class Address {
        private String[] addressArray;

        public Address(String[] address){
            addressArray = address;
        }

        public String getHouseNumber() {
            return addressArray[0].trim();
        }

        public String getTownship() {
            return !TextUtils.isEmpty(addressArray[1]) ? addressArray[1].substring(7).trim() : "N/A";
        }

        public String getVillage() {
            return !TextUtils.isEmpty(addressArray[2]) ? addressArray[2].substring(8).trim() : "N/A";
        }

        public String getAlley() {
            return !TextUtils.isEmpty(addressArray[3]) ? addressArray[3].substring(3).trim() : "N/A";
        }

        public String getRoad() {
            return !TextUtils.isEmpty(addressArray[4]) ? addressArray[4].substring(3).trim() : "N/A";
        }

        public String getSubdistrict() {
            return !TextUtils.isEmpty(addressArray[5]) ? addressArray[5].substring(4).trim() : "N/A";
        }

        public String getDistrict() {
            String district = "N/A";
            if(!TextUtils.isEmpty(addressArray[6])){
                if(addressArray[6].contains("อำเภอ"))
                    district = addressArray[6].substring(5).trim();
                else
                    district = addressArray[6].substring(3).trim();
            }
            return district;
        }

        public String getProvince() {
            String province = "N/A";
            if(!TextUtils.isEmpty(addressArray[7])){
                if(addressArray[7].contains("จังหวัด"))
                    province = addressArray[7].substring(7).trim();
                else
                    province = addressArray[7].trim();
            }
            return province;
        }

//			public String getPostcode() {
//				return addressArray[5];
//			}

        @Override
        public String toString() {
            String a = "";
            for (String s : addressArray) {
                a += s+":";
            }
            return  a;
        }

    }

    public class Name {

        private String[] nameArray;

        public Name(String[] name){
            this.nameArray = name;
        }

        public String getPrename() {
            return nameArray[0];
        }

        public String getFirstname() {
            return nameArray[1];
        }

        public String getMidname() {
            return nameArray[2];
        }

        public String getLastname() {
            return nameArray[3];
        }
    }

}
