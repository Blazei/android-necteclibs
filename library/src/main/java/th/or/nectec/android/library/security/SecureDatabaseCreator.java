/** ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */
package th.or.nectec.android.library.security;

import java.io.File;
import java.io.IOException;

import android.content.Context;


/**
 * 
 * Factory class for help create empty database in private mode because
 * context.createoropendatabse() create database file with some meta-data which
 * may not need by some application
 * 
 * @version 1.0
 * @author Piruin Panichphol
 * 
 * @since Family Folder Collector 2.0
 * 
 */
public class SecureDatabaseCreator {
	static boolean writable = true;
	static boolean readable = true;
	static boolean ownerOnly = true;

	/**
	 * create Database file with permission 6-- (read/writable only by owner)
	 * 
	 * @since 1.0

	 * @param context
	 *            context base for create database
	 * @param name
	 *            name of database file output
	 * @return File object for database null if IOException
	 */
	public static File CreateSecureDatabase(Context context, String name) {
		File db = new File(makeDatabseDir(context).getAbsolutePath(), name);
		

		try {
			if (!db.exists())
				db.createNewFile();
			db.setWritable(SecureDatabaseCreator.writable, SecureDatabaseCreator.ownerOnly);
			db.setReadable(SecureDatabaseCreator.readable, SecureDatabaseCreator.ownerOnly);
			return db;
		} catch (IOException io) {

			io.printStackTrace();
			return null;
		}

	}

	/**
	 * create Database file with permission 66- (owner and owner group can read/write)
	 * Note this slower than create for only owner
	 * 
	 * @since 1.0
	 * @param context context base for create database
	 * @param name name of database file output
	 * @return File object for database null if IOException
	 */
	public static File CreateSecureDatabasGroup(Context context, String name) {
		try {

			
			context.openFileOutput(name, Context.MODE_PRIVATE).close();
			File db = new File( context.getFilesDir().getPath()
					+ "/files/", name);
			
			File ndb = new File(makeDatabseDir(context).getAbsolutePath(), name);
			if(ndb.exists())
				ndb.delete();
			
			db.renameTo(ndb);
			
			return ndb;
		} catch (IOException io) {
			io.printStackTrace();

			return null;
		}

	}

	
	private static File makeDatabseDir(Context context){
		
		File dir = new File(context.getFilesDir().getPath()
				+ "/databases/");
		if(!dir.exists()){
			context.openOrCreateDatabase("temp.db", Context.MODE_PRIVATE, null);
		}
		return dir;
	}
}
