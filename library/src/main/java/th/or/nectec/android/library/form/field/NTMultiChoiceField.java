/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import java.util.ArrayList;

import th.or.nectec.android.library.form.IMultiColumnField;
import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.CheckBoxesGroup;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.View;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTMultiChoiceField extends NTField implements IMultiColumnField {

	/**
	 * Column to store value in database.
	 */
	String[] mColumns;

	/**
	 * String format array for set to be Choice
	 */
	String[] mChoice;



	public NTMultiChoiceField(CheckBoxesGroup view, String[] column) {
		super(view, column[0]);
		mColumns = column;

	}

	public NTMultiChoiceField(String[] column, String lable, String[] choice) {
		super(column[0], lable);
		mColumns = column;
		mChoice = choice;

	}

	@Override
	public String[] getColumns() {
		return mColumns;
	}

	@Override
	public void setValueByCursor(Cursor c) {
		CheckBoxesGroup cbg = ((CheckBoxesGroup) mView);
		cbg.setChecked(false);

		for (String col : mColumns) {
			String value = c.getString(c.getColumnIndex(col));
			cbg.setChecked(true, value);
		}
	}

	@Override
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();

		ArrayList<String> chkList = ((CheckBoxesGroup) mView).getCheckedItem();
		int chkListSize = chkList.size();
		int colLength = mColumns.length;

		for (int i = 0; i < colLength && i < chkListSize; i++) {
			cv.put(mColumns[i], chkList.get(i));
		}
		return cv;
	}

	@Deprecated
	@Override
	public String getValue() {
		return null;
	}

	@Deprecated
	@Override
	public void setValue(String value) {
	}

	@Override
	public View onCreateView(Context context) {
		CheckBoxesGroup cbg = new CheckBoxesGroup(context, null);
		cbg.setMode(CheckBoxesGroup.MULTI_CHOICE);
		cbg.setArray(mChoice);

		return cbg;
	}

}
