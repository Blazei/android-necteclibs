/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import th.or.nectec.android.library.form.validator.AbstractValidator;
import th.or.nectec.android.library.form.validator.ValidatorException;
import th.or.nectec.android.library.form.validator.NotEmptyValidator;

/**
 * 
 * abstract Field class to use with NTForm.
 * If want advance option please extends this Class and override follow method
 * <pre>
 * 		constructer,
 * 		getValue(),
 * 		setValue(),
 * </pre>
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public abstract class NTField  {

	protected NTForm mForm;
	protected String mLabel;
	protected View mView;
	protected String mColumn;

	protected String mInvalidMessage;
	protected HashMap<String, String> mFieldOption;

	protected ArrayList<AbstractValidator> mValidators = new ArrayList<AbstractValidator>();

	/**
	 * Constructor of NTField if already have view of field in layout
	 * 
	 * @param view  view to bind with field
	 * @param Column  name of field column
	 */
	public NTField(View view, String Column) {
		mView = view;
		view.setTag("field:"+Column);
		mColumn = Column;
	}
	
	/**
	 * Constructor of NTField for auto-generate view by NTForm.buildForm()
	 * 
	 * @param Column
	 * @param label
	 */
	public NTField(String Column, String label){
		mColumn = Column;
		mLabel = label;
	}
	
	
	/**
	 * Was called upon field add to NTForm
	 * 
	 * @param form instance of NTForm that was added current field
	 *
	 * @since 1.0
	 */
	protected void onAddToForm(NTForm form){
		mForm = form;
	}

	public String getColumn() {
		return mColumn;
	}
	
	public String getLabel(){
		return mLabel;
	}
	
	public NTField setLabel(String label){
		mLabel = label;
		return this;
	}
	
	public boolean isMultiColumn(){
		return (this instanceof IMultiColumnField);
	}

	public View getView() {
		return mView;
	}

	public NTField addValidator(AbstractValidator validator) {
		this.mValidators.add(validator);
		return this;
	}

	public void addValidator(AbstractValidator... validators) {
		int count = validators.length;
		for (int i = 0; i < count; i++) {
			this.mValidators.add(validators[i]);
		}
	}
	
	public NTField refreshValidator(AbstractValidator validator) {
		removeValidator();
		addValidator(validator);
		return this;
	}

	public void refreshValidator(AbstractValidator... validators) {
		removeValidator();
		addValidator(validators);
	}
	
	public NTField removeValidator() {
		this.mValidators.clear();
		return this;
	}

	public NTField setMandatory(Context context){
		this.addValidator(new NotEmptyValidator(context));
		return this;
	}

    boolean skipValidateOnEmpty = false;

    public boolean isSkipValidateOnEmpty() {
        return skipValidateOnEmpty;
    }

    public NTField setSkipValidateOnEmpty(boolean skipValidateOnEmpty) {
        this.skipValidateOnEmpty = skipValidateOnEmpty;
        return this;
    }

	
	public boolean isValid() {
	
		String value = getValue();

        if(skipValidateOnEmpty && TextUtils.isEmpty(value))
            return true;

		Iterator<AbstractValidator> it = this.mValidators.iterator();
		while (it.hasNext()) {
			AbstractValidator validator = it.next();
			try {
				if (!validator.isValid(value)) {
					
					mInvalidMessage = validator.getMessage();
					onInvalid(validator);
					return false;
				}
			} catch (ValidatorException e) {
				System.err.println(e.getMessage());
				System.err.println(e.getStackTrace());
				mInvalidMessage = e.getMessage();
				
				return false;
			}
		}
		onValid();
		return true;
	}
	
	protected void onValid(){
		Log.d("NTField", mColumn +" is valid");
	}
	
	protected void onInvalid(AbstractValidator validator){
		Log.d("NTField", mColumn +" Invalid msg="+ validator.getMessage());
	}

	/**
	 * 
	 * This method was not call by NTForm.buildContentValue if isMultiColumns is true
	 * 
	 * @return current value of View of Field
	 *
	 * @since 1.0
	 */
	public abstract String getValue();
	

	
	/**
	 * set Value for View of field
	 * This method was not call by NTForm.restoreData() if isMultiColumns is true
	 * 
	 * @param value
	 *
	 * @since 1.0
	 */
	public abstract void setValue(String value);
	

	public HashMap<String, String> getFieldOption(){
		return mFieldOption;
	}
	
	public void setFieldOption(HashMap<String, String> option){
		mFieldOption = option; 
	}
	
	public String getMessages() {
		return mInvalidMessage;
	}
	
	@Override
	public String toString() {
		return "NTField [Class="+ getClass() +", Column=" + mColumn + ", getValue()=" + getValue()
				+ ", getMessages()=" + getMessages() + "]";
	}
	
	public void setEnabled(boolean enabled){
		if(mView != null)
			mView.setEnabled(enabled);
	}
	
	
	protected void buildForm(Context context, ViewGroup parent, boolean isLast, boolean isNextQuestionNoTitle){
		if(parent.findViewWithTag("field:"+mColumn) != null)
			return;
		
		if(!TextUtils.isEmpty(mLabel)){
			TextView label = new TextView(context);
			label.setText(mLabel);
			label.setTag("field-label:"+mColumn);
			parent.addView(label);
		}
		
		if(mView == null)
			mView = onCreateView(context);
		
		if(mView == null){
			throw new IllegalArgumentException("Invalid onBuildForm() return value or not yet implemented "+ this.toString());
		}
		mView.setTag("field:"+mColumn);
		parent.addView(mView);
		
		if(!isLast && !isNextQuestionNoTitle){
			//separater between questionaire
			int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, context.getResources().getDisplayMetrics());
			View spacing = new View(context);
			LinearLayout.LayoutParams LineParams = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.MATCH_PARENT, padding);
			parent.addView(spacing, LineParams);
		}
	}
	
	/**
	 * This was called after added Label's View on parent-view
	 * NOTE must return view that was added to parent-view
	 * 
	 * @param context
	 * @param parent
	 * @return view of field to added on parent
	 *
	 * @since 1.0
	 */
	public abstract View onCreateView(Context context);

}
