/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * GAP-Thailand Project
 *
 * Copyright (C) 2013 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */
package th.or.nectec.android.library.network;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import th.or.nectec.android.library.network.HttpPostTask.Response;
import th.or.nectec.android.library.util.JSONInsertOrUpdateTask;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ProgressBar;

/**
 * @author blaze
 *
 */
public abstract class DataFetcher {
	
	public static final int FETCH_NOTHING = -1;
	Context mContext;
	ProgressBar mProgressBar;
	SQLiteOpenHelper mOpenHelper;
	OnFetchFinishListener mFinishListener;
	
	
	public DataFetcher(Context context){
		mContext = context;
	}
	
	public Context getContext(){
		return mContext;
	}
	
	
	public void setOnFetchFinishListener(OnFetchFinishListener mFinishListener) {
		this.mFinishListener = mFinishListener;
	}


	public void setmProgressBar(ProgressBar mProgressBar) {
		this.mProgressBar = mProgressBar;
	}



	public void setmOpenHelper(SQLiteOpenHelper mOpenHelper) {
		this.mOpenHelper = mOpenHelper;
	}

	private int mCallingId = FETCH_NOTHING;
	
	public int getLastFetchId(){
		return mCallingId;
	}

	/**
	 * call this method for start fecth operation and update database
	 * 
	 * @param id Fecth's type id this param should define as public static int FETCH_ on each class
	 * @param params httpPostTask param with all require parameter such as url postmap 
	 */
	protected void fetch(int id, HttpPostTask.Parameter... params){
		mParams = params;
		mPostTask = new HttpPostTask(mPostCallback);
		mPostTask.execute(params);
		mCallingId = id;
	}
	
	HttpPostTask.Parameter[] mParams;
	HttpPostTask mPostTask;

	private HttpPostTask.TaskCallBack mPostCallback = new HttpPostTask.TaskCallBack() {

		@Override
		public void onProgressUpdate(Response... values) {
			Response res = values[0];
			if (res.status != 200) {
				return;
			}
			try {
				JSONObject response = new JSONObject(res.data);
				if (!response.getString("status").equals("success")) {
					onStatusFail(mCallingId, response.getString("message"));
					return;
				}

				final JSONObject returnData = response.optJSONObject("result");
				if (returnData != null) {
					JSONInsertOrUpdateTask updateTask = new JSONInsertOrUpdateTask(mContext, mOpenHelper, returnData, mProgressBar, new JSONInsertOrUpdateTask.OnFinishListener() {
						
						@Override
						public void onTaskFinish(String result) {
							
							boolean conti = returnData.optBoolean("continue");
							int page = returnData.optInt("page");
							if(conti){
								onFectchNextPage(mCallingId, returnData, page, page+1);
								
								if(mFinishListener != null){
									mFinishListener.onFetchNextPage(mCallingId, returnData, page, page + 1);
								}
							}else{
								//call internal on finish method
								onUpdateFinish(mCallingId, result);
								
								//call external listener on fetch finish event
								if(mFinishListener != null)
									mFinishListener.onFetchFinish(mCallingId, returnData ,result);
								}
						}
					});
					
					updateTask.execute(getJsonParam(mCallingId));
				}

			} catch (JSONException e) {
				onInvalidJsonFormat(mCallingId, res.data);
				e.printStackTrace();
			}
		}

		@Override
		public void onPostExecute(ArrayList<Response> result) {
		}

		@Override
		public void onIOException() {
			onFetchIOException(mCallingId, mPostTask, mParams);
		}
	};
	
	protected abstract JSONInsertOrUpdateTask.Params getJsonParam(int id);
	
	
	protected void onStatusFail(int id, String message){
		
	}
	
	protected void onInvalidJsonFormat(int id,String json){
		
	}
	
	protected void onUpdateFinish(int id, String result){
		
	}
	
	protected void onFectchNextPage(int id, JSONObject returnData, int currentPage, int nextPage ){
		
	}
	
	abstract protected void onFetchIOException(int id, HttpPostTask postTask, HttpPostTask.Parameter... param);
	
	public interface OnFetchFinishListener{
		public void onFetchFinish(int id, JSONObject returnData, String result);
		public void onFetchNextPage(int id, JSONObject returnData, int currentPage, int nextPage);
	}
	
}
