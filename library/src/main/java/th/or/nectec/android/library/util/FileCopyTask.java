package th.or.nectec.android.library.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.os.AsyncTask;

public class FileCopyTask extends AsyncTask<FileCopyTask.FileCopyList, String, Integer> {
	
	public static class FileCopyList{
		ArrayList<File> source;
		ArrayList<File> dest;
		
		public FileCopyList(){
			source = new ArrayList<File>();
			dest = new ArrayList<File>();
		}
		
		public void addList(File source, File dest){
			if(source == null || dest == null)
				return;
			
			if(source.exists()){
				this.source.add(source);
				this.dest.add(dest);
			}else{
				throw new IllegalStateException("Source file to copy must be exist");
			}
		}
	}

	@Override
	protected Integer doInBackground(FileCopyList... params) {
		for(FileCopyList list : params){
			int count = list.source.size();
			for(int i = 0; i < count; i++){
				try {
					Log.d("CopyTask", "start copy file");
					FileUtils.copyFile(list.source.get(i), list.dest.get(i));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return 1;
	}
	
	



}
