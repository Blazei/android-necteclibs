/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import th.or.nectec.android.library.form.IMultiColumnField;
import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.util.StringUtils;
import th.or.nectec.android.library.widget.CheckBoxesGroup;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTCheckListField extends NTField implements IMultiColumnField {

	/**
	 * Column to store value in database Columns Length must equal to choice
	 */
	String[] mColumns;

	/**
	 * String format array for set to be Choice
	 */
	String[] mChoice;

	/**
	 * ONly use in FLAT-MODE
	 */
	String mCheckedValue = "1";

	/**
	 * ONLY use in FLAT-MODE
	 */
	String mNotCheckedValue = "0";

	public static final String[] CheckedValue = new String[] { "yes", "true",
			"1", };

	public static final String[] UncheckedValue = new String[] { "no", "false",
			"0", };

	/**
	 * @param view
	 * @param Column
	 */
	public NTCheckListField(CheckBoxesGroup view, String[] Column) {
		super(view, Column[0]);
		mColumns = Column;

	}

	public NTCheckListField(CheckBoxesGroup view, String checkedValue,
			String notCheckValue, String[] column) {
		super(view, column[0]);
		mColumns = column;
		mCheckedValue = checkedValue;
		mNotCheckedValue = notCheckValue;

	}
	
	public NTCheckListField(String[] column, String label, String[] choice){
		super(column[0], label);
		mColumns = column;
		mChoice = choice;
	}

	public NTCheckListField(String[] column, String label, String[] choice,
			String checkedValue, String notCheckValue) {
		super(column[0], label);
		mColumns = column;
		mChoice = choice;
		mCheckedValue = checkedValue;
		mNotCheckedValue = notCheckValue;

	}

	@Override
	public String[] getColumns() {
		return mColumns;
	}

	@Override
	public void setValueByCursor(Cursor c) {
		CheckBoxesGroup cbg = ((CheckBoxesGroup) mView);
		cbg.setChecked(false);

		for (String col : mColumns) {
			String value = c.getString(c.getColumnIndex(col));
			if (!TextUtils.isEmpty(value)
					&& (StringUtils.isEqualIgnoreCase(value, mCheckedValue)))
				cbg.setChecked(true, value);
			else
				cbg.setChecked(false, value);
		}
	}

	@Override
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();

		ArrayList<String> chkList = ((CheckBoxesGroup) mView).getCheckedItem();
		int chkListSize = chkList.size();
		int colLength = mColumns.length;

		String[] chkListArray = chkList.toArray(new String[chkListSize]);
		for (int i = 0; i < colLength; i++) {
			String col = mColumns[i];
			if (StringUtils.isEqualIgnoreCase(col, chkListArray))
				cv.put(col, mCheckedValue);
			else
				cv.put(col, mNotCheckedValue);
		}
		return cv;
	}

	@Deprecated
	@Override
	public String getValue() {
		return null;
	}

	@Deprecated
	@Override
	public void setValue(String value) {
	}

	@Override
	public View onCreateView(Context context) {
		CheckBoxesGroup cbg = new CheckBoxesGroup(context, null);
		cbg.setMode(CheckBoxesGroup.MULTI_CHOICE);
		cbg.setArray(mChoice);
	
		return cbg;
	}

}
