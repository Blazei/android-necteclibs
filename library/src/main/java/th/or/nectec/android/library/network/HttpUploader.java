/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

/**
 * 
 * HTTP-Uploader Utilities that implemented AsyncTask
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * @since Family Folder Collector Plus
 * 
 */
public class HttpUploader extends
		AsyncTask<HttpUploader.UploadParams, String, Integer> {

	public static final String TAG = "uploader";

	public static class UploadParams {
		// protected ArrayList<String> url = new ArrayList<String>();
		// protected ArrayList<String> filePath = new ArrayList<String>();
		protected String url = null;
		protected String filePath = null;
		public Map<String, String> params;

		/**
		 * @param url of request to upload file
		 * @param path absolute path of file to upload
		 */
		public UploadParams(String url, String path) {
			this.url = url;
			this.filePath = path;
			this.params = new HashMap<String, String>();
		}

		public void putParam(String key, String value) {
			if (params == null)
				params = new HashMap<String, String>();
			params.put(key, value);
		}
	}

	String uploadedUrl;
	String uploadedPath;

	@Override
	protected Integer doInBackground(HttpUploader.UploadParams... params) {
		int successCount = 0;
		for (UploadParams p : params) {
			
			String statusString = "fail";
			String response = null;
			ServerResponse res = HttpUploader.upload(p.url, p.filePath, p.params);
			
			
			if (res.code == 200) {
				statusString = "success";
				successCount = successCount + 1;
			}
			publishProgress(statusString, p.url, p.filePath, res.message);
			
			if (isCancelled())
				break;

		}
		return successCount;
	}

	/**
	 * 
	 * @param dest
	 *            destination url
	 * @param path
	 *            absolute path of file to upload
	 * @return server response message
	 */
	public static ServerResponse  upload(String dest, String path,
			Map<String, String> reqParams) {
		ServerResponse res = new ServerResponse();
		
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		// DataInputStream inputStream = null;
		int serverResponseCode = 0;

		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = reqParams.entrySet()
				.iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> params = iterator.next();
			bodyBuilder.append(params.getKey()).append('=')
					.append(params.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}

		String body = bodyBuilder.toString();

		String pathToOurFile = path;
		String urlServer = dest;
		if (body.length() > 0) {
			urlServer = dest + "?" + body;
		}

		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		File sourceFile = new File(pathToOurFile);
		if (!sourceFile.isFile()) {
			Log.e(TAG, "Source File Does not exist");
			res.code = 404;
			res.message = "Source File Does not exist";
			return res;
		}

		String message = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(
					pathToOurFile));

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setChunkedStreamingMode(0);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream
					.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
							+ pathToOurFile + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file to buffer
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				// Write file from buffer
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			// Write end of parameter
			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens
					+ lineEnd);
			// Completed file push to server

			// Get Responses from the server (code and message)
			serverResponseCode = connection.getResponseCode();
			String serverResponseMessage = connection.getResponseMessage();
			
			res.code = serverResponseCode;
			
			Log.d(TAG, "ServerResponse code=" + serverResponseCode + " msg="
					+ serverResponseMessage);

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			Log.e(TAG, ex.toString());
		}

		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				line = line.trim();

				if (!TextUtils.isEmpty(line)) {
					res.message = (!TextUtils.isEmpty(res.message)) ? res.message + line
							: line;
					Log.i(TAG, "RES Message: " + line);
				}
			}
			rd.close();
			
		} catch (IOException ioex) {
			Log.e(TAG, "error: " + ioex.getMessage(), ioex);
		}
		//Log.d(TAG, "upload response :"+ res.message);
		return res; // like 200 (Ok)
	}
	
	static class ServerResponse{
		int code;
		String message;
		
		public ServerResponse(){
			code = 0;
			message = null;
		}
	}

	UploadProcessCallback callback;

	@Override
	protected void onProgressUpdate(String... values) {
		super.onProgressUpdate(values);
		if (callback != null) {
			boolean status = values[0].equals("success") ? true : false;
			
			callback.onProgressUpdate(status, values[1], values[2], values[3]);
		}
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if (callback != null) {
			callback.onProcessFinish(result);
		}
	}

	public void setProcessCallback(UploadProcessCallback callback) {
		this.callback = callback;
	}

	public static interface UploadProcessCallback {
		/**
		 * callback method after each one request has finished
		 * 
		 * @param status true if connection successful, false otherwise NOTE should checksum of file and response it back to device
		 * @param url of request connection that just finish
		 * @param path of uploaded file 
		 * @param response message for server
		 *
		 * @since 1.0
		 */
		public void onProgressUpdate(boolean status, String url, String path, String response);

		public void onProcessFinish(int uploaded);
	}

}
