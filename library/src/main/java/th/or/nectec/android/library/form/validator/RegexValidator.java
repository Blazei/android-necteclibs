package th.or.nectec.android.library.form.validator;

import android.content.Context;
import android.text.TextUtils;

import java.util.regex.Pattern;

import th.or.nectec.android.library.R;

/**
 * Created by Blaze on 12/24/14 AD.
 */
public class RegexValidator extends AbstractValidator {
    private Pattern mPattern;

    public static final String THAI_ALPHABET = "^[ก-๙ -]{1,} *[ก-๙ -]";
    public static final String THAI_TELEPHONE_NO = "0\\d{8}";
    public static final String THAI_CELL_NO = "0(((6|8|9)\\d{8}))";
    public static final String THAI_POSTCODE = "\\d{5}";

    private int mErrorMessage = R.string.validator_regexp;

    public RegexValidator(Context c) {
        super(c);
    }

    public RegexValidator(Context c, int errorMessage) {
        super(c);
        mErrorMessage = errorMessage;
    }

    public RegexValidator(Context c, int errorMessage, String pattern) {
        super(c);
        mErrorMessage = errorMessage;
        mPattern = Pattern.compile(pattern);
    }

    public void setPattern(String pattern){
        mPattern = Pattern.compile(pattern);
    }

    public void setPattern(Pattern pattern) {
        mPattern = pattern;
    }

    @Override
    public boolean isValid(String value) throws ValidatorException {
        if(TextUtils.isEmpty(value)){
            return true;
        }
        if(mPattern != null){
            return mPattern.matcher(value).matches();
        }else{
            throw new ValidatorException("You can set Regexp Pattern first");
        }
    }

    @Override
    public String getMessage() {
        return mContext.getString(mErrorMessage);
    }


}
