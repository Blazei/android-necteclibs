/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.ThaiCitizenIdEditText;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class NTCitizenIDField extends NTField {

	public NTCitizenIDField(ThaiCitizenIdEditText view, String column){
		super(view, column);
	}
	
	/**
	 * @param Column
	 * @param lable
	 */
	public NTCitizenIDField(String Column, String lable) {
		super(Column, lable);
		
	}

	@Override
	public String getValue() {
		return ((ThaiCitizenIdEditText)mView).getCitizenId();
	}

	@Override
	public void setValue(String value) {
		if(!TextUtils.isEmpty(value) && TextUtils.isDigitsOnly(value))
			((ThaiCitizenIdEditText)mView).setText(value);
	}
	
	@Override
	public boolean isValid() {
		if(!((ThaiCitizenIdEditText)mView).isValidID()){
			return false;
		}
		return super.isValid();
	}

	@Override
	public View onCreateView(Context context) {
		ThaiCitizenIdEditText txt = new ThaiCitizenIdEditText(context, null);
		return txt;
	}

}
