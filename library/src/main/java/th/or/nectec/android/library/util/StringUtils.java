/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.util;

import android.text.TextUtils;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class StringUtils {

	public static boolean isEmpty(String... str) {
		int len = str.length;
		for (int i = 0; i < len; i++) {
			if (TextUtils.isEmpty(str[i])) {
				return true;
			}
		}
		return false;
	}
	
	
	public static boolean isEqualIgnoreCase(String base, String... str ){
		int len = str.length;
		for (int i = 0; i < len; i++) {
			if(base.equalsIgnoreCase(str[i])){
				return true;
			}	
		}
		return true;
	}
}
