/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.validator;

import th.or.nectec.android.library.util.ThaiCitizenID;
import android.content.Context;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class ThaiCitizenIDValidator extends AbstractValidator {

	
	/**
	 * @param c
	 */
	public ThaiCitizenIDValidator(Context c) {
		super(c);
		
	}

	@Override
	public boolean isValid(String value) throws ValidatorException {
		
		return ThaiCitizenID.Validate(value.replaceAll("-", ""));
	}

	@Override
	public String getMessage() {
		return "รหัสประชาชนไม่ถูกต้อง";
	}

}
