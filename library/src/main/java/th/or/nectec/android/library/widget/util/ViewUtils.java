/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class ViewUtils {

	public static  boolean isSetBackground(Context context, AttributeSet attrs, int defStyle){
		TypedArray a = context.obtainStyledAttributes(attrs, new int[] { android.R.attr.background},
                defStyle, 0);
		Drawable bgDrawable = a.getDrawable(0);
		//String bg = getExplicitValue(attrs, "background");
		
		//Log.d("ViewUtils", "drawable="+bgDrawable+" & bg="+bg);
		
		return (bgDrawable != null) ;
		
	}
	
	public static String getExplicitValue(AttributeSet attrs, String name){
		return attrs.getAttributeValue("http://schemas.android.com/apk/res/android", name);
	}
}
