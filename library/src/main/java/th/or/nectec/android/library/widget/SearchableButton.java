/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Button;

import th.or.nectec.android.library.provider.columns.NameColumn;
import th.or.nectec.android.library.widget.SearchListDialog.ItemClickListener;
import th.or.nectec.android.library.R;

/**
 * 
 * Custom view to replace Searchable Spinner for use with non-long id data
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * @since 1.0
 * 
 */
public class SearchableButton extends Button implements ItemClickListener {

	public SearchableButton(Context context, AttributeSet attrs) {
		super(context, attrs, android.R.attr.editTextStyle);

		if (!isInEditMode()) {
			init();
		}
		//
		//		if (!ViewUtils.isSetBackground(context, attrs, 0)) {
		//
		//			TypedArray att = context.getTheme().obtainStyledAttributes(
		//					new int[] { android.R.attr.editTextBackground, });
		//			try {
		////				this.setBackgroundDrawable(att.getDrawable(0));
		////				this.setFocusable(true);
		//
		//			} finally {
		//				att.recycle();
		//			}
		//		}
	}

	private void init() {

		this.setGravity(Gravity.LEFT);
		this.setMaxLines(2);

		this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		// this.setTextColor(getResources().getColor(
		// android.R.color.primary_text_light));
		float density = getResources().getDisplayMetrics().density;
		int paddingLR = (int) (getResources()
				.getDimension(R.dimen.text_padding) * density);
		int paddingT = (int) (getResources().getDimensionPixelSize(
				R.dimen.edittext_padding_top) * density);
		int paddingB = (int) (getResources().getDimensionPixelSize(
				R.dimen.edittext_padding_buttom) * density);
		this.setPadding(paddingLR, paddingT, paddingLR, paddingB);
		this.setGravity(Gravity.CENTER_VERTICAL);
		this.setFocusable(false);
	}

	private String selectId;

	private String tag;
	private FragmentManager fm;
	private SearchListDialog f;

	/**
	 * 
	 * @param fm
	 *            support Fragment manager for Activity or Fragment
	 * @param cls
	 *            class of Search List Dialog to show when button was click
	 * @param args
	 *            arguments for Search List Dialog @see FFCSearchListDialog
	 * @param tag
	 *            String tag to use with Fragment manager for handle fragment
	 *            transaction
	 * @return this object
	 * 
	 * @since 1.0
	 */
	public SearchableButton setDialog(FragmentManager fm,
			Class<? extends SearchListDialog.BaseAdapter> cls, Bundle args,
			String tag) {

		this.tag = tag;

		this.fm = fm;
		Fragment prev = fm.findFragmentByTag(tag);
		if (prev != null)
			f = (SearchListDialog) prev;
		else
			f = (SearchListDialog) Fragment.instantiate(getContext(),
					cls.getName(), args);

		f.setItemClickListener(this);

		return this;
	}

	public SearchableButton setDialog(FragmentManager fm,
			Class<? extends SearchListDialog.BaseAdapter> cls, String tag) {
		return this.setDialog(fm, cls, null, tag);
	}

	@Override
	public boolean performClick() {
		boolean handle = false;
		if (!handle && fm != null) {
			FragmentTransaction ft = fm.beginTransaction();
			Fragment prev = fm.findFragmentByTag(tag);
			if (prev != null)
				ft.remove(prev);
			ft.addToBackStack(null);

			if (f != null) {
				f.setItemClickListener(this);
				f.show(ft, tag);
				handle = true;
			}
		}
		return handle;
	}

	public String getSelectId() {
		return this.selectId;
	}

	/**
	 * use String ID for set selection ID and show content of that ID
	 * 
	 * @param id
	 *            to set as select id
	 * 
	 * @since 1.0
	 */
	public void setSelectionById(String id) {
		if (!TextUtils.isEmpty(id)) {

			Uri uri = Uri.withAppendedPath(f.getContentUri(), id);
			Cursor c = getContext().getContentResolver().query(uri,
					new String[] { NameColumn._NAME }, null, null, null);

			if (c.moveToFirst()) {
				this.selectId = id;
				this.setText(c.getString(0));
				Log.d("SB", c.getString(0));
			} else {
				Log.d("SB", "not found");
			}
		} else {
			Log.d("SB", "Null id");
		}
	}

	/**
	 * for set selected item
	 * 
	 * @author jey Atiwat
	 * @param id
	 *            to set as select id
	 * @param partialSelection
	 *            addition instance where to query data. NOTE must have place
	 *            for id parameter. For instance "id=? AND id2='5456'"
	 */
	public void setSelectionById(String id, String partialSelection) {
		Uri uri = f.getContentUri();
		Cursor c = getContext().getContentResolver().query(uri,
				new String[] { NameColumn._NAME }, partialSelection,
				new String[] { id }, null);
		if (c.moveToFirst()) {
			this.selectId = id;
			this.setText(c.getString(0));
			Log.d("SB", c.getString(0));
		} else {
			Log.d("SB", "Null id");
		}
	}

	/**
	 * set selection by value and custom selection
	 * 
	 * @param value
	 *            selection value to find id
	 * @param selection
	 *            sql's where string to use with 'value', suck as "name=?"
	 */
	public void setSelectionByValue(String value, String selection) {
		Uri uri = f.getContentUri();
		Cursor c = getContext().getContentResolver().query(uri,
				new String[] { BaseColumns._ID }, selection,
				new String[] { value }, null);
		if (c.moveToFirst()) {
			this.selectId = c.getString(0);
			this.setText(value);
		} else {
			// this.setText("");
		}
	}

	/**
	 * use String ID for set selection ID and show content of that ID
	 * 
	 * @param id
	 *            to set as select id
	 * 
	 * @since 1.0
	 */
	public void clearSelection() {
		this.selectId = null;
		this.setText("");
	}

	@Override
	public void onItemClick(HighLightCursorAdapter adapter, long id,
			int position) {
		throw new IllegalArgumentException(
				"SearchableButton not support for HighLightCursorAdapter");
	}

	@Override
	public void onItemClick(CursorStringIdAdapter adapter, String id,
			String text) {
		f.dismiss();
		this.selectId = id;
		this.setText(text);
		this.getSelectId();

		if (mListener != null) {
			mListener.onItemSelect(id);
		}
	}

	private OnItemSelectListener mListener;

	public void setOnItemSelectListener(OnItemSelectListener listener) {
		this.mListener = listener;
	}

	public static interface OnItemSelectListener {
		public void onItemSelect(String id);
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		SavedState ss = new SavedState(superState);
		ss.id = this.selectId;
		ss.text = this.getText().toString();

		return ss;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		// end

		this.selectId = ss.id;
		this.setText(ss.text);
	}

	static class SavedState extends BaseSavedState {
		String id;
		String text;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			this.id = in.readString();
			this.text = in.readString();
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeString(this.id);
			out.writeString(this.text);
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};

	}

}
