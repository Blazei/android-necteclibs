/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.validator;

import th.or.nectec.android.library.util.DateTime;
import android.text.TextUtils;
import android.util.Log;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class DateValidator extends AbstractValidator {

	DateTime mOldestDate, mNewestDate;
	String mErrMessage;
	/**
	 * @param c
	 */
	public DateValidator(DateTime oldestDate, DateTime newestDate, String errorMessage) {
		super(null);
		mOldestDate = oldestDate;
		mNewestDate = newestDate;
		mErrMessage = errorMessage;
	}
	
	public DateValidator(String oldestDate, String newestDate, String errorMessage){
		super(null);
		if(!TextUtils.isEmpty(oldestDate)){
			mOldestDate = DateTime.newInstance(oldestDate);
		}
		if(!TextUtils.isEmpty(newestDate)){
			mNewestDate = DateTime.newInstance(newestDate);
		}
		mErrMessage = errorMessage;
	}
	
	

	/**
	 * @return the mOldestDate
	 */
	public DateTime getOldestDate() {
		return mOldestDate;
	}

	/**
	 * @param mOldestDate the mOldestDate to set
	 */
	public void setOldestDate(DateTime mOldestDate) {
		this.mOldestDate = mOldestDate;
	}

	/**
	 * @return the mNewestDate
	 */
	public DateTime getNewestDate() {
		return mNewestDate;
	}

	/**
	 * @param mNewestDate the mNewestDate to set
	 */
	public void setNewestDate(DateTime mNewestDate) {
		this.mNewestDate = mNewestDate;
	}

	@Override
	public boolean isValid(String value) throws ValidatorException {
		DateTime val = DateTime.newInstance(value);
		boolean valid = true;
		if(mNewestDate != null && val.compareTo(mNewestDate) > 0){
			valid = false;
		}
		if(mOldestDate != null && val.compareTo(mOldestDate) < 0){
			valid = false;
		}
		return valid;
	}

	@Override
	public String getMessage() {
		return mErrMessage;
	}
	
	public static final String MESSAGE_PAST_DATE = "date can't be in future";
	public static final String MESSAGE_FUTURE_DATE = "date can't be past";
	
	public static DateValidator getPastDateValidator(){
		return new DateValidator("1875-01-01", DateTime.getCurrentDate(), MESSAGE_PAST_DATE);
	}
	
	/**
	 * 
	 * @param oldestDate  set possible oldest date, may null if not specify
	 * @param msg  message when invalid input
	 * @return Instance of validator
	 *
	 * @since 1.0
	 */
	public static  DateValidator getPastDateValidator(DateTime oldestDate ,String msg){
			Log.d("Oldest Date",oldestDate+"");
			return new DateValidator(oldestDate.toString(), DateTime.getCurrentDate(), msg);
	}
	
	public static  DateValidator getFutureDateValidator(){
		return new DateValidator(DateTime.getCurrentDate(), null, MESSAGE_FUTURE_DATE);
	}
	
	public static  DateValidator getFutureDateValidator(DateTime newestDate, String msg){
		return new DateValidator(DateTime.getCurrentDate(), newestDate.toString() , msg);
	}

}
