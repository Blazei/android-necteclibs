/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.text.TextUtils;

/**
 * 
 * Util class for handle about FFC date & time
 * 
 * @version 1.51
 * 
 * @author Piruin Panichphol
 * @since Family Folder Collector Plus
 * 
 */
public class DateTime implements Comparable<DateTime> {

	private static final int[] mDayOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31,
			30, 31, 30, 31 };
	
	private static final String[] mThaiMonthArray = {"มกราคม", "กุมภาพันธ์",
		"มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
		"กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม",  };

	private static final String[] mEngMonthArray = { "January", "February",
			"March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December", };
	
	public int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;
	
	public DateTime(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	public DateTime(int year, int month, int day, int hour, int minute, int second) {
		this(year, month, day);
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public DateTime() {
		this.year = 0;
		this.month = 0;
		this.day = 0;
	}

	public static String getCurrentDate() {
		try {
			Calendar current = Calendar.getInstance();
			int currentYear = current.get(Calendar.YEAR);
			int currentMonth = current.get(Calendar.MONTH) + 1;
			int currentDay = current.get(Calendar.DAY_OF_MONTH);
			String month = (currentMonth < 10) ? "0" + currentMonth : ""
					+ currentMonth;
			String day = (currentDay < 10) ? "0" + currentDay : "" + currentDay;
			return currentYear + "-" + month + "-" + day;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	public static String getCurrentTime() {
		try {
			Calendar current = Calendar.getInstance();
			int currentHour = current.get(Calendar.HOUR_OF_DAY);
			int currentMinute = current.get(Calendar.MINUTE);
			int currentSecond = current.get(Calendar.SECOND);
			String Hour = (currentHour < 10) ? "0" + currentHour : ""
					+ currentHour;
			String Minute = (currentMinute < 10) ? "0" + currentMinute : ""
					+ currentMinute;
			String Second = (currentSecond < 10) ? "0" + currentSecond : ""
					+ currentSecond;
			return Hour + ":" + Minute + ":" + Second ;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	public static String getCurrentDateTime() {
		String datetime = getCurrentDate() + " " + getCurrentTime();
		return datetime;

	}

	private static String getMonthNameThai(int monthIndex) {
		if (monthIndex < 1 || monthIndex > 12)
			throw new IndexOutOfBoundsException();
		String[] month = mThaiMonthArray;
		return month[monthIndex - 1];
	}
	
	private static String getMonthName(int monthIndex) {
		if (monthIndex < 1 || monthIndex > 12)
			throw new IndexOutOfBoundsException();
		String[] month = mEngMonthArray;
		return month[monthIndex - 1];
	}

	public static String getFullFormatThai(String date) {
		DateTime d = DateTime.newInstance(date);
		return getFullFormatThai( d);
	}

	public static String getFullFormatThai( DateTime d) {
		if (d != null)
			return d.day + " " + getMonthNameThai( d.month) + " "
					+ (d.year + 543);
		return null;
	}

	public static String getFullFormat(DateTime d) {
		if (d != null)
			return d.year + " " + getMonthName(d.month) + " " + d.day;
		return null;
	}
	
	public String getDatetimeAgo(){
		DateTime current = DateTime.newInstance(DateTime.getCurrentDateTime());
		if (this.compareTo(current) != OLDER)
			return null;
		DateTime distance = new AgeCalculator(current, this).calulate();
		int dist = 0;
		dist += (distance.year * 365);
		dist += (distance.month * 30);
		dist += distance.day;

		if(dist == 0){
			long currentUnix = dateTimeToUnix(current.toString());
			long thisUnix = dateTimeToUnix(this.toString());
			
			DateTime timeAgo = secToDateTime(currentUnix - thisUnix);
			
			if(timeAgo.hour > 0){
				return timeAgo.hour +" ชั่วโมง";
			}else if(timeAgo.minute > 0){
				return timeAgo.minute + " นาที";
			}else{
				return timeAgo.second + " วินาที";
			}
		}else if(dist == 1){
			return "เมื่อวาน";
		}else{
			String thFormat = DateTime.getFullFormatThai(this);
			if(this.year == current.year)
				thFormat = thFormat.substring(0, thFormat.length() - 4);
			
			return thFormat;
		}
	}
	
	
	
	private DateTime secToDateTime(long sec){
		DateTime d = new DateTime();
		d.hour = (int) sec / 3600;
	    int remainder = (int) sec - d.hour * 3600;
	    d.minute = remainder / 60;
	    remainder = remainder - d.minute * 60;
	    d.second = remainder;
		return d;		
	}
	
	/**
	 * 
	 * @param String in format [yyyy-mm-dd HH:mm:ss] to get unix time
	 * @return  unixTime in second
	 */
	private static long dateTimeToUnix(String dt){
		
		java.text.DateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss", new Locale("th", "TH"));
		Date date = null;
		try{
			date = format.parse(dt);
			return date.getTime() / 1000L;
		} catch (java.text.ParseException e) {
			e.printStackTrace();
			return 0L;
		}

	}
	

	/**
	 *  format yyyy-mm-dd HH:MM:SS
	 */
	@Override
	public String toString() {
		String month = (this.month < 10) ? "0" + this.month : "" + this.month;
		String day = (this.day < 10) ? "0" + this.day : "" + this.day;
		
	
		
		String hour = (this.hour < 10) ? "0" + this.hour : "" + this.hour;
		String minute = (this.minute < 10) ? "0" + this.minute : "" + this.minute;
		String second = (this.second < 10) ? "0" + this.second : "" + this.second;
		
		String dateString = this.year + "-" + month + "-" + day;
		String timeString = hour + ":" + minute + ":" + second;
		
		String returnString = (!timeString.equals("00:00:00"))
				? dateString + " " + timeString : dateString;
		return returnString;
	}
	
	
	/**
	 * 
	 * @param date
	 *            String of date to create Date Object [yyyy-mm-dd HH:MM:SS]
	 * @return Date object
	 * @throws NumberFormatException if date string format is invalid
	 * @since 1.0
	 */
	public static DateTime newInstance(String date) {
		if (TextUtils.isEmpty(date))
			return null;
		
		
		int day = Integer.parseInt(date.substring(8, 10));
		int month = Integer.parseInt(date.substring(5, 7));
		int year = Integer.parseInt(date.substring(0, 4));
		
		if(date.length() >= 18){
			
			int second = Integer.parseInt(date.substring(17, 19));
			int minute = Integer.parseInt(date.substring(14, 16));
			int hour = Integer.parseInt(date.substring(11, 13));
			return new DateTime(year, month, day, hour, minute, second);
		}else
			return new DateTime(year, month, day);
	}
	
	/**
	 * @return  DateTime.newInstance(DateTime.getCurrentDateTime());
	 */
	public static DateTime newInstance(){
		return DateTime.newInstance(DateTime.getCurrentDateTime());
	}
	
	/**
	 * 
	 * @param day
	 *            number of day that want to increase
	 * 
	 * @throws IllegalArgumentException
	 *             if day < 1
	 * 
	 * @since 1.5
	 */
	public void increaseDay(int day) {
		if (day < 1)
			throw new IllegalArgumentException("increase day more than 0");

		int leftDay = getMaxDayOfMonth() - this.day;
		if (day - leftDay <= 0) {
			this.day += day;
		} else {
			this.day = 0;
			increaseMonth(1);
			day -= leftDay;
			
			while (day - getMaxDayOfMonth() > 0) {
				day -= getMaxDayOfMonth();
				increaseMonth(1);
			}
			this.day += day;
		}
	}

	/**
	 * 
	 * @param month
	 *            number of month that want to increase
	 * 
	 * @throws IllegalArgumentException
	 *             if month < 1
	 * 
	 * @since 1.5
	 */
	public void increaseMonth(int month) {
		if (month < 1)
			throw new IllegalArgumentException(
					"increase month must more than 0");

		int monthIndex = this.month - 1;
		monthIndex = monthIndex + month;
		if (monthIndex > 11) {
			int incYear = monthIndex / 11;
			this.year += incYear;
			monthIndex = (monthIndex - incYear) % 11;
		}
		this.month = monthIndex + 1;
	}

	/**
	 * 
	 * @return get possible day of Date month
	 * 
	 * @since 1.5
	 */
	public int getMaxDayOfMonth() {
		int maxDay = 0;
		if (this.month == 2) {
			maxDay = LeapDay.february(this.year);
		} else {
			maxDay = DateTime.mDayOfMonth[this.month - 1];
		}
		return maxDay;
	}
	

	/**
	 * 
	 * @param another
	 *            Date to compare must newer than this caller date
	 * @return distance of day to another Date
	 * 
	 * @since 1.25
	 */
	public int distanceTo(DateTime another) {
		if (this.compareTo(another) != OLDER)
			return 0;
		DateTime distance = new AgeCalculator(another, this).calulate();
		int dist = 0;
		dist += (distance.year * 365);
		dist += (distance.month * 30);
		dist += distance.day;
		return dist;

	}

	public static final int NEWER = 1;
	public static final int OLDER = -1;
	public static final int EQUAL = 0;

	@Override
	public int compareTo(DateTime another) {
		if (this.year == another.year) {
			
			if (this.month == another.month) {
				
				if (this.day == another.day){
					
					if(this.hour == another.hour){
						
						if(this.minute == another.minute){
							
							if(this.second == another.second){
								return EQUAL;
							}
							else if(this.second > another.second)
								return NEWER;
							else
								return OLDER;
						}
						else if (this.minute > another.minute)
							return NEWER;
						else
							return OLDER;
					}
					else if (this.hour > another.hour)
						return NEWER;
					else
						return OLDER;
				}
				else if (this.day > another.day)
					return NEWER;
				else
					return OLDER;
			} 
			else if (this.month > another.month) {
				return NEWER;
			} else {
				return OLDER;
			}
		} 
		else if (this.year > another.year) {
			return NEWER;
		} else {
			return OLDER;
		}
	}

}
