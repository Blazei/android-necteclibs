/* 
 * MaxProfit Android Project
 *
 * Copyright (C) 2010-2014 Leonidlab
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.database.sqlite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author Piruin Panichphol
 *
 */
public class SQLiteUtils {

	//Insert data from csv file
		public static void writeDataFromCSV(Context context, SQLiteDatabase db, String csvFileName,
				String tableName, String[] columns) {
			try {
				int columnCount = columns.length;
				BufferedReader br = new BufferedReader(new InputStreamReader(
						context.getAssets().open(csvFileName)));
				String readLine = null;

				try {
					db.beginTransaction();
					while ((readLine = br.readLine()) != null) {
						String[] str = readLine.trim().split(",");
						ContentValues values = new ContentValues();
						//Log.d("create", "length="+ str.length+" value0="+str[0]+"value="+ str[1] + " value2="+str[2] +" value3="+ str[3]);
						for (int i = 0; i < columnCount; i++) {
							values.put(columns[i], str[i].trim().replace("\"", ""));
						}
						db.insert(tableName, null, values);
					}
					db.setTransactionSuccessful();
				} catch (IOException e) {
					e.printStackTrace();
				} finally{
					db.endTransaction();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
}
