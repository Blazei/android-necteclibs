/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public interface NTFormGeneratorColumn {

	public static final String ID = "id";
	public static final String ORDER = "order";
	public static final String TABLE = "table";
	public static final String COLUMN = "column";
	public static final String COLUMNS = "columns";
	public static final String LABEL = "label";
	public static final String TYPE = "type";
	public static final String VALUE = "value";
	public static final String MANDATORY = "mandatory";
	public static final String VALIDATOR = "validator";
	public static final String RELATION = "relation";
	public static final String OPTION = "option";
}
