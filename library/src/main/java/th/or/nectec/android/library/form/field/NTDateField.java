/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.util.DateTime;
import th.or.nectec.android.library.widget.ThaiDatePicker;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class NTDateField extends NTField {

	/**
	 * @param view
	 * @param Column
	 */
    boolean needDefineChkBox;

	public NTDateField(ThaiDatePicker view, String Column) {
		super(view, Column);
	}
	
	public NTDateField(String column, String label){
		super(column, label);
	}
	
	@Override
	public void setValue(String value) {
		try{
            if(!TextUtils.isEmpty(value)){
                ((ThaiDatePicker) mView).updateDate(DateTime.newInstance(value));
                ((ThaiDatePicker) mView).setDefineableChecked(true);
            }else{
                ((ThaiDatePicker) mView).setDefineableChecked(false);
            }
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public String getValue() {
        DateTime value = ((ThaiDatePicker) mView).getDate();
		return value!=null ? value.toString() : null;
	}

	@Override
	public View onCreateView(Context context) {
		ThaiDatePicker tdp = new ThaiDatePicker(context);
        tdp.setDefineable(needDefineChkBox);
		tdp.updateDate();
		
		return tdp;
	}
}
