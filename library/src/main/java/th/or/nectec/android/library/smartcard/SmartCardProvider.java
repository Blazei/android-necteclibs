package th.or.nectec.android.library.smartcard;

import java.security.InvalidParameterException;
import java.util.HashMap;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.idvision.androididcardlib.iCardUsbReader;

/**
 * Provider Class for provide Reader interface object, just register Activity
 * then get Reader and done what you want. leave other part suck as initialize
 * reader on DEVICE_ATTACHED, close reader when DEVICE_DETACHED</br> </br> NOTE
 * must add follow part to AndroidManifest.xml under activity's tag that use
 * this class</br>
 * 
 * <pre>
 * <code>
 *  &lt;intent-filter&gt;
 *      &lt;action android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED" /&gt;
 *  &lt;/intent-filter&gt;
 *  
 *  &lt;meta-data android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED"
 *               android:resource="@xml/device_filter" /&gt;
 * </code>
 * </pre>
 * 
 * @author blaze
 * 
 */
public class SmartCardProvider {

	private iCardUsbReader sReader;
	private UsbManager mUsbManager;
	private UsbDevice mDevice;
	private static final String TAG = "SmartCardProvider";
	public static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

	private ReaderStatusListener mStatusListener;

	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					mDevice = (UsbDevice) intent
							.getParcelableExtra(UsbManager.EXTRA_DEVICE);
					if (intent.getBooleanExtra(
							UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						// setDevice(device);
						sReader.init(mUsbManager, mDevice);

						if (mStatusListener != null) {
							mStatusListener.onReaderPermissinoGranted(sReader);
							mStatusListener.onReaderStatusChanged(sReader,
									sReader.getReaderStatus());
						}

					} else {
						Log.d(TAG, "permission denied for device " + mDevice);

						if (mStatusListener != null) {
							mStatusListener.onReaderPermissionDenial(sReader);
							mStatusListener.onReaderStatusChanged(sReader,
									false);
						}
					}
				}
			} else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {

				sReader.init(mUsbManager, mDevice);

				if (!sReader.getReaderStatus()) {
					sReader.close();
					sReader = null;

					sReader = new iCardUsbReader();
					requestPermission();
				}

				if (mStatusListener != null) {
					mStatusListener.onReaderAttached(sReader);
					mStatusListener.onReaderStatusChanged(sReader,
							sReader.getReaderStatus());
				}
			} else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {

				sReader.close();
				if (mStatusListener != null) {
					mStatusListener.onReaderDetached(sReader);
					mStatusListener.onReaderStatusChanged(sReader, false);
				}
			}
		}
	};

	Context mContext;

	/**
	 * register Broadcast Receiver for context to receive status of USB_DEVICE
	 * with 3 state ACTION_USB_PERMISSION ACTION_USB_DEVICE_ATTACHED
	 * ACTION_USB_DEVICE_DETACHED
	 * 
	 * @param context
	 */
	private void registerReaderReceiver(Context context) {

		mUsbManager = (UsbManager) context
				.getSystemService(Context.USB_SERVICE);

		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
		filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);

		context.registerReceiver(mUsbReceiver, filter);

		requestPermission();

		sReader = new iCardUsbReader();

	}

	public void unregisterReaderReceiver() {
		mContext.unregisterReceiver(mUsbReceiver);
	}

	public SmartCardProvider(Context context, ReaderStatusListener listener) {
		mContext = context;

		registerReaderReceiver(mContext);
		setReaderStatusListener(listener);
	}

	public void requestPermission() {
		PendingIntent mPermissionIntent = PendingIntent.getBroadcast(mContext,
				0, new Intent(ACTION_USB_PERMISSION), 0);

		HashMap<String, UsbDevice> map = mUsbManager.getDeviceList();
		for (UsbDevice device : map.values()) {
			if (device.getDeviceClass() == UsbConstants.USB_CLASS_PER_INTERFACE) {
				mDevice = device;
				mUsbManager.requestPermission(device, mPermissionIntent);
			}
		}
	}

	public iCardUsbReader getReader() {
		if (sReader == null) {
			throw new IllegalStateException(
					"must register context before call getReader");
		}
		return sReader;
	}

	public void setReaderStatusListener(ReaderStatusListener statusListener) {
		if (statusListener == null) {
			throw new InvalidParameterException("ReaderStatusListener is NULL");
		}
		this.mStatusListener = statusListener;
		this.mStatusListener.onReaderStatusChanged(sReader,
				sReader.getReaderStatus());

	}

	public interface ReaderStatusListener {
		public void onReaderPermissionDenial(iCardUsbReader reader);

		public void onReaderPermissinoGranted(iCardUsbReader reader);

		public void onReaderAttached(iCardUsbReader reader);

		public void onReaderDetached(iCardUsbReader reader);

		public void onReaderStatusChanged(iCardUsbReader reader, boolean ready);
	}

}
