/* 
 * DengueSurvey Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.database.sqlite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import android.text.TextUtils;

/**
 * 
 * SQLite Create statement helper class for prevent bug from create too long SQL
 * string and reduce redundant work!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class SQLiteCreateBuilder {

	String mTablename;
	ArrayList<String> mColumnList = new ArrayList<String>();
	StringBuilder mColumnsBuilder;
	StringBuilder mTableConstraintBuilder;

	public static final String CONSTRAINT_PRIMARY_KEY = "PRIMARY KEY";
	public static final String CONSTRAINT_UNIQUE = "UNIQUE";
	public static final String CONSTRAINT_NOT_NULL = "NOT NULL";
	public static final String CONSTRAINT_DEFAULT = "DEFAULT";
	public static final String CONSTRAINT_FOREIGN_KEY = "FOREIGN KEY";

	private static final String TYPE_TEXT = "TEXT";
	private static final String TYPE_INT = "INT";
	private static final String TYPE_REAL = "REAL";
	private static final String TYPE_NUM = "NUM";

	public SQLiteCreateBuilder(String tablename) {
		this.mTablename = tablename;
	}

	private void addColumn(String column, String type, String[] constraint) {
		if (mColumnsBuilder == null) {
			mColumnsBuilder = new StringBuilder();
		} else {
			mColumnsBuilder.append(",");
		}

		// "EMP_ID INT"
		mColumnsBuilder.append(" " + column + " " + type);
		mColumnList.add(column);

		if (constraint != null) {
			int constraintCount = constraint.length;

			for (int i = 0; i < constraintCount; i++) {
				// "EMP_ID INT PRIMARY KEY"
				mColumnsBuilder.append(" " + constraint[i]);
			}
		}
	}

	public SQLiteCreateBuilder addTextColumn(String column, String[] constraint) {
		this.addColumn(column, TYPE_TEXT, constraint);
		return this;
	}

	public SQLiteCreateBuilder addTextColumns(String... columns) {
		int columnCount = columns.length;
		for (int i = 0; i < columnCount; i++) {
			this.addColumn(columns[i], TYPE_TEXT, null);
		}
		return this;
	}

	public SQLiteCreateBuilder addNumColumn(String column, String[] constraint) {
		this.addColumn(column, TYPE_NUM, constraint);
		return this;
	}

	public SQLiteCreateBuilder addIntColumn(String column, String[] constraint) {
		this.addColumn(column, TYPE_INT, constraint);
		return this;
	}

	public SQLiteCreateBuilder addRealColumn(String column, String[] constraint) {
		this.addColumn(column, TYPE_REAL, constraint);
		return this;
	}



	/**
	 * 
	 * @param name 	constraint name
	 * @param type 	PRIMARY_KEY, UNIQUE or FOREIGN KEY
	 * @param columns 	string array of column name
	 * @param clause 	conflict-clause for PRIMARY_KEY and UNIQUE type can be null, foreign-key-clause for FOREIGN_KEY 
	 *
	 * @since 1.0
	 */
	private void addTableConstraint(String name, String type, String[] columns, String clause) {
		if (mTableConstraintBuilder == null) {
			mTableConstraintBuilder = new StringBuilder();
		} else {
			mTableConstraintBuilder.append(",");
		}

		
		if (!TextUtils.isEmpty(name)) {
			mTableConstraintBuilder.append(" CONSTRANINT " + name);
		}

		mTableConstraintBuilder.append(" " + type);

		int columnCount = columns.length;
		if (columnCount > 0) {
			int lastColumn = columnCount -1 ; 
			
			mTableConstraintBuilder.append(" (");
			for (int i = 0; i < columnCount; i++) {
				String col =  i != lastColumn ? columns[i] + "," : columns[i];
				mTableConstraintBuilder.append(" "+ col);
			}
			mTableConstraintBuilder.append(" )");
		}
		if (!TextUtils.isEmpty(clause)) {
			mTableConstraintBuilder.append(" "+ name);
		}
		
		
	}

	public SQLiteCreateBuilder addPrimaryKey(String... columns) {
		if(columns.length > 0);
			addTableConstraint(null, CONSTRAINT_PRIMARY_KEY, columns, null);
		return this;
	}
	
	public SQLiteCreateBuilder addUNIQUE(String... columns){
		if(columns.length > 0);
			addTableConstraint(null, CONSTRAINT_UNIQUE, columns, null);
		return this;
	}
	

	/**
	 * 
	 * @return String of SQL create statement
	 * 
	 * @since 1.0
	 */
	public String build() {
		StringBuilder sb = new StringBuilder("CREATE TABLE " + this.mTablename);
		sb.append("(");
		sb.append(this.mColumnsBuilder);
		
		if(mTableConstraintBuilder != null){
			sb.append(",");
			sb.append(this.mTableConstraintBuilder);
		}
		sb.append(")");

		return sb.toString();

	}

	/**
	 * build projection from what you add to this CreateBuilder
	 * 
	 * @return projection map of table
	 * 
	 * @since 1.0
	 */
	public HashMap<String, String> buildProjectionMap() {
		HashMap<String, String> map = new HashMap<String, String>();

		int columnCount = mColumnList.size();
		for (int i = 0; i < columnCount; i++) {
			String column = mColumnList.get(i);
			map.put(column, mTablename + "." + column);
		}

		return map;
	}

	/**
	 * ONLY work with column that key and value equal in PROJECTION_MAP NOTE all
	 * column will have no constraint property
	 * 
	 * @param map
	 *            projection map for use to build create statement
	 * 
	 * @since 1.0
	 */
	public SQLiteCreateBuilder fromProjectionMap(HashMap<String, String> map) {

		for (Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			if (key.equals(value)) {
				addColumn(key, TYPE_TEXT, null);
			}
			// do what you have to do here
			// In your case, an other loop.
		}

		return this;
	}

	@Override
	public String toString() {
		return "SQLiteCreateBuilder [tablename=" + mTablename
				+ ", columnsBuilder=" + mColumnsBuilder.toString()
				+ ", build()=" + build() + "]";
	}

}
