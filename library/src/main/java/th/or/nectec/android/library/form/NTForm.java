/* 
ห * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form;

import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import th.or.nectec.android.library.R;
import th.or.nectec.android.library.form.field.NTAddressField;
import th.or.nectec.android.library.form.field.NTArrayFormatSpinnerField;
import th.or.nectec.android.library.form.field.NTAutoCompleteTextField;
import th.or.nectec.android.library.form.field.NTCheckBoxField;
import th.or.nectec.android.library.form.field.NTCheckListField;
import th.or.nectec.android.library.form.field.NTDateField;
import th.or.nectec.android.library.form.field.NTLandAreaField;
import th.or.nectec.android.library.form.field.NTMultiChoiceField;
import th.or.nectec.android.library.form.field.NTNumberField;
import th.or.nectec.android.library.form.field.NTSingleChoiceField;
import th.or.nectec.android.library.form.field.NTTextField;
import th.or.nectec.android.library.form.generate.Field;
import th.or.nectec.android.library.form.generate.Value;
import th.or.nectec.android.library.widget.ArrayFormatSpinner;
import th.or.nectec.android.library.widget.LandAreaEditText;
import th.or.nectec.android.library.widget.ThaiDatePicker;
import th.or.nectec.android.library.widget.address.AddressView;

/**
 * Class for handle almost all boring coding process on create user-input Form
 * such as Validate, Restore Data if in edit-mode, Find View-Id And Build ContentValues
 *
 * <pre>
 * Note this package is Inspire by & Extend from
 * Project  Android-Validator
 * by throrin19@github
 * url https://github.com/throrin19/Android-Validator
 * </pre>
 *
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTForm implements LoaderCallbacks<Cursor> {

	private static final int RESTORE_DATA = 112;
	private static final int RESTORE_EAV_DATA = 113;
	Activity mActivity;
	View mView;

	ArrayList<NTField> mFieldList = new ArrayList<NTField>();

	private String mErrMessage;

	public NTForm(Activity activity) {
		mActivity = activity;
		mView = activity.getWindow().getDecorView()
				.findViewById(android.R.id.content);
	}

	public NTForm(Activity activity, View view) {
		mActivity = activity;
		mView = view;
	}

	public NTField addField(NTField field) {
		mFieldList.add(field);
		field.onAddToForm(this);
		return field;
	}

	public NTTextField addTextField(int id, String column) {
		TextView v = (TextView) mView.findViewById(id);
		NTTextField field = new NTTextField(v, column);
		this.addField(field);
		return field;
	}

    public NTDateField addDateField(int id, String column) {
        return addDateField(id, column, false);
    }

	public NTDateField addDateField(int id, String column, boolean needDefine) {
		ThaiDatePicker v = (ThaiDatePicker) mView.findViewById(id);
        v.setDefineable(needDefine);
		v.updateDate();
		NTDateField field = new NTDateField(v, column);
		this.addField(field);
		return field;
	}

	public NTArrayFormatSpinnerField addArrayFormatField(int id, String column,
			int arrayId) {
		ArrayFormatSpinner v = (ArrayFormatSpinner) mView.findViewById(id);
		v.setArray(arrayId);
		NTArrayFormatSpinnerField field = new NTArrayFormatSpinnerField(v,
				column);
		this.addField(field);
		return field;
	}

	public NTAddressField addAddressField(int id, String addressCode,
			String subdistrict, String district, String province) {

		AddressView address = (AddressView) mView.findViewById(id);

		NTAddressField field = new NTAddressField(address, addressCode,
				subdistrict, district, province);
		this.addField(field);
		return field;
	}
	
	public NTLandAreaField addLandAreaField(int id, String column) {
		LandAreaEditText v = (LandAreaEditText) mView.findViewById(id);
		NTLandAreaField field = new NTLandAreaField(v, column);
		this.addField(field);
		return field;
	}

	private String[] mExtraColumn;

	/**
	 * Add string array of Columns that not add as NTField for query with
	 * restoreData() for implement at onRestored() of RestoreListenter
	 * 
	 * @param columns
	 * 
	 * @since 1.0
	 */
	public void addExtraRestoreColumn(String... columns) {
		mExtraColumn = columns;
	}

	public void restoreData(Uri contentUri, String where, String[] whereArgs,
			RestoreDataListener listener) {
		Bundle args = new Bundle();
		args.putString("uri", contentUri.toString());
		args.putString("where", where);
		args.putStringArray("whereArgs", whereArgs);

		mRestoreListner = listener;

		mActivity.getLoaderManager().restartLoader(RESTORE_DATA, args, this);
	}

	public void restoreData(Uri contentUri, String where, String[] whereArgs) {
		this.restoreData(contentUri, where, whereArgs, null);
	}
	
	String mEavAttribute;
	String mEavValue;
	public void restoreEAVData(Uri contentUri, String attribute, String value, String where, String[] whereArgs){
		mEavAttribute = attribute;
		mEavValue = value;
		
		Bundle args = new Bundle();
		args.putString("uri", contentUri.toString());
		args.putString("where", where);
		args.putStringArray("whereArgs", whereArgs);

		mActivity.getLoaderManager().restartLoader(RESTORE_EAV_DATA, args, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Uri uri = Uri.parse(args.getString("uri"));
		String where = args.getString("where");
		String[] whereArgs = args.getStringArray("whereArgs");
		
		CursorLoader cl = null;
		switch(id){
		
		case RESTORE_DATA:
			cl = new CursorLoader(mActivity, uri, getProjection(),
				where, whereArgs, null);
			break;
		case RESTORE_EAV_DATA:
			cl = new CursorLoader(mActivity, uri, new String[]{ mEavAttribute, mEavValue},
					where, whereArgs, null);
			break;
		}

		return cl;
	}

	private String[] getProjection() {
		ArrayList<String> proj = new ArrayList<String>();

		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			NTField field = mFieldList.get(i);
			if (!field.isMultiColumn())
				proj.add(field.getColumn());
			else {
				String[] cols = ((IMultiColumnField) field).getColumns();
				int colCount = cols.length;
				for (int j = 0; j < colCount; j++) {
					proj.add(cols[j]);
				}
			}
		}

		if (mExtraColumn != null) {
			int extraColumnCount = mExtraColumn.length;
			for (int i = 0; i < extraColumnCount; i++) {
				proj.add(mExtraColumn[i]);
			}
		}

		return proj.toArray(new String[proj.size()]);
	}

	public Activity getActivity() {
		return mActivity;
	}

	/**
	 * 
	 * @param columns
	 *            columns of name to get
	 * @return Field that have columns name equal with input. Null otherwise
	 * 
	 * @since 1.0
	 */
	public NTField getFieldByColumn(String columns) {
		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			NTField field = mFieldList.get(i);

			if (columns.equalsIgnoreCase(field.getColumn())) {
				return field;
			}
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor c) {

		if (!c.moveToFirst()) {
			if (mRestoreListner != null) {
				mRestoreListner.onRestoreDataNotFound();
			}
			return;
		}

		switch(loader.getId()){
			case RESTORE_DATA:
				int fieldCount = mFieldList.size();
				for (int i = 0; i < fieldCount; i++) {
					NTField field = mFieldList.get(i);
		
					if (!field.isMultiColumn()) {
						String value = c.getString(c.getColumnIndex(field.getColumn()));
						if (!TextUtils.isEmpty(value)) {
							field.setValue(value);
							continue;
						}
					} else {
						IMultiColumnField mfield = (IMultiColumnField) field;
						mfield.setValueByCursor(c);
					}
				}
				break;
			case RESTORE_EAV_DATA:
				Log.d("NTForm", "not move to first");
				do{
					String attribute = c.getString(0);
					String value = c.getString(1);
					Log.d("NTForm", "attr="+attribute+" value="+value);
					
					NTField field = getFieldByColumn(attribute);
					if(field != null && !TextUtils.isEmpty(value))
						field.setValue(value);
				}while(c.moveToNext());
				break;
		}

		if (mRestoreListner != null)
			mRestoreListner.onRestored(c);

	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// Do nothing
	}
	
	public void setEnabled(boolean enabled){
		
		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			mFieldList.get(i).setEnabled(enabled);
		}
	}

	public boolean validate() {
		int invalid = 0;
		mErrMessage = null;

		boolean focused = false;
		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			NTField field = mFieldList.get(i);

			if (!field.isValid()) {
				invalid++;

				if (!focused) {
					mErrMessage = field.getMessages();
					field.getView().requestFocus();
					// set first invalid field to be focus;
					focused = true;
				}
			}
		}
		return invalid > 0 ? false : true;
	}

	public ContentValues buildContentValue() {
		ContentValues cv = new ContentValues();

		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			NTField field = mFieldList.get(i);
			if (!field.isMultiColumn()) {
				cv.put(field.getColumn(), field.getValue());
			} else {
				ContentValues v = ((IMultiColumnField) field)
						.getContentValues();
				cv.putAll(v);
			}
		}
		return cv;
	}
	
	
	public ContentValues[] buildEAVContentValue(String attributeColumnName, String valueColumnsName){
		int fieldCount = mFieldList.size();
		
		ContentValues[] cvs = new ContentValues[fieldCount];
		for (int i = 0; i < fieldCount; i++) {
			ContentValues cv = new ContentValues();
			
			NTField field = mFieldList.get(i);
			cv.put(attributeColumnName, field.getColumn());
			cv.put(valueColumnsName, field.getValue());
			
			cvs[i] = cv;
		}
		
		return cvs;
	}

	public ArrayList<NTField> getFields() {
		return mFieldList;
	}

	public void Log() {
		Log.d("NTForm", this.toString());

		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			NTField field = mFieldList.get(i);

			Log.d("NTForm", field.toString());
		}
	}

	public String getErrorMessage() {
		return mErrMessage;
	}

	public void buildForm() {
		if (mView instanceof ViewGroup)
			this.buildForm((ViewGroup) mView);
		else
			throw new IllegalArgumentException("Have no ViewGroup Can't build form");
	}

	public void buildForm(ViewGroup parent) {
		if (parent == null) {
			throw new NullPointerException(
					"Build Form container must not be Null. please check buildForm() input");
		}

		int fieldCount = mFieldList.size();
		for (int i = 0; i < fieldCount; i++) {
			boolean isLast = (i==fieldCount-1) ? true : false;
			boolean isNextQuestionNoTitle = (!isLast && TextUtils.isEmpty(mFieldList.get(i+1).getLabel())) ? true : false;
			NTField field = mFieldList.get(i);
			field.buildForm(mActivity, parent, isLast, isNextQuestionNoTitle);
		}
	}

	@Override
	public String toString() {
		return "NTForm [mFieldList=" + mFieldList.size() + ", getProjection()="
				+ Arrays.toString(getProjection()) + "]";
	}

	RestoreDataListener mRestoreListner;

	public interface RestoreDataListener {
		/**
		 * 
		 * @param c
		 *            cursor of data that query from restoreData() method
		 * 
		 * @since 1.0
		 */
		public void onRestored(Cursor c);

		public void onRestoreDataNotFound();
	}

	public static class Builder {
		
		public static final String TAG = "Form-Builder";

		public static final int TYPE_TEXT = 1;
		public static final int TYPE_INTEGER = 2;
		public static final int TYPE_DECIMAL = 3;
		public static final int TYPE_MULTICHOICE = 4;
		public static final int TYPE_SINGLECHOICE = 5;
		public static final int TYPE_ADDRESS = 6;
		public static final int TYPE_DATE = 7;
		public static final int TYPE_CHECKLIST = 8;
		public static final int TYPE_AUTOCOMPLETE = 9;
		public static final int TYPE_CHECKBOX = 10;

		public static HashMap<String, Integer> mFieldTypeMap;
		static {
			mFieldTypeMap = new HashMap<String, Integer>();
			mFieldTypeMap.put("text", TYPE_TEXT);
			mFieldTypeMap.put("integer", TYPE_INTEGER);
			mFieldTypeMap.put("decimal", TYPE_DECIMAL);
			mFieldTypeMap.put("multi-choice", TYPE_MULTICHOICE);
			mFieldTypeMap.put("checklist", TYPE_CHECKLIST);
			mFieldTypeMap.put("checkbox", TYPE_CHECKBOX);
			mFieldTypeMap.put("single-choice", TYPE_SINGLECHOICE);
			mFieldTypeMap.put("address", TYPE_ADDRESS);
			mFieldTypeMap.put("date", TYPE_DATE);
			mFieldTypeMap.put("autocomplete", TYPE_AUTOCOMPLETE);
		}

		Activity mActivity;
		TextView mNotFoundTxt;

		public Builder(Activity activity, Cursor c) {
			mActivity = activity;
			setCursor(c);

			mNotFoundTxt = new TextView(activity);
		}

		public static String[] REQUIRE_COLUMN = new String[] {
				NTFormGeneratorColumn.COLUMN, NTFormGeneratorColumn.COLUMNS,
				NTFormGeneratorColumn.LABEL, NTFormGeneratorColumn.MANDATORY,
				NTFormGeneratorColumn.TYPE, NTFormGeneratorColumn.VALIDATOR,
				NTFormGeneratorColumn.VALUE, NTFormGeneratorColumn.OPTION, };

		Field[] mField;

		public Builder setCursor(Cursor c) {

			int count = c.getCount();
			if (count == 0) {
				return null;
			}
			mField = new Field[count];
			for (int i = 0; i < count; i++) {
				c.moveToPosition(i);
				mField[i] = Field.createByCursor(c);
			}

			return this;
		}

		public NTForm build(ViewGroup parent) {
			if (parent == null)
				throw new IllegalArgumentException("parent view is null");

			if (mField.length == 0)
				parent.addView(mNotFoundTxt);

			NTForm form = new NTForm(mActivity);

			for (int i = 0; i < mField.length; i++) {
				Field f = mField[i];
				NTField ntField = null;
				switch (mFieldTypeMap.get(f.getType())) {
				case TYPE_TEXT:
					NTTextField txt = new NTTextField(f.getColumnName(),
							f.getLabel());
					ntField = txt;
					break;
				case TYPE_INTEGER:
					NTNumberField integer = new NTNumberField(f.getColumnName(),
							f.getLabel());
					integer.setNumberInputType(NTNumberField.INTEGER);
					ntField = integer;
					break;
				case TYPE_DECIMAL:
					NTNumberField decimal = new NTNumberField(f.getColumnName(),
							f.getLabel());
					decimal.setNumberInputType(NTNumberField.DECIMAL);
					ntField = decimal;
					break;
				case TYPE_MULTICHOICE:
					NTMultiChoiceField mc = new NTMultiChoiceField(
							f.getColumnsName(), f.getLabel(), Value.toString(f
									.getValues()));
					ntField = mc;
					break;
				case TYPE_CHECKLIST:
					NTCheckListField cl = new NTCheckListField(
							f.getColumnsName(), f.getLabel(), Value.toString(f
									.getValues()));
					ntField = cl;
					break;
				case TYPE_DATE:
					NTDateField dt = new NTDateField(f.getColumnName(),
							f.getLabel());
					ntField = dt;
					break;
				case TYPE_ADDRESS:
					NTAddressField ad;
					if (!TextUtils.isEmpty(f.getColumnName())){
						//if specify column prefix
						ad = new NTAddressField(f.getColumnName(), f.getLabel());
					}
					else{
						ad = new NTAddressField(f.getLabel());
					}
					ntField = ad;
					break;
				case TYPE_SINGLECHOICE:
					NTSingleChoiceField sc = new NTSingleChoiceField(
							f.getColumnName(), f.getLabel(), Value.toString(f
									.getValues()));
					ntField = sc;
					break;
				case TYPE_AUTOCOMPLETE:
					NTAutoCompleteTextField at = new NTAutoCompleteTextField(f.getColumnName(), f.getLabel());
					
					String[] suggest = new String[f.getValues().length];
					for(int valueIndex = 0 ; valueIndex < f.getValues().length ; valueIndex++){
						suggest[valueIndex] = f.getValues()[valueIndex].getLabel();
						Log.d("ttt", "suggest="+suggest[valueIndex]);
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, R.layout.default_spinner_item, suggest);
					at.setAdapter(adapter);
					
					ntField = at;
					break;
				case TYPE_CHECKBOX:
					NTCheckBoxField cb = new NTCheckBoxField(
							f.getColumnName(),
							f.getLabel(),
							Value.toString(f.getValues()));
					ntField = cb;
					break;
				}

				ntField.setFieldOption(f.getOption());
				if (f.isMandatory()){
					Log.d("NTForm", "mandatory="+f.isMandatory());
					ntField.setMandatory(mActivity);
				}
				form.addField(ntField);
			}

			form.buildForm(parent);
			return form;
		}
		
		public NTForm build() {
			View v = mActivity.getWindow().getDecorView()
					.findViewById(android.R.id.content);
			if (v instanceof ViewGroup) {
				ViewGroup parent = (ViewGroup) v;
				return this.build(parent);
			}else{
				Toast.makeText(mActivity, "can't build form", Toast.LENGTH_SHORT).show();
				return null;
			}
			
		}
	}
}
