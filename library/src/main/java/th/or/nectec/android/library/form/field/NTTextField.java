/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import th.or.nectec.android.library.form.validator.AbstractValidator;
import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.ClearableEditText;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class NTTextField extends NTField {
	
	

	/**
	 * @param view
	 * @param Column
	 */
	public NTTextField(TextView view, String Column) {
		super(view, Column);
	}
	
	public NTTextField(String column, String label){
		super(column, label);
	}

	@Override
	public String getValue() {
		return ((TextView) mView).getText().toString();
	}

	@Override
	public void setValue(String value) {
		((TextView) mView).setText(value);
	}
	
	@Override
	protected void onInvalid(AbstractValidator validator) {
		super.onInvalid(validator);
		((TextView) mView).setError(validator.getMessage());
	}
	
	@Override
	protected void onValid() {
		super.onValid();
		((EditText) mView).setError(null);
	}

	@Override
	public View onCreateView(Context context) {
		 ClearableEditText txt = new ClearableEditText(context, null);
		 //parent.addView(tv);
		 return txt;
	}

	
}
