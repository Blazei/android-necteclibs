package th.or.nectec.android.library.util;

import android.text.TextUtils;
import android.widget.EditText;

public class NumberEditTextUtils {

	public static int getInt(EditText view){
		
		int value;
		
		if(view != null){
			if(!TextUtils.isEmpty(view.getText().toString())){
				value = Integer.valueOf(view.getText().toString());
			}else{
				value = 0;
			}
		}else{
			value = 0;
		}
		
		return value;
	}
}
