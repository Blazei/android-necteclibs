/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public interface IMultiColumnField {

	
	public String[] getColumns();
	
	
	/**
	 * 
	 * set Value for Multi-column field by Cursor
	 * was called by onRestore data
	 * 
	 * @param c
	 *
	 * @since 1.0
	 */
	public  void setValueByCursor(Cursor c);
	
	public  ContentValues getContentValues();
}
