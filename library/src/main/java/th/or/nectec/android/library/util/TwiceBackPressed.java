package th.or.nectec.android.library.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by N. Choatravee on 22/12/2557.
 */
public class TwiceBackPressed {

    public static final int DURATION_SHORT = 0;
    public static final int DURATION_LONG = 1;

    private static final int TIMEOUT_SHORT = 2500;
    private static final int TIMEOUT_LONG = 4000;
    int mTimeout = TIMEOUT_SHORT;
    int mDelay = 500;
    long time1 = 0;
    long time2 = 0;

    String mMessage = "กรุณากดอีกครั้งเพื่อออก";

    Context mContext;

    public TwiceBackPressed(Context context){
        mContext = context;
    }

    /**
     *  @param duration set time duration when do double press. (short or long)
     */
    public void setToastDuration(int duration){
        switch(duration){
            case DURATION_LONG:
                mTimeout = TIMEOUT_LONG;
                break;
            case DURATION_SHORT:
            default:
                mTimeout = TIMEOUT_SHORT;
                break;
        }
    }

    /**
     *  @param message set message to notify before press button again.
     */
    public void setToastMessage(String message){
        mMessage = message;
    }

    /**
     *  @param stringResource set message to notify before press button again by using resource id.
     */
    public void setToastMessage(int stringResource){
        mMessage = mContext.getResources().getString(stringResource);
    }

    /**
     *  @return return status that can continue your action after do twice pressed.
     */
    public boolean onTwiceBackPressed(){

        if(time1==0){
            time1 = System.currentTimeMillis();
            int toastTimeOut = (mTimeout==TIMEOUT_SHORT) ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG;
            Toast.makeText(mContext, mMessage , toastTimeOut).show();
            return false;
        }else{
            time2 = System.currentTimeMillis();
        }

        long duration = time2-time1;

        if(duration<mDelay){
            Log.d("FAIL", "double press delay timeout (delay "+duration+"/"+mDelay+" ms)");
            return false;
        }else if(duration<mTimeout){
            Log.d("SUCCESS", "double press success (duration "+duration+")");
            return true;
        }else{
            Log.d("FAIL", "double press timeout (timeout "+duration+"/"+mTimeout+" ms)");
            time1 = 0;
            time2 = 0;
            onTwiceBackPressed();
        }

        return false;
    }
}
