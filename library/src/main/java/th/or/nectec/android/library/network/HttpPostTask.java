/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

/**
 * for example
 * -- sending post to server
 * <pre>
 * 
 *      String url = http://www.example.in.th/login.php";
 *		
 *		HashMap<String, String> post = new HashMap<String, String>();
 *		post.put("username", "user1");
 *		post.put("password", "1234");
 *		
 *		HttpPostTask.Parameter param = new HttpPostTask.Parameter();
 *		param.post = post;
 *		param.url = url;
 *		
 *		HttpPostTask task = new HttpPostTask(mPostCallBack);
 *		task.execute(param);
 *</pre>
 *-- receive response from server
 *<pre>
 *      HttpPostTask.TaskCallBack mPostCallBack = new HttpPostTask.TaskCallBack() {
 *		
 *		public void onProgressUpdate(Response... values) {
 *		}
 *		
 *		public void onPostExecute(ArrayList<Response> result) {
 *		}
 *		
 *		public void onIOException() {
 *		}
 *	};
 * </pre>
 * 
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * @deprecated
 * 
 */
public class HttpPostTask
		extends
		AsyncTask<HttpPostTask.Parameter, HttpPostTask.Response, ArrayList<HttpPostTask.Response>> {

	public static final String TAG = "PostTask";
	private TaskCallBack mCallback;

	public HttpPostTask(HttpPostTask.TaskCallBack callback) {
		mCallback = callback;
	}

	@Override
	protected ArrayList<HttpPostTask.Response> doInBackground(
			Parameter... params) {
		Log.e(TAG, "POST TASK START");
		ArrayList<Response> resList = new ArrayList<HttpPostTask.Response>();
		for (Parameter p : params) {
			Response res = new Response();
			try {

				Log.e(TAG, "POST TASK START");
				post(p.url, p.post, res);
				resList.add(res);
				publishProgress(res);
			} catch (IOException io) {
				Log.e(TAG, "IO error on PostTask", io);
				io.printStackTrace();
				res.status = 10;
				publishProgress(res);
			}
		}
		return resList;
	}

	private static String post(String endpoint, Map<String, String> params,
			Response res) throws IOException {
		URL url;
		try {
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			bodyBuilder.append(param.getKey()).append('=')
					.append(param.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}
		String body = bodyBuilder.toString();
		Log.v(TAG, "Posting '" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;

		conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		conn.setFixedLengthStreamingMode(bytes.length);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded;charset=UTF-8");
		// post the request
		OutputStream out = conn.getOutputStream();
		out.write(bytes);
		out.close();

		// handle the response
		int status = conn.getResponseCode();
		String message = conn.getResponseMessage();
		res.msg = message;
		res.status = status;
		if (status != 200) {
			throw new IOException("Post failed with error code=" + status
					+ ", msg=" + conn.getResponseMessage());
		}
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			line = line.trim();

			if (!TextUtils.isEmpty(line)) {
				res.data = (!TextUtils.isEmpty(res.data)) ? res.data + line
						: line;
				Log.i(TAG, "RES Message: " + line);
			}
		}
		rd.close();
		conn.disconnect();

		res.url = endpoint;
		// res.data = line;
		return line;

	}

	@Override
	protected void onProgressUpdate(HttpPostTask.Response... values) {
		super.onProgressUpdate(values);
		if (mCallback != null) {
			Log.d(TAG, "on Progress Update");
			if (values[0].status == 10) {
				mCallback.onIOException();
				Log.d(TAG, "IOException called");
			} else {
				mCallback.onProgressUpdate(values);
			}
		}
	}

	@Override
	protected void onPostExecute(ArrayList<Response> result) {
		super.onPostExecute(result);
		if (mCallback != null) {
			mCallback.onPostExecute(result);
		}
	}

	public static interface TaskCallBack {

		void onProgressUpdate(HttpPostTask.Response... values);

		void onPostExecute(ArrayList<Response> result);

		void onIOException();
	}

	public static class Parameter {
		public String url;
		public Map<String, String> post;

		public Parameter(){
			post = new HashMap<String, String>();
		}
		
		public void putPost(String key, String value){
			if(post == null)
				post = new HashMap<String, String>();
			post.put(key, value);
		}
	}

	public static class Response {
		public String url;
		public int status = 404;
		public String msg;
		public String data;
		@Override
		public String toString() {
			return "Response [url=" + url + ", status=" + status + ", msg="
					+ msg + ", data=" + data + "]";
		}
		
		
	}

}
