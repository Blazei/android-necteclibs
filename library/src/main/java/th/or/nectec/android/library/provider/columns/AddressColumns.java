/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.provider.columns;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public interface AddressColumns {

	public static final String ADDRESS_CODE = "address_code";
	public static final String HOUSE_NO = "house_no";
	public static final String VILLAGE = "village";
	public static final String TOWNSHIP = "township";
	public static final String ROAD = "road";
	public static final String ADDRESS = "address";
	public static final String SUBDISTRICT = "subdistrict";
	public static final String DISTRICT = "district";
	public static final String PROVINCE = "province";
	public static final String POSTCODE = "postcode";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ALTITUDE = "altitude";
	
}
