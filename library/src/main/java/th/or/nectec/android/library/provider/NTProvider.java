/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.provider;

import th.or.nectec.android.library.provider.NTTable.RefAddress;
import th.or.nectec.android.library.provider.NTTable.RefPrename;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 *
 * add description here!
 *
 * @version 1.0
 *
 * @author blaze
 *
 */
public class NTProvider extends ContentProvider {

	public static String AUTHORITY = "th.or.nectec.android.library.provider.NTProvider";

	public static final int ADDRESS = 100;
	public static final int PRENAME = 101;

	public static UriMatcher mUriMatcher;
	static {
		mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		mUriMatcher.addURI(AUTHORITY, "ref/address", ADDRESS);
		mUriMatcher.addURI(AUTHORITY, "ref/prename", PRENAME);
	}

	NTDatabaseOpenHelper mOpenHelper;

	@Override
	public boolean onCreate() {
		mOpenHelper = new NTDatabaseOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = mOpenHelper.getReadableDatabase();
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

		switch (mUriMatcher.match(uri)) {
		case ADDRESS:
			builder.setProjectionMap(RefAddress.PROJECTION_MAP);
			builder.setTables(RefAddress.TABLENAME);
			break;
		case PRENAME:
			builder.setProjectionMap(RefPrename.PROJECTION_MAP);
			builder.setTables(RefPrename.TABLENAME);
			break;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		Cursor cursor = builder.query(db, projection, selection, selectionArgs,
				null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		return 0;
	}

}
