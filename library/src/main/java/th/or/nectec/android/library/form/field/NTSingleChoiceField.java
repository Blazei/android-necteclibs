/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.CheckBoxesGroup;
import android.content.Context;
import android.util.Log;
import android.view.View;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTSingleChoiceField extends NTField {

	private String[] mChoice;

	public NTSingleChoiceField(CheckBoxesGroup view, String Column) {
		super(view, Column);
	}

	public NTSingleChoiceField(String column, String lable, String[] choice) {
		super(column, lable);
		mChoice = choice;
	}

	@Override
	public String getValue() {
		CheckBoxesGroup cbg = ((CheckBoxesGroup) mView);
		if (cbg.isChecked()) {
			return cbg.getCheckedItem().get(0);
		} else {
			return null;
		}
	}

	@Override
	public void setValue(String value) {
		((CheckBoxesGroup) mView).setChecked(true, value);
	}

	@Override
	public View onCreateView(Context context) {

		CheckBoxesGroup cbg = new CheckBoxesGroup(context, null);
		cbg.setMode(CheckBoxesGroup.SINGLE_CHOICE);
		cbg.setArray(mChoice);
		return cbg;

	}
}
