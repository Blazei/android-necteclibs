package th.or.nectec.android.library.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

public class NestedExpandableListView extends ExpandableListView implements OnTouchListener, OnScrollListener {

	private int listViewTouchAction;
	private static final int MAXIMUM_LIST_ITEMS_VIEWABLE = 99;

	public NestedExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		listViewTouchAction = -1;
		setOnScrollListener(this);
		setOnTouchListener(this);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		
		if (getExpandableListAdapter() != null && getExpandableListAdapter().getGroupCount() > MAXIMUM_LIST_ITEMS_VIEWABLE) {
			if (listViewTouchAction == MotionEvent.ACTION_MOVE) {
				scrollBy(0, -1);
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int newHeight = 0;
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		if (heightMode != MeasureSpec.EXACTLY) {
			ExpandableListAdapter expandListAdapter = getExpandableListAdapter();
			
			if (expandListAdapter != null && !expandListAdapter.isEmpty()) {
				
				int groupPosition = 0;
				int groupCount = getExpandableListAdapter().getGroupCount();
				for (groupPosition = 0 ; groupPosition < groupCount ; groupPosition++) {
					boolean isExpand = isGroupExpanded(groupPosition);
					View grouplistItem = expandListAdapter.getGroupView(groupPosition, isExpand, null, this);
					//now it will not throw a NPE if listItem is a ViewGroup instance
					if (grouplistItem instanceof ViewGroup) {
						grouplistItem.setLayoutParams(new LayoutParams(
								LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					}
					
					//get summation height of child
					int allChildHeight = 0;
					if(isExpand){
						int childCount = getExpandableListAdapter().getChildrenCount(groupPosition);
						int childPosition=0;
						
						for(childPosition=0; childPosition<childCount; childPosition++){
							boolean isLastChild = (childPosition<childCount) ? false : true;
							View childListItem = expandListAdapter.getChildView(groupPosition, childPosition, isLastChild, null, this);
							if (childListItem instanceof ViewGroup) {
								childListItem.setLayoutParams(new LayoutParams(
										LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
							}
							
							childListItem.measure(widthMeasureSpec, heightMeasureSpec);
							allChildHeight += childListItem.getMeasuredHeight();
						}
					}
					
					
					grouplistItem.measure(widthMeasureSpec, heightMeasureSpec);
					newHeight += grouplistItem.getMeasuredHeight()+allChildHeight;
				}
				newHeight += (getDividerHeight() * groupPosition)+getPaddingTop()+getPaddingBottom();
			}
			if ((heightMode == MeasureSpec.AT_MOST) && (newHeight > heightSize)) {
				if (newHeight > heightSize) {
					newHeight = heightSize;
				}
			}
		} else {
			newHeight = getMeasuredHeight();
		}
		setMeasuredDimension(getMeasuredWidth(), newHeight);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (getExpandableListAdapter() != null && getAdapter().getCount() > MAXIMUM_LIST_ITEMS_VIEWABLE) {
			Log.d("adapter count touch",getExpandableListAdapter().getGroupCount()+"");
			if (listViewTouchAction == MotionEvent.ACTION_MOVE) {
				scrollBy(0, 1);
			}
		}
		return false;
	}
}