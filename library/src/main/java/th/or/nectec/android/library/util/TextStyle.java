package th.or.nectec.android.library.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * Created by N. Choatravee on 1/7/2556.
 */
public class TextStyle {

    public static int dppx_conv(Context context, int dp){
        //convert from dp to px unit
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int)((dp * displayMetrics.density));
    }


    public static void SetFonts(Context context, ViewGroup parent, String fontname, float multiplies){

        AssetManager AssetMgr = context.getAssets();
        Typeface face = Typeface.createFromAsset(AssetMgr,"fonts/"+fontname);

        System.out.println("Child count "+parent.getChildCount());
        

        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);

            if (child instanceof LinearLayout) {
                //Support for EditText
                LinearLayout ln = (LinearLayout) child;
                SetFonts(context, ln, fontname, multiplies);

            }else if (child instanceof TableLayout) {
                //Support for EditText
                TableLayout tl = (TableLayout) child;
                SetFonts(context, tl, fontname, multiplies);

            }else if (child instanceof RelativeLayout) {
                //Support for EditText
            	RelativeLayout rel = (RelativeLayout) child;
                SetFonts(context, rel, fontname, multiplies);

            }else if (child instanceof TextView) {
                //Support for EditText
                TextView txt = (TextView) child;
                txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, txt.getTextSize()*multiplies);
                txt.setTypeface(face);

            }else if (child instanceof EditText) {
                //Support for EditText
                EditText et = (EditText) child;
                et.setTextSize(TypedValue.COMPLEX_UNIT_PX, et.getTextSize()*multiplies);
                et.setTypeface(face);

            }else if (child instanceof RadioGroup) {
                //Support for RadioGroups
                RadioGroup radio = (RadioGroup) child;
                for (int radioC=0; radioC<radio.getChildCount(); radioC++){
                    RadioButton rd = (RadioButton)radio.getChildAt(radioC);
                    rd.setTextSize(TypedValue.COMPLEX_UNIT_PX, rd.getTextSize()*multiplies);
                    rd.setTypeface(face);
                }

            }else if (child instanceof CheckBox) {
                //Support for Checkboxes
                CheckBox cb = (CheckBox) child;
                cb.setTextSize(TypedValue.COMPLEX_UNIT_PX, cb.getTextSize()*multiplies);
                cb.setTypeface(face);
            }else if (child instanceof Spinner) {
                //Support for Spinner
                Spinner sp = (Spinner) child;


            } else {


            }
        }
    }

}


