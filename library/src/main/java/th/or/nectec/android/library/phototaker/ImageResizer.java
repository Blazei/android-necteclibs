package th.or.nectec.android.library.phototaker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

public class ImageResizer {

	public static final String TAG = "ImageResizer";
	protected int outputX = 100;
	protected int outputQuality = 90;
	
	public ImageResizer(int minLenght){
		outputX = minLenght;
	}
	
	public ImageResizer(){
		outputX = 100;
	}
	
	public void setQuality(int qPercentage){
		if(qPercentage > 100){
			outputQuality = 100;
		}else if(qPercentage < 0){
			outputQuality = 0;
		}else{
			outputQuality = qPercentage;
		}
		
	}

	public boolean doResizeImage(File input, File output, boolean deleteInput) {
		if (!input.exists())
			return false;

		try {
			Bitmap bitmap = BitmapFactory.decodeFile(input.getAbsolutePath());
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			
			int least = (width < height) ? width : height;
			int newWidth = outputX;

			// calculate the scale
			float scaleWidth, scaleHeight = scaleWidth = ((float) newWidth) / least;
			// float scaleHeight = scaleWidth;

			// createa matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);
			Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width,
					height, matrix, true);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			resizedBitmap.compress(Bitmap.CompressFormat.JPEG, outputQuality, baos);
			// Create Image File from baos
			intoJPEGfile(baos.toByteArray(), output);

			if (deleteInput)
				input.delete();
			return true;
		} catch (Exception ex) {
			Log.e(TAG, "Delete temp file cause by can't ResizeImage");
			return false;
		}
	}

	private void intoJPEGfile(byte[] imageData, File output) {
		try {
			if(!output.exists()){
				output.createNewFile();
			}
			
			FileOutputStream buf;
			buf = new FileOutputStream(output);
			buf.write(imageData);
			buf.flush();
			buf.close();
		} catch (Exception e) {
			Log.v(TAG, "error while write picture to file");
			e.printStackTrace();
		}
	}

	public boolean doResizeImage(File input, File output) {
		return doResizeImage(input, output, true);
	}

}
