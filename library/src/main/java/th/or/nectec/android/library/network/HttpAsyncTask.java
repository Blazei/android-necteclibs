/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

/**
 * Improved version of HttpPostTask for support all HTTP Method to compatible
 * with RESTful API
 * 
 * for example -- sending post to server
 * 
 * <pre>
 * 
 *      String url = http://www.example.in.th/login.php";
 * 	
 * 	HashMap<String, String> post = new HashMap<String, String>();
 * 	post.put("username", "user1");
 * 	post.put("password", "1234");
 * 	
 * 	HttpAsyncTask.Request request = new HttpAsyncTask.Request(url, HttpAsyncTask.Request.METHOD_POST);
 * 	request.post = post;
 * 	request.url = url;
 * 	
 * 	HttpPostTask task = new HttpPostTask(mPostCallBack);
 * 	task.execute(request);
 * </pre>
 * 
 * -- receive response from server
 * 
 * <pre>
 * HttpPostTask.TaskCallBack mPostCallBack = new HttpPostTask.TaskCallBack() {
 * 
 * 	public void onProgressUpdate(Response... values) {
 * 	}
 * 
 * 	public void onPostExecute(ArrayList&lt;Response&gt; result) {
 * 	}
 * 
 * 	public void onIOException() {
 * 	}
 * };
 * </pre>
 * 
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * 
 */
public class HttpAsyncTask
		extends
		AsyncTask<HttpAsyncTask.Request, HttpAsyncTask.Response, ArrayList<HttpAsyncTask.Response>> {

	public static final String TAG = "PostTask";
	public static final int TIME_OUT = 30000;
	public static final int SLEEP_TIME = 500;
	private TaskCallBack mCallback;
	private int maxAttempt = 3;

	public HttpAsyncTask(HttpAsyncTask.TaskCallBack callback) {
		mCallback = callback;
	}
	
	public void setMaxAttempt(int maxAttempt){
		if(maxAttempt < 1)
			maxAttempt = 1;
		if(maxAttempt > 10)
			maxAttempt = 10;
		
		this.maxAttempt = maxAttempt;
	}

	@Override
	protected ArrayList<HttpAsyncTask.Response> doInBackground(
			Request... request) {
		ArrayList<Response> resList = new ArrayList<HttpAsyncTask.Response>();
		for (Request req : request) {
			Response res = new Response();
			for (int i = 0; i < maxAttempt; i++) {
				try {

					Log.e(TAG, "HTTP TASK :"+ req.url + "&attempt="+(i+1));
					connect(req, res);
					resList.add(res);
					publishProgress(res);
					Thread.sleep(SLEEP_TIME);
					break;
					
				} catch (SocketTimeoutException se) {
					Log.e(TAG, "Socket Timeout");
					continue;
					
				} catch (IOException io) {
					Log.e(TAG, "IO error on PostTask", io);
					io.printStackTrace();
					res.status = 10;
					publishProgress(res);
					break;
				} catch (InterruptedException e) {
					Log.e(TAG, "Interrupted error on PostTask", e);
					e.printStackTrace();
					break;
				} 
			}
		}
		return resList;
	}

	private static String connect(HttpAsyncTask.Request request, Response res)
			throws IOException, SocketTimeoutException {

		StringBuilder bodyBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = request.params.entrySet()
				.iterator();
		// constructs the POST body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> params = iterator.next();
			bodyBuilder.append(params.getKey()).append('=')
					.append(params.getValue());
			if (iterator.hasNext()) {
				bodyBuilder.append('&');
			}
		}

		String body = bodyBuilder.toString();
		String endpoint = request.url;
		URL url;
		try {
			if (request.method.equals(Request.METHOD_GET) && body.length() > 0) {
				endpoint = endpoint + "?" + body;
			}
			url = new URL(endpoint);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		Log.v(TAG, "Requesting '" + request.method + "" + body + "' to " + url);
		byte[] bytes = body.getBytes();
		HttpURLConnection conn = null;

		conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(TIME_OUT);
		conn.setReadTimeout(TIME_OUT);
		conn.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded;charset=UTF-8");
		conn.setUseCaches(false);
		conn.setRequestMethod(request.method);

		if (request.method.equals(Request.METHOD_POST)) {
			conn.setDoOutput(true);
			conn.setFixedLengthStreamingMode(bytes.length);

			// post the request
			OutputStream out = conn.getOutputStream();
			out.write(bytes);
			out.close();
		}

		// handle the response
		int status = conn.getResponseCode();
		String message = conn.getResponseMessage();
		res.msg = message;
		res.status = status;
		if (status != 200) {
			throw new IOException("Request failed with error code=" + status
					+ ", msg=" + conn.getResponseMessage());
		}
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			line = line.trim();

			if (!TextUtils.isEmpty(line)) {
				res.data = (!TextUtils.isEmpty(res.data)) ? res.data + line
						: line;
				Log.i(TAG, "RES Message: " + line);
			}
		}
		rd.close();
		conn.disconnect();

		res.url = request.url;
		return line;

	}

	@Override
	protected void onProgressUpdate(HttpAsyncTask.Response... values) {
		super.onProgressUpdate(values);
		if (mCallback != null) {
			Log.d(TAG, "on Progress Update");
			if (values[0].status == 10) {
				mCallback.onIOException();
				Log.d(TAG, "IOException called");
			} else {
				mCallback.onProgressUpdate(values);
			}
		}
	}

	@Override
	protected void onPostExecute(ArrayList<Response> result) {
		super.onPostExecute(result);
		if (mCallback != null) {
			mCallback.onPostExecute(result);
		}
	}

	public static interface TaskCallBack {

		void onProgressUpdate(HttpAsyncTask.Response... values);

		void onPostExecute(ArrayList<Response> result);

		void onIOException();
	}

	public static class Request {

		public static final String METHOD_DELETE = "DELETE";
		public static final String METHOD_GET = "GET";
		public static final String METHOD_POST = "POST";
		public static final String METHOD_PUT = "PUT";

		public String url;
		public String method;
		public Map<String, String> params;

		public Request(String endpoint, String method) {
			this.url = endpoint;
			this.method = method;
			this.params = new HashMap<String, String>();
		}

		/**
		 * Convenience for new Request(endpoint, Request.METHODE_POST)
		 * 
		 * @param endpoint
		 */
		public Request(String endpoint) {
			this(endpoint, METHOD_POST);
		}

		public void putParam(String key, String value) {
			if (params == null)
				params = new HashMap<String, String>();
			params.put(key, value);
		}
	}

	public static class Response {
		public String url;
		public int status = 404;
		public String msg;
		public String data;

		@Override
		public String toString() {
			return "Response [url=" + url + ", status=" + status + ", msg="
					+ msg + ", data=" + data + "]";
		}

	}

}
