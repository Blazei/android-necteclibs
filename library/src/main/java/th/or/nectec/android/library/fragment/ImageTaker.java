package th.or.nectec.android.library.fragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import th.or.nectec.android.library.phototaker.ImageResizer;
import th.or.nectec.android.library.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridLayout;
import android.widget.ImageView;

public class ImageTaker extends Fragment implements View.OnClickListener,
		View.OnLongClickListener {

	/**
	 * int argument to set operation mode default is MODE_EDIT
	 */
	public static final String ARGS_MODE = "mode";
	public static final int MODE_EDIT = 0;
	public static final int MODE_VIEW = 1;

	public static final int DEFAULT_ITEM_SIZE = 256;
	private static final int IMAGE_ALPHA = (int) (255 * 0.76f);

	/**
	 * int argument to specify root layout should be id of layout that have
	 * ScrollView at root
	 */
	public static final String ARGS_LAYOUT_ID = "layout_id";
	/**
	 * int argument of layout id to be item layout that have image view within
	 * it
	 */
	public static final String ARGS_ITEM_LAYOUT_ID = "item_id";

	/**
	 * ImageView's id in layout NOTE this is most important argument
	 */
	public static final String ARGS_ITEM_IMAGE_ID = "item_img_id";
	
	/**
	 * int argument to specify image item width and height default are
	 * WRAP_CONTENT this argument ignore if container view is GridLayout
	 */
	public static final String ARGS_ITEM_SIZE = "item_size";

	/**
	 * int argument of container layout within root layout may it was id of
	 * linearlayout in scollview
	 */
	public static final String ARGS_CONTAINER_ID = "container_id";

	/**
	 * String array of image name to show as initial image
	 */
	public static final String ARGS_RESTORE_IMAGE = "restore_image";

	/**
	 * int argument of column when container view is GridLayout
	 */
	public static final String ARGS_ITEM_COLUMN_COUNT = "columns_count";

	
	private static final int REQUEST_DOC_CAPTURE = 3021;

	/**
	 * this parameter use only if container is GridLayout
	 */
	protected int mItemColumnCount = 3;
	
	private int mImageCounter = 1;
	private String shootImageName;

	private ViewGroup mContainer;
	protected int mItemLayout;
	protected int mItemImageId;
	protected int mItemSize = DEFAULT_ITEM_SIZE;
	private int mMode;
	
	

	OnDeleteRequestListener mDeleteListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		int layout  = R.layout.image_taker;
		int containerId = R.id.container;
		Bundle args = getArguments();
		if (args != null) {

			layout = args.getInt(ARGS_LAYOUT_ID, R.layout.image_taker);
			containerId = args.getInt(ARGS_CONTAINER_ID, R.id.container);
		}

		View v = inflater.inflate(layout, container, false);
		mContainer = (ViewGroup) v.findViewById(containerId);
		return v;
	}

	private ArrayList<String> mImageList = null;
	private ArrayList<String> mAddList = new ArrayList<String>();
	private ArrayList<String> mDeletedList = new ArrayList<String>();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			mItemLayout = args.getInt(ARGS_ITEM_LAYOUT_ID, R.layout.image_item);
			mItemImageId = args.getInt(ARGS_ITEM_IMAGE_ID, R.id.image);
			mItemSize = args.getInt(ARGS_ITEM_SIZE, DEFAULT_ITEM_SIZE);
			mItemColumnCount = args.getInt(ARGS_ITEM_COLUMN_COUNT, mItemColumnCount);
		}

		boolean initailed = false;
		if (savedInstanceState != null) {
			mImageList = savedInstanceState.getStringArrayList("pic");
			mAddList = savedInstanceState.getStringArrayList("add");
			mDeletedList = savedInstanceState.getStringArrayList("delete");
			shootImageName = savedInstanceState.getString("shootname");
			mImageCounter = savedInstanceState.getInt("count", 1);
			initailed = savedInstanceState.getBoolean("initialed");
		}

		if (!initailed) {
			mImageList = args.getStringArrayList(ARGS_RESTORE_IMAGE);
		}

		if (mImageList == null)
			mImageList = new ArrayList<String>();

		addEmptyItem();

		if (mImageList.size() > 0) {
			restoreImage();
		}

	}
	
	public void setItemSize(int width){
		mItemSize = width;
		getArguments().putInt(ARGS_ITEM_SIZE, width);
		
		
		mContainer.removeAllViews();
		addEmptyItem();
		if (mImageList.size() > 0) {
			restoreImage();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putStringArrayList("pic", mImageList);
		outState.putString("shootname", shootImageName);
		outState.putInt("count", mImageCounter);
		outState.putBoolean("initialed", true);
		outState.putStringArrayList("add", mAddList);
		outState.putStringArrayList("delete", mDeletedList);
	}
	
	public static final int THUMB_SIZE = 256;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_DOC_CAPTURE) {
			if (resultCode == Activity.RESULT_OK) {
				File cache = getActivity().getExternalCacheDir();
				File imgi = new File(cache, shootImageName);
				if (!imgi.exists()) {
					shootImageName = null;
					return;
				}

				ImageResizer ir = new ImageResizer(THUMB_SIZE);
				File sd = new File(cache, "thumb_" + shootImageName);
				ir.doResizeImage(imgi, sd, false);


				ImageView img = onCreateImageItem(sd.getAbsolutePath(),
						imgi.getAbsolutePath());

				mContainer.addView(img, 1, onCreateLayoutParam());

				mImageList.add(imgi.getAbsolutePath());
				mAddList.add(imgi.getAbsolutePath());
			}
		} else {
			shootImageName = null;
		}
	}

	protected ImageView onCreateImageItem( String srcPath,
			String tag) {
		
		View item = LayoutInflater.from(getActivity()).inflate(mItemLayout,
				null);
		ImageView img = (ImageView) item.findViewById(mItemImageId);
		if(!TextUtils.isEmpty(srcPath))
			img.setImageDrawable(Drawable.createFromPath(srcPath));
		img.setTag(tag);
		img.setOnClickListener(this);
		img.setOnLongClickListener(this);

		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			img.setImageAlpha(IMAGE_ALPHA);
		}else{
			img.setAlpha(0.75f);
		}
		return img;
	}

	protected LayoutParams onCreateLayoutParam() {
		int itemSize = this.mItemSize;
		
		if(mContainer instanceof GridLayout){
			int width = (int) (mContainer.getMeasuredWidth() * 0.99);
			if(width > 0)
				itemSize = width / mItemColumnCount;
		}

		return new LayoutParams(itemSize, itemSize);
	}

	/**
	 * 
	 * @param restoreList Array List of image path 
	 *
	 * @since 1.0
	 */
	public void setRestoreImageList(ArrayList<String> restoreList) {

		if (mImageList.size() == 0) {
			mImageList = new ArrayList<String>(restoreList);
		} else {
			mImageList.addAll(mImageList.size() - 1, restoreList);
		}

		mContainer.removeAllViews();
		addEmptyItem();
		if (mImageList.size() > 0) {
			restoreImage();
		}

	}

	private void restoreImage() {
		int addPostion = mContainer.getChildCount() >= 1 ? 1 : 0;

		LayoutParams itemParam = onCreateLayoutParam();

		for (int i = 0; i < mImageList.size(); i++) {
			

			File file = new File(mImageList.get(i));

			String absolutePath = file.getAbsolutePath();
			String filePath = absolutePath.substring(0,
					absolutePath.lastIndexOf(File.separator));
			File thumb = new File(filePath, "thumb_" + file.getName());

			if (!thumb.exists()) {
				ImageResizer ir = new ImageResizer();
				ir.doResizeImage(file, thumb, false);
			}

		
			ImageView img = onCreateImageItem(thumb.getAbsolutePath(),
					mImageList.get(i));

			mContainer.addView(img, addPostion, itemParam);
			
		}
	}

	private void addEmptyItem() {
		mMode = getArguments().getInt(ARGS_MODE, MODE_EDIT);
		if (mMode == MODE_VIEW)
			return;

		ImageView img = onCreateImageItem(null, "new");
		mContainer.addView(img, onCreateLayoutParam());
	}

	@Override
	public final void onClick(View v) {
		String tag = (String) v.getTag();
		if (!tag.equals("new")) {
			Intent intent = new Intent();
			intent.setAction(android.content.Intent.ACTION_VIEW);
			File file = new File(tag);
			intent.setDataAndType(Uri.fromFile(file), "image/jpeg");
			startActivity(intent);
		} else {
			Date today = new Date();
			SimpleDateFormat f_id = new SimpleDateFormat("yyMMddkkmmssSSS",
					Locale.US);

			String name = f_id.format(today);
			File image = new File(getActivity().getExternalCacheDir(),
					String.format("imgtaker_%s.jpg", name));

			Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
			camera.putExtra("outputFormat", CompressFormat.JPEG.toString());
			camera.putExtra("return-data", true);

			shootImageName = image.getName();
			startActivityForResult(camera, REQUEST_DOC_CAPTURE);
		}
	}

	@Override
	public final boolean onLongClick(View v) {
		String tag = (String) v.getTag();
		if (!tag.equals("new")) {

			int mode = getArguments().getInt(ARGS_MODE, MODE_EDIT);
			if (mode == MODE_VIEW)
				return false;

			if (mDeleteListener != null) {
				if (mDeleteListener.onDeleteRequest(v, tag)) {
					// if onDeleteRequest return true it mean user will manual
					// delete item
					// with removeItem()
					return true;
				}
			}

			removeItem(v, tag);
			return true;
		}
		return false;
	}

	public void setOnDeleteRequestListener(OnDeleteRequestListener listener) {
		this.mDeleteListener = listener;
	}

	/**
	 * 
	 * @param v
	 *            item view to be remove
	 * @param path
	 *            absolute file path to remove from fragment memory NOTE it
	 *            still be on storage!
	 */
	public void removeItem(View v, String path) {
		mContainer.removeView(v);
		mImageList.remove(path);

		if (!mAddList.remove(path)) {
			Log.d("dd", "removed path=" + path);
			mDeletedList.add(path);
		} else {
			File f = new File(path);
			f.delete();
		}
	}

	/**
	 * @return list of all image file
	 */
	public ArrayList<String> getImageList() {
		return mImageList;
	}

	/**
	 * <pre>
	 * new image file will be store at application external cache directory with name format 
	 * "imgtaker_[yyMMddkkmmssSSS].jpg" 
	 * 
	 * NOTE there is no '[' or ']'
	 * </pre>
	 * 
	 * @return list of recent added Image not including ARGS_RESTORE_IMAGE array
	 *         list
	 */
	public ArrayList<String> getAddedImageList() {
		return mAddList;
	}

	/**
	 * @return list of file path that was removed from ARGS_RESTORE_IMAGE array
	 *         list
	 */
	public ArrayList<String> getDeleteImageList() {
		return mDeletedList;
	}

	public static ImageTaker getNewInstance(int imageItemLayout,
			int imageItemId, ArrayList<String> restoreFileName, int mode) {
		Bundle args = new Bundle();
		args.putInt(ImageTaker.ARGS_LAYOUT_ID, R.layout.image_taker);
		args.putInt(ImageTaker.ARGS_CONTAINER_ID, R.id.container);
		args.putInt(ImageTaker.ARGS_ITEM_LAYOUT_ID, imageItemLayout);
		args.putInt(ImageTaker.ARGS_ITEM_IMAGE_ID, imageItemId);
		args.putStringArrayList(ImageTaker.ARGS_RESTORE_IMAGE, restoreFileName);
		args.putInt(ImageTaker.ARGS_MODE, mode);

		ImageTaker it = new ImageTaker();
		it.setArguments(args);

		return it;
	}

	public static ImageTaker getNewInstance(int imageItemLayout,
			int imageItemId, ArrayList<String> restoreFileName) {
		return getNewInstance(imageItemLayout, imageItemId, restoreFileName,
				MODE_EDIT);
	}

	public static ImageTaker getNewInstance(int imageItemLayout, int imageItemId) {
		return getNewInstance(imageItemLayout, imageItemId, null);
	}

	public static ImageTaker getNewInstance(ArrayList<String> restoreFileName) {
		restoreFileName = restoreFileName == null ? new ArrayList<String>()
				: restoreFileName;
		return getNewInstance(R.layout.image_item, R.id.image, restoreFileName);
	}

	public static ImageTaker getNewInstanceViewMode(
			ArrayList<String> restoreFileName) {
		restoreFileName = restoreFileName == null ? new ArrayList<String>()
				: restoreFileName;
		return getNewInstance(R.layout.image_item, R.id.image, restoreFileName,
				MODE_VIEW);
	}

	public interface OnDeleteRequestListener {

		/**
		 * Listener method calling by user long click on view item to Request
		 * Delete Function
		 * 
		 * @param v
		 *            item view be requested to remove
		 * @param path
		 *            absolute file path to be currently display on v item
		 * @return true if u will manual remove, false if use build-in remove
		 *         function
		 */
		public boolean onDeleteRequest(View v, String path);
	}

}
