/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class FontUtils {

	Context context;
	String font;
	
	Typeface typeface;
	
	public FontUtils(Context context, String font){
		this.context = context;
		this.font = font;
		
		this.typeface = Typeface.createFromAsset(this.context.getAssets(), this.font);
	}
	
	public void overrideFonts(final View v) {
	    try {
	        if (v instanceof ViewGroup) {
	            ViewGroup vg = (ViewGroup) v;
	            for (int i = 0; i < vg.getChildCount(); i++) {
	                View child = vg.getChildAt(i);
	                this.overrideFonts(child);
	         }
	        } else if (v instanceof TextView ) {
	            ((TextView) v).setTypeface(this.typeface);
	        }
	    } catch (Exception e) {
	 }
	 }
}
