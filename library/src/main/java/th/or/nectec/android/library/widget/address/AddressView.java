/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget.address;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import th.or.nectec.android.library.provider.NTTable.RefAddress;
import th.or.nectec.android.library.util.StringUtils;
import th.or.nectec.android.library.widget.address.AddressDialog.onAddressListener;

/**
 * Address View is Button in EditText skin. open AddressDialog for easy and secure way
 * insert address data from user whenever user click on this view
 * <pre>
 * HOW TO USE
 * 	1.copy asset/RefAddress.csv in NectecLibrary to asset folder of application
 * 	2.copy provider tag of NTProvider in NectecLibrary AndroidManifest.xml
 * 		 to AndroidManifest.xml of application project
 * 	3.have fun!
 * </pre>
 *
 * @author blaze
 * @version 1.0
 */
public class AddressView extends Button implements onAddressListener {

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.editTextStyle);
//		if (!ViewUtils.isSetBackground(context, attrs, 0)) {
//
//			TypedArray att = context.getTheme().obtainStyledAttributes(
//					new int[] { android.R.attr.editTextBackground });
//			try {
//				Drawable d = att.getDrawable(0);
//				this.setBackgroundDrawable(d);
//				this.setFocusable(true);
//
//			} finally {
//				att.recycle();
//			}
//		}

        if (context instanceof Activity) {
            Activity act = (Activity) context;
            initialize(act, RefAddress.CONTENT_URI);
        }
    }

    private Activity mActivity;
    private Uri mUri;
    private AddressDialog mDialog;

    private void initialize(Activity activity, Uri uri) {
        mActivity = activity;
        mUri = uri;

        Log.d("dialog", "init");

        if (mActivity != null && mUri != null) {
            FragmentManager fm = mActivity.getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            AddressDialog prev = (AddressDialog) fm.findFragmentByTag(tag);
            if (prev != null) {
                mDialog = prev;
            } else {
                mDialog = AddressDialog.newInstance(mUri);
            }

            mDialog.setOnAddressSelectListener(this);
        } else {
            Log.d("dialog", "null null null");
        }
    }

    String tag = "address-dialog";


    @Override
    public boolean performClick() {
        boolean handle = false;
        this.requestFocus();
        if (mActivity != null && mUri != null) {
            FragmentManager fm = mActivity.getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment prev = fm.findFragmentByTag(tag);

            if (prev != null)
                ft.remove(prev);

            // ft.addToBackStack(null);

            if (mDialog != null) {
                mDialog.setAddress(subdistrict, district, province);
                mDialog.setOnAddressSelectListener(this);
                mDialog.show(fm, tag);
                handle = true;
            }
            ft.commit();
        }
        return handle;
    }

    /**
     * @return the code
     */
    public String getAddressCode() {
        return code;
    }

    /**
     * @return the mSubdistrict
     */
    public String getSubdistrict() {
        return subdistrict;
    }

    /**
     * @return the mDistrict
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    @Override
    public void onAddressSelect(String code, String s, String d, String p) {

        setLocale(code, s, d, p);
    }

    /**
     * @param code        Address code of selected address, optional
     * @param subdistrict name of mSubdistrict to set
     * @param district    name of mDistrict to set
     * @param province    name of province to set
     * @return string that show on AddressView same as AddressView.getText()
     * @since 1.0
     */
    public String setLocale(String code, String subdistrict, String district,
                            String province) {

        String str = null;
        if (StringUtils.isEmpty(province, district, subdistrict)) {
            return str;
        } else if (TextUtils.isEmpty(code)) {
            Cursor findAddressCode = getContext().getContentResolver().query(
                    RefAddress.CONTENT_URI, new String[]{RefAddress.ADDRESS_CODE},
                    RefAddress.SUBDISTRICT + "=? AND " + RefAddress.DISTRICT + "=? AND " + RefAddress.PROVINCE + "=?",
                    new String[]{subdistrict, district, province}, null);
            if (findAddressCode.moveToNext()) {
                this.code = findAddressCode.getString(0);
            }
        } else {
            this.code = code;
        }

        this.subdistrict = subdistrict;
        this.district = district;
        this.province = province;

        if (province.equals("กรุงเทพมหานคร")) {

            str = "แขวง" + subdistrict + " เขต" + district + " " + province;
        } else {
            str = "ต." + subdistrict + " อ." + district + " จ." + province;
        }
        this.setText(str);
        return str;
    }

    public void clearLocale() {
        this.code = null;
        this.subdistrict = null;
        this.district = null;
        this.province = null;
        this.setText("");
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        SavedState ss = new SavedState(superState);
        ss.code = this.code;
        ss.province = this.province;
        ss.district = this.district;
        ss.subdistrict = this.subdistrict;

        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        onAddressSelect(ss.code, ss.subdistrict, ss.district, ss.province);
    }

    String code, subdistrict, district, province;

    static class SavedState extends BaseSavedState {
        String code, subdistrict, district, province;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.code = in.readString();
            this.province = in.readString();
            this.district = in.readString();
            this.subdistrict = in.readString();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeString(this.code);
            out.writeString(this.province);
            out.writeString(this.district);
            out.writeString(this.subdistrict);
        }

        // required field that makes Parcelables from a Parcel
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

}
