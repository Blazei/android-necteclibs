/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.field;

import th.or.nectec.android.library.form.NTField;
import th.or.nectec.android.library.widget.LandAreaEditText;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author N. Choatravee
 *
 */
public class NTLandAreaField extends NTField {

	/**
	 * @param view
	 * @param Column
	 */
	public NTLandAreaField(LandAreaEditText view, String Column) {
		super(view, Column);
	}
	
	public NTLandAreaField(String column, String label){
		super(column, label);
	}

	@Override
	public String getValue() {
		return String.valueOf(((LandAreaEditText) mView).getArea());
	}

	@Override
	public void setValue(String value) {
		
		if(TextUtils.isEmpty(value)){
			return;
		}else{
			double area = Double.valueOf(value);
			((LandAreaEditText) mView).setArea((int) area);
		}
	}

	@Override
	public View onCreateView(Context context) {
		LandAreaEditText area = new LandAreaEditText(context, null);
		 //parent.addView(tv);
		 return area;
	}

	
}
