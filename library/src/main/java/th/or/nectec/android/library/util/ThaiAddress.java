package th.or.nectec.android.library.util;

import android.text.TextUtils;

public class ThaiAddress {

	public static String sortAddress(String home_no, String township, String village,
			String sidestreet, String road, String subdistrict, String district, String province) {
		String address = "";
		if (!TextUtils.isEmpty(home_no)) {
			address += "บ้านเลขที่ "+home_no + " ";
		}
		if (!TextUtils.isEmpty(township)) {
			address += "หมู่ที่ " + township + "\n";
		}
		if (!TextUtils.isEmpty(village)) {
			address += "หมู่บ้าน" + village+" ";
		}
		if (!TextUtils.isEmpty(sidestreet)) {
			address += "ซอย" + sidestreet+" ";
		}
		if (!TextUtils.isEmpty(road)) {
			address += "ถนน" + road + "\n";
		}

		if(!TextUtils.isEmpty(province)){
			if(province.equals("กรุงเทพมหานคร")){
				if (!TextUtils.isEmpty(subdistrict)) {
					address +="แขวง"+subdistrict+" ";
				}
				if (!TextUtils.isEmpty(district)) {
					address +="เขต"+district+" "+province;
				}
			}else{
				if (!TextUtils.isEmpty(subdistrict)) {
					address +="ตำบล"+subdistrict+" ";
				}
				if (!TextUtils.isEmpty(district)) {
					address +="อำเภอ"+district+"\nจังหวัด"+province;
				};
			}
		}
		return address;
	}

    public static String sortAddress(String subdistrict, String district, String province){
        String address = "";
        if(!TextUtils.isEmpty(province)){
            if(province.equals("กรุงเทพมหานคร")){
                if (!TextUtils.isEmpty(subdistrict)) {
                    address +="แขวง"+subdistrict+" ";
                }
                if (!TextUtils.isEmpty(district)) {
                    address +="เขต"+district+" "+province;
                }
            }else{
                if (!TextUtils.isEmpty(subdistrict)) {
                    address +="ตำบล"+subdistrict+" ";
                }
                if (!TextUtils.isEmpty(district)) {
                    address +="อำเภอ"+district+"\nจังหวัด"+province;
                };
            }
        }
        return address;
    }
	
	public static String sortAddressWithLine(String home_no, String township, String village,
			String sidestreet, String road,String subdistrict, String district, String province) {
		String address = "";
		if (!TextUtils.isEmpty(home_no)) {
			address += "บ้านเลขที่ "+home_no + " ";
		}
		if (!TextUtils.isEmpty(township)) {
			address += "หมู่ที่ " + township + " ";
		}
		if (!TextUtils.isEmpty(village)) {
			address += "หมู่บ้าน" + village + " ";
		}
		if (!TextUtils.isEmpty(sidestreet)) {
			address += "ซอย" + sidestreet + " ";
		}
		if (!TextUtils.isEmpty(road)) {
			address += "ถนน" + road;
		}
		
		if(!TextUtils.isEmpty(province)){
			if(province.equals("กรุงเทพมหานคร")){
				if (!TextUtils.isEmpty(subdistrict)) {
					address +="\nแขวง"+subdistrict+" ";
				}
				if (!TextUtils.isEmpty(district)) {
					address +="เขต"+district+"\n"+province;
				}
			}else{
				if (!TextUtils.isEmpty(subdistrict)) {
					address +="\nตำบล"+subdistrict+" ";
				}
				if (!TextUtils.isEmpty(district)) {
					address +="อำเภอ"+district+"\nจังหวัด"+province;
				};
			}
		}

		return address;
	}
	

}
