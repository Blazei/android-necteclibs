/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.provider.columns;

/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public interface ApproveColumns {

	public static final String APPROVE_BY = "approve_by";
	public static final String APPROVE_TIME = "approve_time";
}
