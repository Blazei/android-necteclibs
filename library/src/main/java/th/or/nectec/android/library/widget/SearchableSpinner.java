/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * FFC-Plus Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.AttributeSet;
import android.widget.CursorAdapter;
import android.widget.Spinner;

import th.or.nectec.android.library.widget.SearchListDialog.ItemClickListener;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author piruinpanichphol
 * @since 1.0
 * 
 */
public class SearchableSpinner extends Spinner implements ItemClickListener {

	SearchListDialog f;
	FragmentManager fm;
	String tag;
	HighLightCursorAdapter mAdapter;
	static int count = 0;

	public SearchableSpinner(Context context, FragmentManager fm,
			Class<? extends SearchListDialog> cls, String tag) {
		super(context);

		setDialog(fm, cls, tag);
	}

	public SearchableSpinner(Context context) {
		super(context);
	}

	public SearchableSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SearchableSpinner setDialog(FragmentManager fm,
			Class<? extends SearchListDialog> cls, Bundle args, String tag) {

		this.tag = tag;

		this.fm = fm;
		Fragment prev = fm.findFragmentByTag(tag);
		if (prev != null)
			f = (SearchListDialog) prev;
		else
			f = (SearchListDialog) Fragment.instantiate(getContext(),
					cls.getName(), args);
		
		Cursor c = getContext().getContentResolver().query(f.getContentUri(),
				f.getProjection(), null, null, BaseColumns._ID);

		mAdapter = new HighLightCursorAdapter(getContext(), f.getLayout(), c,
				f.getFrom(), f.getTo());
		f.setItemClickListener(this);
		this.setAdapter(mAdapter);

		return this;
	}
	
	public SearchableSpinner setDialog(FragmentManager fm, 
			Class<? extends SearchListDialog> cls, String tag){
		return setDialog(fm, cls, null, tag);
	}

	@Override
	public boolean performClick() {
		boolean handle = false;
		if (!handle && fm != null) {
			FragmentTransaction ft = fm.beginTransaction();
			Fragment prev = fm.findFragmentByTag(tag);
			if (prev != null)
				ft.remove(prev);
			ft.addToBackStack(null);

			if (f != null) {
				f.setItemClickListener(this);
				f.show(ft, tag);

			}
		}
		return handle;
	}

	
	public void setSelectionById(long id){
		this.setSelection(binarySearch(mAdapter, id, 0, mAdapter.getCount()),
				true);
	}

	private static final int binarySearch(CursorAdapter adapter, long id, int min,
			int max) {
		if (max < min) {
			return 0;
		}
		int mid = (max + min) / 2;

		long curId = adapter.getItemId(mid);
		if (curId > id)
			return binarySearch(adapter, id, min, mid - 1);
		else if (curId < id)
			return binarySearch(adapter, id, mid + 1, max);
		else
			return mid;
	}

	@Override
	public void onItemClick(HighLightCursorAdapter adapter, long id,
			int position) {
		f.dismiss();
		this.setSelection(binarySearch(mAdapter, id, 0, mAdapter.getCount()),
				true);
	}

	@Override
	public void onItemClick(CursorStringIdAdapter adapter, String id,
			String text) {
		throw new IllegalArgumentException("SearchableSpinner not support for CursorStringIdAdapter dialog");
	}

}
