/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * GAP-Thailand Project
 *
 * Copyright (C) 2013 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */
package th.or.nectec.android.library.network;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import th.or.nectec.android.library.network.HttpAsyncTask.Response;
import th.or.nectec.android.library.util.DateTime;
import th.or.nectec.android.library.util.JSONInsertOrUpdateTask;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * @author blaze
 * 
 */
public class DataGrabber {

	public static final int FETCH_NOTHING = -1;
	Context mContext;
	ProgressBar mProgressBar;
	SQLiteOpenHelper mOpenHelper;
	OnFetchFinishListener mFinishListener;
	ArrayList<GrabberPack> mRequestList;
	HttpAsyncTask mPostTask;

	public DataGrabber(Context context) {
		mContext = context;
	}

	public Context getContext() {
		return mContext;
	}

	public void setOnFetchFinishListener(OnFetchFinishListener mFinishListener) {
		this.mFinishListener = mFinishListener;
	}

	public void setmProgressBar(ProgressBar mProgressBar) {
		this.mProgressBar = mProgressBar;
	}

	public void setmOpenHelper(SQLiteOpenHelper mOpenHelper) {
		this.mOpenHelper = mOpenHelper;
	}

	public void putRequest(HttpAsyncTask.Request request,
			JSONInsertOrUpdateTask.Params jsonParam) {
		if (request == null || jsonParam == null) {
			throw new IllegalArgumentException("request or jsonParam is null");
		}

		if (this.mRequestList == null) {
			this.mRequestList = new ArrayList<GrabberPack>();
		}
		GrabberPack pck = new GrabberPack(request, jsonParam);
		mRequestList.add(pck);
		
		if (mProgressBar != null) {
			mProgressBar.setMax(mRequestList.size());
		}
	}

	int currentRequest = 0;
	String lastRequestTime;

	/**
	 * call this method for start fecth operation and update database
	 * 
	 * @param id
	 *            Fecth's type id this param should define as public static int
	 *            FETCH_ on each class
	 * @param params
	 *            HttpAsyncTask param with all require parameter such as url
	 *            postmap
	 */
	public void fetch() {
		
		
		if (mRequestList == null || mRequestList.isEmpty()) {
			throw new InvalidParameterException("reqest list is empty");
		}
		GrabberPack pack = mRequestList.get(currentRequest);
		if (pack == null)
			throw new InvalidParameterException("not found request at "
					+ currentRequest);
		
		if (mProgressBar != null) {
			mProgressBar.setProgress(currentRequest+1);
		}

		lastRequestTime = DateTime.getCurrentDateTime();
		mPostTask = new HttpAsyncTask(mRequestCallback);
		mPostTask.execute(pack.request);
	}
	
	protected void fectchNextPage(int id, JSONObject returnData,
			int currentPage, int nextPage) {
		HttpAsyncTask.Request req = mRequestList.get(currentRequest).request;
		if (req != null) {
			req.putParam("page", "" + nextPage);
			fetch();
		}else{
			throw new NullPointerException("Request object is NULL");
		}
		
	}

	protected void fetchNextReqeust(String tablename, JSONObject returnData) {
		if (mFinishListener != null)
			mFinishListener.onFetchNextRequest( 
					tablename, returnData);
	
		
		if (++currentRequest < mRequestList.size()) {
			fetch();
		} else {
			if(mFinishListener != null)
				mFinishListener.onFetchFinish();
		}
	}




	private HttpAsyncTask.TaskCallBack mRequestCallback = new HttpAsyncTask.TaskCallBack() {

		@Override
		public void onProgressUpdate(Response... values) {
			Response res = values[0];
			if (res.status != 200) {
				onErrorOccur(OnFetchFinishListener.EXCEPTION_IO);
				return;
			}
			try {
				JSONObject response = new JSONObject(res.data);
				if (!response.getString("status").equals("success")) {
					onErrorOccur(OnFetchFinishListener.EXCEPTION_STATUS_FAIL);
					return;
				}

				final JSONObject returnData = response.optJSONObject("result");
				if (returnData != null) {
					JSONInsertOrUpdateTask updateTask = new JSONInsertOrUpdateTask(
							mContext, mOpenHelper, returnData, null,
							new JSONInsertOrUpdateTask.OnFinishListener() {

								@Override
								public void onTaskFinish(String result) {
									String tablename = mRequestList.get(currentRequest).json.tablename;
									boolean conti = false;
									int page = 0;
									JSONObject tab = returnData
											.optJSONObject(tablename);
									boolean c = tab.optBoolean("continue");
									if (c == true) {
										conti = true;
										page = tab.optInt("page");

									}

									if (conti) {
										if (mFinishListener != null) {
											mFinishListener.onFetchNextPage(tablename,
													page, page + 1);
										}
										fectchNextPage(currentRequest,
												returnData, page, page + 1);

									} else {

										// call external listener on fetch
										// finish event
										fetchNextReqeust(tablename, returnData);
									}

									

								}
							});
					updateTask.update();
					updateTask.execute(mRequestList.get(currentRequest).json);
				}

			} catch (JSONException e) {
				onErrorOccur(OnFetchFinishListener.EXCEPTION_JSON_FORMAT);
			}
		}

		@Override
		public void onPostExecute(ArrayList<Response> result) {
		}

		@Override
		public void onIOException() {
			onErrorOccur(OnFetchFinishListener.EXCEPTION_IO);
			
		}
	};

	private void onErrorOccur(int errorCase){
		boolean retry = true;
		if(mFinishListener != null)
			retry = mFinishListener.onFetchExceptionOccur(errorCase, 
					mRequestList.get(currentRequest).request);
		
		if(retry)
			fetch();
		else
			fetchNextReqeust(null, null);
	}





	public interface OnFetchFinishListener {
		
		/**
		 * Method was call on Grabber Fetched all request.
		 *
		 * @since 1.0
		 */
		public void onFetchFinish();

		/**
		 * 
		 * @param tablename  name of table in current page
		 * @param currentPage number of current page 
		 * @param nextPage number of next page that will request
		 *
		 * @since 1.0
		 */
		public void onFetchNextPage(String tablename,
				int currentPage, int nextPage);
		
		/**
		 * this method call before The Next Request will fetch. So, you can DO SOMETHING  such as Update Timestamp of last update 
		 * 
		 * @param tablename name of table in previous request,  May NULL if start from after FetchException
		 * @param returnData return data of previous request, May NULL if start from after FetchException
		 *
		 * @since 1.0
		 */
		public void onFetchNextRequest(String tablename, JSONObject returnData);
		
		public static final int EXCEPTION_IO = 1;
		public static final int EXCEPTION_JSON_FORMAT = 2;
		public static final int EXCEPTION_STATUS_FAIL = 3;
		
		/**
		 * @param exceptionCase case of exception EXCEPTION_IO, EXCEPTION_JSON_FORMAT and EXCEPTION_STATUS_FAIL
		 * @param request request object that an error occur
		 * 
		 * @return true if you want to RETRY, false if SKIP default is SKIP
		 *
		 * @since 1.0
		 */
		public boolean onFetchExceptionOccur(int exceptionCase, HttpAsyncTask.Request request);
	}

	private static class GrabberPack {
		public HttpAsyncTask.Request request;
		public JSONInsertOrUpdateTask.Params json;

		public GrabberPack(HttpAsyncTask.Request request,
				JSONInsertOrUpdateTask.Params jsonParam) {
			this.request = request;
			this.json = jsonParam;
		}
	}
}
