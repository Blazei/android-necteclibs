package th.or.nectec.android.library.util;

import android.text.TextUtils;
import android.widget.TextView;

public class TextViewUtils {

	public static void setText(TextView v, String text){
		if(v != null){
			if(!TextUtils.isEmpty(text))
				v.setText(text);
		}
	}
	
	public static void setText(TextView v, String text, String defaultText){
		if(v != null){
			if(!TextUtils.isEmpty(text)){
				v.setText(text);
			}else{
				v.setText(defaultText);
			}
		}
	}
}
