/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.provider;

import java.util.HashMap;

import th.or.nectec.android.library.database.sqlite.SQLiteCreateBuilder;
import th.or.nectec.android.library.provider.columns.AddressColumns;
import th.or.nectec.android.library.provider.columns.NameColumn;
import th.or.nectec.android.library.util.JSONInsertOrUpdateTask.Params;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author blaze
 * 
 */
public class NTTable {

	public static class RefAddress implements AddressColumns {
		public static final String TABLENAME = "RefAddress";

		public static final String UPDATE_TIME = "update_time";

		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ NTProvider.AUTHORITY + "/ref/address");

		public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
				+ "/vnd."+NTProvider.AUTHORITY+".ref.address";
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
				+ "/vnd.th."+NTProvider.AUTHORITY+".ref.address";

		public static final HashMap<String, String> PROJECTION_MAP;
		public static final String CREATE_SQL;
		public static final Params JSON_PARAM;

		static {
			SQLiteCreateBuilder builder = new SQLiteCreateBuilder(TABLENAME);
			builder.addTextColumn(ADDRESS_CODE,
					new String[] { SQLiteCreateBuilder.CONSTRAINT_PRIMARY_KEY });
			builder.addTextColumns(PROVINCE, DISTRICT, SUBDISTRICT, UPDATE_TIME);
			builder.addUNIQUE(PROVINCE, DISTRICT, SUBDISTRICT);

			CREATE_SQL = builder.build();
			PROJECTION_MAP = builder.buildProjectionMap();

			JSON_PARAM = new Params();
			JSON_PARAM.tablename = TABLENAME;
			JSON_PARAM.uri = CONTENT_URI;
			JSON_PARAM.strColumn = new String[]{ ADDRESS_CODE, SUBDISTRICT, DISTRICT, PROVINCE , UPDATE_TIME};
			JSON_PARAM.whereCause = "address_code=?";
			JSON_PARAM.argsColumn = new String[] { ADDRESS_CODE };
		}

	}

	public static class RefPrename implements BaseColumns, NameColumn {

		public static final String TABLENAME = "RefPrename";
		public static final String CODE = "prename_code";
		public static final String PRENAME = "prename";
		public static final String PRENAME_FULL = "prename_full";
		public static final String GENDER = "gender";
		public static final String STATUS = "status";
		public static final String UPDATE_TIME = "update_time";

		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ NTProvider.AUTHORITY + "/ref/prename");
		public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
				+ "/vnd."+NTProvider.AUTHORITY+".ref.prename";
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
				+ "/vnd."+NTProvider.AUTHORITY+".ref.prename";
		
		public static final HashMap<String, String> PROJECTION_MAP;
		public static final Params JSON_PARAM;
		public static final String CREATE_SQL;
		
		static {
			SQLiteCreateBuilder builder = new SQLiteCreateBuilder(TABLENAME);
			builder.addTextColumns(CODE, PRENAME, PRENAME_FULL, GENDER, STATUS,
					UPDATE_TIME);
			builder.addPrimaryKey(CODE);
			builder.addUNIQUE(PRENAME, PRENAME_FULL);

			CREATE_SQL = builder.build();
			PROJECTION_MAP = builder.buildProjectionMap();
			PROJECTION_MAP.put(_ID, CODE+" as "+_ID);
			PROJECTION_MAP.put(_NAME, PRENAME+" as "+_NAME);
			
			JSON_PARAM = new Params();
			JSON_PARAM.tablename = TABLENAME;
			JSON_PARAM.uri = CONTENT_URI;
			JSON_PARAM.strColumn = new String[]{ CODE, PRENAME, PRENAME_FULL, STATUS, UPDATE_TIME };
			JSON_PARAM.whereCause = "prename_code=?";
			JSON_PARAM.argsColumn = new String[] { CODE };
		}
	}

	public static class SyncTable {
		public static final String TABLENAME = "_sync";

		public static final String NAME = "name";
		public static final String UPDATE_TIME = "update_time";

		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ NTProvider.AUTHORITY + "/system/sync");
		public static final String CONTENT_DIR_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
				+ "/vnd."+NTProvider.AUTHORITY+".system.sync";
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
				+ "/vnd."+NTProvider.AUTHORITY+".system.sync";

		public static final HashMap<String, String> PROJECTION_MAP;
		public static final String CREATE_SQL;

		static {

			SQLiteCreateBuilder sb = new SQLiteCreateBuilder(TABLENAME);
			sb.addTextColumns(NAME, UPDATE_TIME);
			sb.addPrimaryKey(NAME);

			CREATE_SQL = sb.build();
			PROJECTION_MAP = sb.buildProjectionMap();
		}

		public static final String DEFAULT_TIMESTAMP = "0000-00-00 00:00:00";

		/**
		 * helper method to easy get last update-time of each table
		 * 
		 * @param context
		 *            just context
		 * @param name
		 *            name of table to get last update_time
		 * @return last update_time of specify table, null if not found
		 * 
		 * @since 1.0
		 */
		public static String getLastUpdateTime(Context context, String name) {
			Cursor c = context.getContentResolver().query(CONTENT_URI,
					new String[] { UPDATE_TIME }, "name=?",
					new String[] { name }, null);
			if (c.moveToFirst()) {
				return c.getString(0);
			} else {
				ContentValues cv = new ContentValues();
				cv.put(NAME, name);
				cv.put(UPDATE_TIME, DEFAULT_TIMESTAMP);
				context.getContentResolver().insert(CONTENT_URI, cv);
				return DEFAULT_TIMESTAMP;
			}
		}

	
	}
}
