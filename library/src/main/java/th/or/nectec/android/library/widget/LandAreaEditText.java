/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.widget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import th.or.nectec.android.library.util.NumberEditTextUtils;
import th.or.nectec.android.library.util.RaiToSqMeterConvertor;
import th.or.nectec.android.library.R;

/**
 * 
 * add description here!
 * 
 * @version 1.0
 * 
 * @author N. Choatravee
 * 
 */

public class LandAreaEditText extends LinearLayout {

	Resources mResources;
	ClearableEditText mRaiEditText, mNganEditText, mTarangwaEditText;

	TextWatcher mTxtWatcher;

	OnAreaChangeListener mListener;
	
	
	public LandAreaEditText(Context context) {
		this(context, null);
	}

	public LandAreaEditText(Context context, AttributeSet attrs) {
		this(context, attrs, android.R.attr.textViewStyle);
	}
	
	public LandAreaEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		this.setGravity(Gravity.CENTER_VERTICAL);

		
		LayoutParams editTextParam = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, 1);

		LayoutParams labelParam = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		mRaiEditText = new ClearableEditText(context);
		mNganEditText = new ClearableEditText(context);
		mTarangwaEditText = new ClearableEditText(context);
		
		mRaiEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
		mNganEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
		mTarangwaEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
		
		TextView raiTxtView, nganTxtView, tarangwaTxtView;
		raiTxtView = new TextView(context,attrs);
		nganTxtView = new TextView(context, attrs);
		tarangwaTxtView = new TextView(context,attrs);
		

		raiTxtView.setText(R.string.rai);
		nganTxtView.setText(R.string.ngan);
		tarangwaTxtView.setText(R.string.tarangwa);


		this.addView(mRaiEditText, editTextParam);
		this.addView(raiTxtView, labelParam);

		this.addView(mNganEditText, editTextParam);
		this.addView(nganTxtView, labelParam);

		this.addView(mTarangwaEditText, editTextParam);
		this.addView(tarangwaTxtView, labelParam);
		

		float minSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				46, getResources().getDisplayMetrics());
		this.setMinimumHeight((int) minSize);

		mTxtWatcher = new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (mListener != null) {
					mListener.onAreaChange(getArea());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		};

		mRaiEditText.addTextChangedListener(mTxtWatcher);
		mNganEditText.addTextChangedListener(mTxtWatcher);
		mTarangwaEditText.addTextChangedListener(mTxtWatcher);
	}

	/**
	 * return value as meter.
	 */
	public double getArea() {
		int rai = NumberEditTextUtils.getInt(mRaiEditText);
		int ngan = NumberEditTextUtils.getInt(mNganEditText);
		int tarangwa = NumberEditTextUtils.getInt(mTarangwaEditText);
		return RaiToSqMeterConvertor.RaiToSqMeter(rai, ngan, tarangwa);
	}

	/**
	 * @param areaSqM
	 *            Set area value using square meter unit.
	 */
	public void setArea(int areaSqM) {
		int rai = RaiToSqMeterConvertor.sqMeterToRai(areaSqM);
		int ngan = RaiToSqMeterConvertor.sqMeterToNgan(areaSqM);
		int tarangwa = RaiToSqMeterConvertor.sqMeterToTarangwa(areaSqM);

		String raiStr = (rai > 0) ? String.valueOf(rai) : "";
		String nganStr = (ngan > 0) ? String.valueOf(ngan) : "";
		String tarangwaStr = (tarangwa > 0) ? String.valueOf(tarangwa) : "";

		mRaiEditText.setText(raiStr);
		mNganEditText.setText(nganStr);
		mTarangwaEditText.setText(tarangwaStr);
	}

	/**
	 * @param rai
	 * @param ngan
	 * @param tarangwa
	 *            Set area value using Thai area unit.
	 */
	public void setArea(int rai, int ngan, int tarangwa) {

		String raiStr = (rai > 0) ? String.valueOf(rai) : "";
		String nganStr = (ngan > 0) ? String.valueOf(ngan) : "";
		String tarangwaStr = (tarangwa > 0) ? String.valueOf(tarangwa) : "";

		mRaiEditText.setText(raiStr);
		mNganEditText.setText(nganStr);
		mTarangwaEditText.setText(tarangwaStr);
	}

	/**
	 * @param areaSqM
	 *            Set area hint using square meter unit.
	 */
	public void setAreaHint(int areaSqM) {
		int rai = RaiToSqMeterConvertor.sqMeterToRai(areaSqM);
		int ngan = RaiToSqMeterConvertor.sqMeterToNgan(areaSqM);
		int tarangwa = RaiToSqMeterConvertor.sqMeterToTarangwa(areaSqM);

		String raiStr = (rai > 0) ? String.valueOf(rai) : "";
		String nganStr = (ngan > 0) ? String.valueOf(ngan) : "";
		String tarangwaStr = (tarangwa > 0) ? String.valueOf(tarangwa) : "";

		mRaiEditText.setHint(raiStr);
		mNganEditText.setHint(nganStr);
		mTarangwaEditText.setHint(tarangwaStr);
	}

	/**
	 * Get area of rai only.
	 */
	public int getRai() {
		return NumberEditTextUtils.getInt(mRaiEditText);
	}

	/**
	 * Get area of ngan only.
	 */
	public int getNgan() {
		return NumberEditTextUtils.getInt(mNganEditText);
	}

	/**
	 * Get area of tarangwa only.
	 */
	public int gettarangwa() {
		return NumberEditTextUtils.getInt(mTarangwaEditText);
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		Log.d("area", getRai() + "/" + getNgan() + "/" + gettarangwa());

		SavedState ss = new SavedState(superState);
		ss.rai = this.getRai();
		ss.ngan = this.getNgan();
		ss.tarangwa = this.gettarangwa();

		return ss;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());

		this.setArea(ss.rai, ss.ngan, ss.tarangwa);
	}

	static class SavedState extends BaseSavedState {
		int rai, ngan, tarangwa;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			this.rai = in.readInt();
			this.ngan = in.readInt();
			this.tarangwa = in.readInt();
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeInt(this.rai);
			out.writeInt(this.ngan);
			out.writeInt(this.tarangwa);
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}
	
	public void setEnabled(boolean enabled){
		mRaiEditText.setEnabled(enabled);
		mNganEditText.setEnabled(enabled);
		mTarangwaEditText.setEnabled(enabled);
	}

	// callback listener
	public void setOnAreaChangeListener(OnAreaChangeListener listener) {
		mListener = listener;
	}

	public static interface OnAreaChangeListener {
		public void onAreaChange(double area);
	}

}
