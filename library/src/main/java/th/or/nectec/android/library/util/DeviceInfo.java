/* ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 *
 * NECTEC Android Library Project
 *
 * Copyright (C) 2010-2012 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */
package th.or.nectec.android.library.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * 
 * Class of static method for easily get Device's Information such as IMEI,
 * WIFI-MacAddress, ANDROID_ID and other
 * 
 * @version 1.0
 * 
 * @author Piruin Panichphol
 * 
 */
public class DeviceInfo {

	public String getUDID(Context context) {
		String wifiMacAddress = getWifiMacAddress(context);
		if (!TextUtils.isEmpty(wifiMacAddress)) {
			return wifiMacAddress;
		}

		String androidID = getAndroidId(context);
		if (!TextUtils.isEmpty(androidID)) {
			return androidID;
		}

		return null;
	}

	/**
	 * return device's model via android.os.Build.MODEL
	 * 
	 * @return
	 * 
	 * @since 1.0
	 */
	public static String getModel() {
		return Build.MODEL;
	}

	/**
	 * return user-visible android version via android.os.Build.VERSION.RELEASE
	 * 
	 * @return VERSION.RELEASE
	 * 
	 * @since 1.0
	 */
	public static String getOsVersion() {
		return Build.VERSION.RELEASE;
	}

	/**
	 * return android SDK Version as Integer via
	 * android.os.Build.VERSION_SDK_INT its possible values are defined in
	 * Build.VERSION_CODES.
	 * 
	 * @return VERSION.SDK_INT
	 * 
	 * @since 1.0
	 */
	public static int getSdkVersion() {
		return Build.VERSION.SDK_INT;
	}

	/**
	 * Since Android 2.3 (�Gingerbread�) this is available via
	 * android.os.Build.SERIAL. Devices without telephony are required to report
	 * a unique device ID here; some phones may do so also.
	 * 
	 * @return Serial Number
	 * 
	 */
	public static String getSerailNumber() {
		return Build.SERIAL;
	}

	/**
	 * ANDROID_ID This is a 64-bit quantity that is generated and stored when
	 * the device first boots. It is reset when the device is wiped
	 * 
	 * ANDROID_ID seems a good choice for a unique device identifier. There are
	 * downsides: First, it is not 100% reliable on releases of Android prior to
	 * 2.2 (�Froyo�). Also, there has been at least one widely-observed bug in a
	 * popular handset from a major manufacturer, where every instance has the
	 * 0same ANDROID_ID.
	 * 
	 * @param context
	 * @return ANDROID_ID
	 * 
	 * @since 1.0
	 */
	public static String getAndroidId(Context context) {
		String id = android.provider.Settings.System.getString(
				context.getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		return id;
	}

	/**
	 * Return the Wifi's MacAddress that already removed ':' and '.' Return null
	 * if WIFI_SERVICE cannot access. Requires Permiision:ACCESS_WIFI_STATE
	 * 
	 * @param context
	 * @return
	 * 
	 * @since 1.0
	 */
	public static String getWifiMacAddress(Context context) {
		WifiManager manager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = manager.getConnectionInfo();
		if (wifiInfo == null || wifiInfo.getMacAddress() == null)
			return null;
		else
			return wifiInfo.getMacAddress().replace(":", "").replace(".", "");
	}

	/**
	 * Returns the unique device ID, for example, the IMEI for GSM and the MEID
	 * or ESN for CDMA phones. Return null if device ID is not available.
	 * Requires Permission: READ_PHONE_STATE
	 * 
	 * @param context
	 * @return unique device ID
	 * 
	 * @since 1.0
	 */
	public static String getImei(Context context) {
		TelephonyManager m = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = m != null ? m.getDeviceId() : null;
		return imei;
	}

	/**
	 * Get IP address from first non-localhost interface
	 * 
	 * @param ipv4
	 *            true=return ipv4, false=return ipv6
	 * @return address or empty string
	 */
	public static String getIpAddress(boolean useIPv4) {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf
						.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress().toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								int delim = sAddr.indexOf('%'); // drop ip6 port
																// suffix
								return delim < 0 ? sAddr : sAddr.substring(0,
										delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return "";
	}

	/**
	 * Returns MAC address of the given interface name.
	 * 
	 * @param interfaceName
	 *            eth0, wlan0 or NULL=use first interface
	 * @return mac address or empty string
	 */
	public static String getMacAddress(String interfaceName) {
		try {
			List<NetworkInterface> interfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName))
						continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac == null)
					return "";
				StringBuilder buf = new StringBuilder();
				for (int idx = 0; idx < mac.length; idx++)
					buf.append(String.format("%02X:", mac[idx]));
				if (buf.length() > 0)
					buf.deleteCharAt(buf.length() - 1);
				return buf.toString();
			}
		} catch (Exception ex) {
		} // for now eat exceptions
		return "";
		/*
		 * try { // this is so Linux hack return
		 * loadFileAsString("/sys/class/net/" +interfaceName +
		 * "/address").toUpperCase().trim(); } catch (IOException ex) { return
		 * null; }
		 */
	}

}
