/* 
 * NectecLibraries Project
 *
 * Copyright (C) 2014-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package th.or.nectec.android.library.form.validator;

import th.or.nectec.android.library.util.DateTime;
import th.or.nectec.android.library.widget.ThaiDatePicker;
import android.text.TextUtils;


/**
 * 
 * add description here!
 *
 * @version 1.0
 * 
 * @author blaze
 *
 */
public class DateFieldCompareValidator extends AbstractValidator {

	int mCompareMode;
	ThaiDatePicker mCompareDateView;
	String mDateValue;
	 
	
	public static final int BEFORE = 0;
	public static final int AFTER = 1;


	public DateFieldCompareValidator(int mode, ThaiDatePicker compareDate, String errorMessage) {
		super(null);
		mCompareMode = mode;
		mCompareDateView = compareDate;
		mErrorMsg = errorMessage;
	}
	

	@Override
	public boolean isValid(String value) throws ValidatorException {
		mDateValue = value;
		DateTime val = DateTime.newInstance(value);
		switch(mCompareMode){
			case BEFORE:
				return val.compareTo(mCompareDateView.getDate())<0 ? false : true;
			case AFTER:
				return val.compareTo(mCompareDateView.getDate())>0 ? false : true;
			default:
				return true;
		}
	}

	@Override
	public String getMessage() {
		if(!TextUtils.isEmpty(mErrorMsg)){
			return mErrorMsg;
		}else{
			switch(mCompareMode){
			case BEFORE:
				return DateTime.getFullFormatThai(mDateValue)+" ไม่ควรช้ากว่าวันที่  "+DateTime.getFullFormatThai(mCompareDateView.getDate());
			case AFTER:
				return DateTime.getFullFormatThai(mDateValue)+" ไม่ควรเลยวันที่  "+DateTime.getFullFormatThai(mCompareDateView.getDate());
			default:
				return "ไม่ทราบ";
		}
		}
		
	}
	

	public static  DateFieldCompareValidator getBeforeDateValidator(ThaiDatePicker compareDate){
		return new DateFieldCompareValidator(BEFORE, compareDate, null);
}
	
	public static  DateFieldCompareValidator getBeforeDateValidator(ThaiDatePicker compareDate ,String msg){
			return new DateFieldCompareValidator(BEFORE, compareDate, msg);
	}
	
	public static  DateFieldCompareValidator getAfterDateValidator(ThaiDatePicker compareDate){
		return new DateFieldCompareValidator(AFTER, compareDate, null);
	}
	
	public static  DateFieldCompareValidator getAfterDateValidator(ThaiDatePicker compareDate, String msg){
		return new DateFieldCompareValidator(AFTER, compareDate, msg);
	}

}
